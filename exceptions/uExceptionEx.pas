unit uExceptionEx;

interface

uses
  SysUtils, Classes;

type
  { DOCUMENTA��O : Quando incluir um novo tipo de erro, lembrar de definir
    a mensagem do erro no vetor MsgTExceptionType no fim desta unit. }
  TExceptionType = (
    etExceptionEx,
    // erros de banco de dados
    etDBException,
    etDBTableNotFound,
    etDBProcedureNotFound,
    etDBConstraintNotFound,
    etDBGeneratorNotFound,
    etDBColumnNotFound,
    etDBDeadlock,
    etDBAccessViolation,
    etDBDuplicatedIndex,
    etDBDuplicatedKey,
    etDBExceptionSintaxError,
    // erros gerados pelos controllers
    etExceptionDuplicated,
    etExceptionPontoSobreposto,
    etExceptionPontoSobrepostoEdicao,
    etExceptionPontoReferencia,
    etExceptionItinerarioExtraUnico,
    etExceptionObjetoInexistente,
    etExceptionCarregarLista,
    etExceptionActionDataBase,
    etExceptionExecuted,
    etExceptionUsuarioLogado,
    etExceptionUsuarioSenhaInvalida,
    etExceptionDesconectarUsuarios,
    etExceptionAnaliseQuilometragem,
    etExceptionTratarXML,
    etExceptionImportarHistoricoWeb,
    etExceptionHabilitarIntegracao,
    etExceptionConsultaVazia,
    etExceptionUnitTest,
    etExceptionString,
    etDBUniqueKeyDuplicated,
    etExceptionSemRegistros,
    etExceptionNaoConectadoAoServidor,
    etExceptionLimitePassageiros,
    etExceptionFormatoArquivoInvalido
  );

  TExceptionEx = class(Exception)
  protected
    FTypeCode: TExceptionType;
  public
    constructor Create(const Msg: string);
    constructor CreateFromStream(AStream: TStream); virtual;
    // controle de envio e recebimento via stream
    procedure WriteToStream(AStream: TStream); virtual;
    property TypeCode: TExceptionType read FTypeCode;
  end;

  TExceptionCustomWarningMessage = class(TExceptionEx)
  private
    FMessage: string;
  public
    constructor Create;
  end;

  TExceptionDuplicated = class(TExceptionEx)
  private
    FValue: string;
    FField: string;
    FModel: string;
  published
  public
    constructor Create(const AValue, AField, AModel: string);
    constructor CreateFromStream(AStream: TStream); override;
    // controle de envio e recebimento via stream
    procedure WriteToStream(AStream: TStream); override;
    // properties
    property Value: string read FValue;
    property Field: string read FField;
    property Model: string read FModel;
  end;

  TExceptionPontoSobreposto = class(TExceptionEx)
  public
    constructor Create;
  end;

   TExceptionPontoSobrepostoEdicao = class(TExceptionEx)
  public
    constructor Create;
  end;

  TExceptionPontoReferencia = class(TExceptionEx)
  public
    constructor Create;
  end;

  TExceptionItinerarioExtraDuplicado = class(TExceptionEx)
  public
    constructor Create;
  end;

  TExceptionHabilitarIntegracao = class(TExceptionEx)
  public
    constructor Create;
  end;

  TExceptionUsuarioLogado = class(TExceptionEx)
  public
    constructor Create;
  end;

  TExceptionUsuarioSenhaInvalida = class(TExceptionEx)
  public
    constructor Create;
  end;

  TExceptionDesconectarUsuarios = class(TExceptionEx)
  public
    constructor Create;
  end;

  TExceptionObjetoInexistente = class(TExceptionEx)
  private
    FModel: string;
    FField: string;
    FValue: string;
  public
    constructor Create(const AModel, AField, AValue: string);
    constructor CreateFromStream(AStream: TStream); override;
    // controle de envio e recebimento via stream
    procedure WriteToStream(AStream: TStream); override;
    //property
    property Model: string read FModel;
    property Field: string read FField;
    property Value: string read FValue;
  end;

  TExceptionImportarHistoricoWeb = class(TExceptionEx)
    constructor Create;
  end;

  TExceptionCarregarLista = class(TExceptionEx)
  private
    FValue: string;
  public
    constructor Create(const AValue: string);
    constructor CreateFromStream(AStream: TStream); override;
    // controle de envio e recebimento via stream
    procedure WriteToStream(AStream: TStream); override;
    //property
    property Value: string read FValue;
  end;

  TExceptionActionDataBase = class(TExceptionEx)
  private
    FAction: string;
    FValue: string;
    FField: string;
    FTable: string;
  public
    constructor Create(const AAction, AValue, AField, ATable: string);
    constructor CreateFromStream(AStream: TStream); override;

    procedure WriteToStream(AStream: TStream); override;
    //property
    property Action: string read FAction;
    property Value: string read FValue;
    property Field: string read FField;
    property Table: string read FTable;
  end;

  TExceptionExecuted = class(TExceptionEx)
  private
    FAction: string;
    FValue: string;
    FSecondValue: string;
  public
    constructor Create(const AAction, AValue: string; ASecondValue: string = '');
    constructor CreateFromStream(AStream: TStream); override;

    procedure WriteToStream(AStream: TStream); override;
    //property
    property Action: string read FAction;
    property Value: string read FValue;
    property SecondValue: string read FSecondValue;
  end;

  TExceptionAnaliseQuilometragem = class(TExceptionEx)
  public
    constructor Create;
  end;

  TExceptionTratarXML = class(TExceptionEx)
  private
    FValue: string;
  public
    constructor Create(const AValue: string);
    constructor CreateFromStream(AStream: TStream); override;
    // controle de envio e recebimento via stream
    procedure WriteToStream(AStream: TStream); override;
    //property
    property Value: string read FValue;
  end;

  TExceptionConsultaVazia = class(TExceptionEx)
    constructor Create;
  end;

  TExceptionUnitTest = class(TExceptionEx)
  private
    FValue: string;
  public
    constructor Create(const AValue: string);
    constructor CreateFromStream(AStream: TStream); override;

    // controle de envio e recebimento via stream
    procedure WriteToStream(AStream: TStream); override;
    //property
    property Value: string read FValue;
  end;

  TExceptionString = class(TExceptionEx)
  private
    FValue: string;
  public
    constructor Create(const AValue: string);
    constructor CreateFromStream(AStream: TStream); override;
    // controle de envio e recebimento via stream
    procedure WriteToStream(AStream: TStream); override;
    //property
     property Value: string read FValue;
  end;

  TExceptionSemRegistros = class(TExceptionEx)
  private
    FValue: string;
  public
    constructor Create;
    constructor CreateFromStream(AStream: TStream); override;
    // controle de envio e recebimento via stream
    procedure WriteToStream(AStream: TStream); override;
    //property
     property Value: string read FValue;
  end;

  TExceptionNaoConectadoAoServidor = class(TExceptionEx)
  public
    constructor Create;
  end;

  TExceptionLimitePassageiros = class(TExceptionEx)
  public
    constructor Create;
  end;

  TExceptionFormatoArquivoInvalido = class(TExceptionEx)
  public
    constructor Create;
  end;

  TExceptionCadastroPassageiro = class(TExceptionEx)
  end;

  TObjetoNaoEncontradoEx = class(TExceptionEx)
  end;

var
  MsgTExceptionType: array[TExceptionType] of string;

implementation

uses
  uHelperTStream;

{ TExceptionEx }

constructor TExceptionEx.Create(const Msg: string);
begin
  inherited Create(Msg);
  FTypeCode := etExceptionEx;
end;

constructor TExceptionEx.CreateFromStream(AStream: TStream);
begin
  inherited Create(MsgTExceptionType[etExceptionEx]);
  
  AStream.Read(FTypeCode, SizeOf(TExceptionType));
end;

procedure TExceptionEx.WriteToStream(AStream: TStream);
begin
  AStream.Write(FTypeCode, SizeOf(TExceptionType));
end;

{ TExceptionDuplicated }

constructor TExceptionDuplicated.Create(const AValue, AField, AModel: string);
begin
  inherited Create(Format(MsgTExceptionType[etExceptionDuplicated], [AValue, AField, AModel]));
  FTypeCode := etExceptionDuplicated;
  FValue := AValue;
  FField := AField;
  FModel := AModel;
end;

constructor TExceptionDuplicated.CreateFromStream(AStream: TStream);
begin
  inherited;

  FValue := AStream.ReadString;
  FField := AStream.ReadString;
  FModel := AStream.ReadString;
  // atualiza a mensagem do erro
  Self.Message := Format(MsgTExceptionType[etExceptionDuplicated], [FValue, FField, FModel]);
end;

procedure TExceptionDuplicated.WriteToStream(AStream: TStream);
begin
  inherited;

  AStream.WriteString(FValue);
  AStream.WriteString(FField);
  AStream.WriteString(FModel);
end;

{ TExceptionPontoSobreposto }

constructor TExceptionPontoSobreposto.Create;
begin
  inherited Create(MsgTExceptionType[etExceptionPontoSobreposto]);
  FTypeCode := etExceptionPontoSobreposto;
end;

{ TExceptionObjetoInexistente }

constructor TExceptionObjetoInexistente.Create(const AModel, AField, AValue: string);
begin
  inherited Create(Format(MsgTExceptionType[etExceptionObjetoInexistente], [AModel, AField, AValue]));
  FTypeCode := etExceptionObjetoInexistente;

  FModel := AModel;
  FField := AField;
  FValue := AValue;
end;

constructor TExceptionObjetoInexistente.CreateFromStream(AStream: TStream);
begin
  inherited;
  FModel := AStream.ReadString;
  FField := AStream.ReadString;
  FValue := AStream.ReadString;
  // atualiza a mensagem do erro
  Self.Message := Format(MsgTExceptionType[etExceptionObjetoInexistente], [FModel, FField, FValue]);
end;

procedure TExceptionObjetoInexistente.WriteToStream(AStream: TStream);
begin
  inherited;
  AStream.WriteString(FModel);
  AStream.WriteString(FField);  
  AStream.WriteString(FValue);
end;

{ TExceptionCarregarLista }

constructor TExceptionCarregarLista.Create(const AValue: string);
begin
  inherited Create(Format(MsgTExceptionType[etExceptionCarregarLista], [AValue]));
  FTypeCode := etExceptionCarregarLista;
  FValue := AValue;
end;

constructor TExceptionCarregarLista.CreateFromStream(AStream: TStream);
begin
  inherited;
  FValue := AStream.ReadString;
  // atualiza a mensagem do erro
  Self.Message := Format(MsgTExceptionType[etExceptionCarregarLista], [FValue]);
end;

procedure TExceptionCarregarLista.WriteToStream(AStream: TStream);
begin
  inherited;
  AStream.WriteString(FValue);
end;

{ TExceptionActionDataBase }

constructor TExceptionActionDataBase.Create(const AAction, AValue, AField,
  ATable: string);
begin
  inherited Create(Format(MsgTExceptionType[etExceptionActionDataBase], [AAction, AValue, AField,ATable]));
  FTypeCode := etExceptionActionDataBase;
  FAction := AAction;
  FValue  := AValue;
  FField  := AField;
  FTable  := ATable;
end;

constructor TExceptionActionDataBase.CreateFromStream(AStream: TStream);
begin
  inherited;
  FAction := AStream.ReadString;
  FValue  := AStream.ReadString;
  FField := AStream.ReadString;
  FTable  := AStream.ReadString;

  // atualiza a mensagem do erro
  Self.Message := Format(MsgTExceptionType[etExceptionActionDataBase], [FAction, FValue, FField, FTable]);
end;

procedure TExceptionActionDataBase.WriteToStream(AStream: TStream);
begin
  inherited;
  AStream.WriteString(FAction);
  AStream.WriteString(FValue);
  AStream.WriteString(FField);
  AStream.WriteString(FTable);
end;

{ TExceptionExecuted }

constructor TExceptionExecuted.Create(const AAction, AValue: string; ASecondValue: string = '');
begin
  inherited Create(Format(MsgTExceptionType[etExceptionExecuted], [AAction, AValue, FSecondValue]));
  FTypeCode := etExceptionExecuted;
  FAction := AAction;
  FValue  := AValue;
  FSecondValue  := ASecondValue;
end;

constructor TExceptionExecuted.CreateFromStream(AStream: TStream);
begin
  inherited;
  FAction := AStream.ReadString;
  FValue  := AStream.ReadString;
  FSecondValue  := AStream.ReadString;
end;

procedure TExceptionExecuted.WriteToStream(AStream: TStream);
begin
  inherited;
  AStream.WriteString(FAction);
  AStream.WriteString(FValue);
  AStream.WriteString(FSecondValue);
end;

{ TExceptionUsuarioLogado }

constructor TExceptionUsuarioLogado.Create;
begin
  inherited Create(MsgTExceptionType[etExceptionUsuarioLogado]);
  FTypeCode := etExceptionUsuarioLogado;
end;

{ TExceptionUsuarioSenhaInvalida }

constructor TExceptionUsuarioSenhaInvalida.Create;
begin
  inherited Create(MsgTExceptionType[etExceptionUsuarioSenhaInvalida]);
  FTypeCode := etExceptionUsuarioSenhaInvalida;
end;

{ TExceptionDesconectarUsuarios }

constructor TExceptionDesconectarUsuarios.Create;
begin
  inherited Create(MsgTExceptionType[etExceptionDesconectarUsuarios]);
  FTypeCode := etExceptionDesconectarUsuarios;
end;

{ TExceptionAnaliseQuilometragem }

constructor TExceptionAnaliseQuilometragem.Create;
begin
  inherited Create(MsgTExceptionType[etExceptionAnaliseQuilometragem]);
  FTypeCode := etExceptionAnaliseQuilometragem;
end;

{ TExceptionTratarXML }

constructor TExceptionTratarXML.Create(const AValue: string);
begin
  inherited Create(Format(MsgTExceptionType[etExceptionTratarXML], [AValue]));
  FTypeCode := etExceptionTratarXML;

  FValue := AValue;
end;

constructor TExceptionTratarXML.CreateFromStream(AStream: TStream);
begin
  inherited;
  FValue := AStream.ReadString;
  // atualiza a mensagem do erro
  Self.Message := Format(MsgTExceptionType[etExceptionObjetoInexistente], [FValue]);
end;

procedure TExceptionTratarXML.WriteToStream(AStream: TStream);
begin
  inherited;
  AStream.WriteString(FValue);
end;

{ TExceptionImportarHistoricoWeb }

constructor TExceptionImportarHistoricoWeb.Create;
begin
  inherited Create(MsgTExceptionType[etExceptionImportarHistoricoWeb]);
  FTypeCode := etExceptionImportarHistoricoWeb;
end;

{ TExceptionHabilitarIntegracao }

constructor TExceptionHabilitarIntegracao.Create;
begin
  inherited Create(MsgTExceptionType[etExceptionHabilitarIntegracao]);
  FTypeCode := etExceptionHabilitarIntegracao;
end;

{ TExceptionConsultaVazia }

constructor TExceptionConsultaVazia.Create;
begin
  inherited Create(MsgTExceptionType[etExceptionConsultaVazia]);
  FTypeCode := etExceptionConsultaVazia;
end;

{ TExceptionUnitTest }

constructor TExceptionUnitTest.Create(const AValue: string);
begin
  inherited Create(Format(MsgTExceptionType[etExceptionUnitTest], [AValue]));
  FTypeCode := etExceptionUnitTest;

  FValue := AValue;
end;

constructor TExceptionUnitTest.CreateFromStream(AStream: TStream);
begin
  inherited;
  FValue := AStream.ReadString;
  // atualiza a mensagem do erro
  Self.Message := Format(MsgTExceptionType[etExceptionUnitTest], [FValue]);
end;

procedure TExceptionUnitTest.WriteToStream(AStream: TStream);
begin
  inherited;
  AStream.WriteString(FValue);
end;

{ TExceptionString }

constructor TExceptionString.Create(const AValue: string);
begin
  inherited Create(Format(MsgTExceptionType[etExceptionString], [AValue]));
  FTypeCode := etExceptionString;
  FValue := AValue;
end;

constructor TExceptionString.CreateFromStream(AStream: TStream);
begin
  inherited;
  FValue := AStream.ReadString;
  // atualiza a mensagem do erro
  Self.Message := Format(MsgTExceptionType[etExceptionString], [FValue]);
end;

procedure TExceptionString.WriteToStream(AStream: TStream);
begin
  inherited;
  AStream.WriteString(FValue);
end;

{ TExceptionItinerarioExtraDuplicado }

constructor TExceptionItinerarioExtraDuplicado.Create;
begin
  inherited Create(MsgTExceptionType[etExceptionItinerarioExtraUnico]);
  Self.Message := MsgTExceptionType[etExceptionItinerarioExtraUnico];
  FTypeCode := etExceptionItinerarioExtraUnico;
end;

{ TExceptionSemRegistros }

constructor TExceptionSemRegistros.Create;
begin
  inherited Create(MsgTExceptionType[etExceptionSemRegistros]);
  FTypeCode := etExceptionSemRegistros;
end;

constructor TExceptionSemRegistros.CreateFromStream(AStream: TStream);
begin
  inherited;
  FValue := AStream.ReadString;
  // atualiza a mensagem do erro
  Self.Message := Format(MsgTExceptionType[etExceptionSemRegistros], [FValue]);
end;

procedure TExceptionSemRegistros.WriteToStream(AStream: TStream);
begin
  inherited;
  AStream.WriteString(FValue);
end;

{ TExceptionCustomWarningMessage }

constructor TExceptionCustomWarningMessage.Create;
begin

end;

{ TExceptionNaoConectadoAoServidor }

constructor TExceptionNaoConectadoAoServidor.Create;
begin
  inherited Create(MsgTExceptionType[etExceptionNaoConectadoAoServidor]);
  FTypeCode := etExceptionNaoConectadoAoServidor;
end;

{ TExceptionLimitePassageiros }

constructor TExceptionLimitePassageiros.Create;
begin
  inherited Create(MsgTExceptionType[etExceptionLimitePassageiros]);
  FTypeCode := etExceptionLimitePassageiros;
end;

{ TExceptionFormatoArquivoInvalido }

constructor TExceptionFormatoArquivoInvalido.Create;
begin
  inherited Create(MsgTExceptionType[etExceptionFormatoArquivoInvalido]);
  FTypeCode := etExceptionFormatoArquivoInvalido;
end;

{ TExceptionPontoSobrepostoEdicao }

constructor TExceptionPontoSobrepostoEdicao.Create;
begin
  inherited Create(MsgTExceptionType[etExceptionPontoSobrepostoEdicao]);
  FTypeCode := etExceptionPontoSobrepostoEdicao;
end;

{ TExceptionPontoReferencia }

constructor TExceptionPontoReferencia.Create;
begin
  inherited Create(MsgTExceptionType[etExceptionPontoReferencia]);
  FTypeCode := etExceptionPontoReferencia;
end;

initialization
  { Documenta��o : As mensagens dos exceptions devem estar coerentes com a compreens�o dos usu�rios }
  MsgTExceptionType[etExceptionEx]                     := 'O sistema n�o conseguiu realizar a opera��o devido a um problema desconhecido.';
  // erros de banco de dados
  MsgTExceptionType[etDBException]                     := 'Erro desconhecido no banco de dados.';
  MsgTExceptionType[etDBTableNotFound]                 := 'A tabela %s n�o existe.';
  MsgTExceptionType[etDBProcedureNotFound]             := 'A procedure %s n�o existe.';
  MsgTExceptionType[etDBConstraintNotFound]            := 'A constraint %s n�o existe.';
  MsgTExceptionType[etDBGeneratorNotFound]             := 'O generator %s n�o existe.';
  MsgTExceptionType[etDBColumnNotFound]                := 'A coluna %s n�o existe na tabela.';
  MsgTExceptionType[etDBDeadlock]                      := '';
  MsgTExceptionType[etDBAccessViolation]               := '';
  MsgTExceptionType[etDBDuplicatedIndex]               := '';
  MsgTExceptionType[etDBDuplicatedKey]                 := 'Inser��o de registro com chave duplicada para a constraint %s na tabela %s.';
  MsgTExceptionType[etDBExceptionSintaxError]          := 'Ocorreu erro de Sintaxe na instru��o SQL.';
  MsgTExceptionType[etDBUniqueKeyDuplicated]           := 'Inser��o de registro com Unique Key duplicada %s.';

  // erros gerados pelos controllers
  MsgTExceptionType[etExceptionDuplicated]             := 'O valor %s no campo %s j� existe em %s. Por favor, inserir outro valor!';
  MsgTExceptionType[etExceptionPontoSobreposto]        := 'O Ponto de Refer�ncia est� sobreposto com outro ponto.';
  MsgTExceptionType[etExceptionPontoSobrepostoEdicao]  := 'N�o � poss�vel finalizar a edi��o quando existe sobreposi��o de pontos.';
  MsgTExceptionType[etExceptionPontoReferencia]        := 'N�o foi poss�vel estabelecer as novas posi��es dos rotogramas do ponto de refer�ncia.' + #13 + ' Tente remover os rotogramas do ponto antes de alterar sua posi��o.';
  MsgTExceptionType[etExceptionObjetoInexistente]      := 'N�o foi poss�vel carregar %s. (%s = %s)';{Model, campo, Valor}
  MsgTExceptionType[etExceptionCarregarLista]          := 'N�o foi poss�vel carregar a lista de %s.';
  MsgTExceptionType[etExceptionActionDataBase]         := 'N�o foi poss�vel %s o valor "%s" do campo "%s" da tabela "%s".';
  MsgTExceptionType[etExceptionExecuted]               := 'N�o foi poss�vel %s o valor %s na base de dados. %s.';
  MsgTExceptionType[etExceptionUsuarioLogado]          := 'Usu�rio j� logado.';
  MsgTExceptionType[etExceptionUsuarioSenhaInvalida]   := 'Usu�rio com Senha Inv�lida.';
  MsgTExceptionType[etExceptionDesconectarUsuarios]    := 'Erro ao desconectar usu�rios.';
  MsgTExceptionType[etExceptionAnaliseQuilometragem]   := 'A quilometragem de p�tio n�o pode ser processada porque n�o foi encontrada a categoria de GARAGEM do ponto de refer�ncia.';
  MsgTExceptionType[etExceptionTratarXML]              := 'N�o foi poss�vel tratar XML %s';
  MsgTExceptionType[etExceptionImportarHistoricoWeb]   := 'N�o foi poss�vel enviar Importar Hist�rico para Web';
  MsgTExceptionType[etExceptionHabilitarIntegracao]    := 'N�o foi poss�vel Habilitar integra��es';
  MsgTExceptionType[etExceptionConsultaVazia]          := 'A consulta n�o retornou nenhum resultado.';
  MsgTExceptionType[etExceptionUnitTest]               := '%s';
  MsgTExceptionType[etExceptionString]                 := '%s';
  MsgTExceptionType[etExceptionItinerarioExtraUnico]   := 'A linha j� possui um itiner�rio extra.';
  MsgTExceptionType[etExceptionSemRegistros]           := 'Consulta n�o retornou registros';
  MsgTExceptionType[etExceptionNaoConectadoAoServidor] := 'N�o foi poss�vel conectar ao servidor: Verifique se o servidor est� sendo executado.';
  MsgTExceptionType[etExceptionLimitePassageiros]      := 'Foi atingido o limite de cadastros de passageiros.';
  MsgTExceptionType[etExceptionFormatoArquivoInvalido] := 'Arquivo com formato inv�lido.';
end.

