unit uDBException;

interface

uses
  uExceptionEx, Classes, SysUtils;

const
  // o valor da constante est� como duplica para atender o nome em portugu�s(duplicada) e em ingl�s(duplicate)
  UNIQUE_KEY = 'UNIQUE KEY';
  FOREIGN_KEY = 'FOREIGN';

type
  // classe base de todas as exce��es geradas no banco de dados
  // --------------------------------------------------------------------------------
  TDBException = class(TExceptionEx)
  public
    constructor Create(const Msg: string);
    class procedure ParserAndRaise(AMessage, ASQL: string);
  end;


  // classe base de todas as exce��es geradas por n�o encontrar algum objeto no banco de dados
  // --------------------------------------------------------------------------------
  TDBExceptionNotFound = class abstract (TDBException)
  protected
    FObjectName: string;
  public
    constructor CreateFromStream(AStream: TStream); override;
    // controle de envio e recebimento via stream
    procedure WriteToStream(AStream: TStream); override;
  end;

  TDBExceptionTableNotFound = class(TDBExceptionNotFound)
  public
    constructor Create(ATableName: string);
    property TableName: string read FObjectName;
  end;

  TDBExceptionProcedureNotFound = class(TDBExceptionNotFound)
  public
    constructor Create(AProcedureName: string);
    property ProcedureName: string read FObjectName;
  end;

  TDBExceptionConstraintNotFound = class(TDBExceptionNotFound)
  public
    constructor Create(AConstraintName: string);
    property ConstraintName: string read FObjectName;
  end;

  TDBExceptionGeneratorNotFound = class(TDBExceptionNotFound)
  public
    constructor Create(AGeneratorName: string);
    property GeneratorName: string read FObjectName;
  end;

  TDBExceptionColumnNotFound = class(TDBExceptionNotFound)
  public
    constructor Create(AColumnName: string);
    property ColumnName: string read FObjectName;
  end;

  TDBExceptionDuplicatedUniqueKey = class(TDBException)
    constructor Create(AMessage: string);
  end;

  // informa��es duplicadas
  // --------------------------------------------------------------------------------
  TDBExceptionDuplicatedIndex = class(TDBException)
    constructor Create(AMessage: string);
  end;

  TDBExceptionDuplicatedKey = class(TDBException)
  private
    FTableName: string;
    FConstraintName: string;
  public
    // controle da classe
    constructor Create(const ATableName, AConstraintName: string);
    constructor CreateFromStream(AStream: TStream); override;
    // controle de envio e recebimento via stream
    procedure WriteToStream(AStream: TStream); override;
    // properties
    property TableName: string read FTableName;
    property ConstraintName: string read FConstraintName;
  end;

  
  // erros gerais
  // --------------------------------------------------------------------------------
  TDBExceptionDeadlock = class(TDBException)
    constructor Create(AMessage: string);
  end;

  TDBExceptionAccessViolation = class(TDBException)
    constructor Create(AMessage: string);
  end;

  TDBExceptionSintaxError = class(TDBException)
    constructor Create(AMessage: string);
  end;

implementation

uses uHelperTStream, IdGlobal;

{ TDBException }

constructor TDBException.Create(const Msg: string);
begin
  inherited Create(Msg);
  FTypeCode := etDBException;
end;

class procedure TDBException.ParserAndRaise(AMessage, ASQL: string);
var
  TableName: string;
  FieldName: string;
  ObjectName: string;
begin
  if (Pos('lock conflict on no wait transaction', AMessage) > 0) then
    raise TDBExceptionDeadlock.Create(AMessage);

  if (Pos('update conflicts with concurrent update', AMessage) > 0) then
    raise TDBExceptionDeadlock.Create(AMessage);

  // Firebird
  if (Pos('violation of PRIMARY or UNIQUE KEY constraint', AMessage) > 0) then
  begin
    Fetch(AMessage, 'violation of PRIMARY or UNIQUE KEY constraint "');
    ObjectName := Fetch(AMessage, '"');
    Fetch(AMessage, ' on table "');
    TableName := Fetch(AMessage, '"');
    raise TDBExceptionDuplicatedKey.Create(TableName, ObjectName);
  end;

  if Pos('UNIQUE KEY', AMessage) > 0 then
  begin
//    Fetch(AMessage, 'Violation of UNIQUE KEY constraint');
    Raise TDBExceptionDuplicatedUniqueKey.Create(AMessage);
  end;

  // SQLServer - PT
  if (Pos('Viola��o da restri��o PRIMARY KEY', AMessage) > 0) then
  begin
    Fetch(AMessage, 'Viola��o da restri��o PRIMARY KEY ''');
    TableName := Fetch(AMessage, '''');
    Fetch(AMessage, '''');
    ObjectName := Fetch(AMessage, '''');
    raise TDBExceptionDuplicatedKey.Create(TableName, ObjectName);
  end
  else if (Pos('duplicada', AMessage) > 0) then
  begin
    Fetch(AMessage, 'Viola��o da restri��o PRIMARY KEY ''');
    TableName := Fetch(AMessage, '''');
    Fetch(AMessage, '''');
    ObjectName := Fetch(AMessage, '''');
    raise TDBExceptionDuplicatedKey.Create(TableName, ObjectName);
  end;

  // SQLServer - EN
  if (Pos('Violation of PRIMARY KEY constraint', AMessage) > 0) then
  begin
    Fetch(AMessage, 'Violation of PRIMARY KEY constraint ''');
    TableName := Fetch(AMessage, '''');
    Fetch(AMessage, '''');
    ObjectName := Fetch(AMessage, '''');
    raise TDBExceptionDuplicatedKey.Create(TableName, ObjectName);
  end
  else if (Pos('duplicate', AMessage) > 0) then
  begin
    Fetch(AMessage, 'Violation of PRIMARY KEY constraint ''');
    TableName := Fetch(AMessage, '''');
    Fetch(AMessage, '''');
    ObjectName := Fetch(AMessage, '''');
    raise TDBExceptionDuplicatedKey.Create(TableName, ObjectName);
  end;

  if (Pos('violation of FOREIGN KEY constraint', AMessage) > 0) then
  begin
    Fetch(AMessage, 'violation of FOREIGN KEY constraint "');
    ObjectName := Fetch(AMessage, '"');
    Fetch(AMessage, ' on table "');
    TableName := Fetch(AMessage, '"');
    raise TDBExceptionDuplicatedKey.Create(TableName, ObjectName);
  end;

  if Pos('Dynamic SQL Error', AMessage) > 0 then
  begin
    if (Pos('Column unknown', AMessage) > 0) then
    begin
      FieldName := Fetch(AMessage, 'Column unknown');
      raise TDBExceptionColumnNotFound.Create(FieldName);
    end;

    if (Pos('table/view', AMessage) > 0) and (Pos('does not exist', AMessage) > 0) then
    begin
      Fetch(AMessage, 'table/view ');
      TableName := Fetch(AMessage, ' ');
      raise TDBExceptionTableNotFound.Create(TableName);
    end;
  end;

  if (Pos('unsuccessful metadata update', AMessage) > 0) then
  begin
    if (Pos('CONSTRAINT', AMessage) > 0) and (Pos('does not exist', AMessage) > 0) then
    begin
      Fetch(AMessage, 'CONSTRAINT ', True, False);
      ObjectName := Fetch(AMessage, ' ');
      raise TDBExceptionConstraintNotFound.Create(ObjectName);
    end;

    if (Pos('Generator not found', AMessage) > 0) then
    begin
      Fetch(ASQL, 'GENERATOR ', True, False);
      ObjectName := Fetch(ASQL, ' ');
      raise TDBExceptionGeneratorNotFound.Create(ObjectName);
    end;

    if (Pos('attempt to store duplicate value', AMessage) > 0) then
    begin
      raise TDBExceptionDuplicatedIndex.Create(AMessage);
    end;
  end;

  if (Pos('Procedure', AMessage) > 0) and (Pos('not found', AMessage) > 0) then
  begin
    Fetch(AMessage, 'Procedure ');
    TableName := Fetch(AMessage, ' ');
    raise TDBExceptionProcedureNotFound.Create(TableName);
  end;

  if (Pos('Access violation at address', AMessage) > 0) then
    raise TDBExceptionAccessViolation.Create(AMessage);

  if (Pos('Token unknown', AMessage) > 0) then
    raise TDBExceptionSintaxError.Create(AMessage);

  raise TDBException.Create(AMessage);
end;

{ TDBExceptionTableNotFound }

constructor TDBExceptionTableNotFound.Create(ATableName: string);
begin
  inherited Create(Format(MsgTExceptionType[etDBTableNotFound], [ATableName]));
  FTypeCode := etDBTableNotFound;
end;

{ TDBExceptionProcedureNotFound }

constructor TDBExceptionProcedureNotFound.Create(AProcedureName: string);
begin
  inherited Create(Format(MsgTExceptionType[etDBProcedureNotFound], [AProcedureName]));
  FTypeCode := etDBProcedureNotFound;
end;

{ TDBExceptionConstraintNotFound }

constructor TDBExceptionConstraintNotFound.Create(AConstraintName: string);
begin
  inherited Create(Format(MsgTExceptionType[etDBConstraintNotFound], [AConstraintName]));
  FTypeCode := etDBConstraintNotFound;
end;

{ TDBExceptionNotFound }

constructor TDBExceptionNotFound.CreateFromStream(AStream: TStream);
begin
  inherited;

  // atualiza a mensagem do erro
  Self.Message := Format(MsgTExceptionType[FTypeCode], [FObjectName])
end;

procedure TDBExceptionNotFound.WriteToStream(AStream: TStream);
begin
  inherited;

end;

{ TDBExceptionGeneratorNotFound }

constructor TDBExceptionGeneratorNotFound.Create(AGeneratorName: string);
begin
  inherited Create(Format(MsgTExceptionType[etDBGeneratorNotFound], [AGeneratorName]));
  FTypeCode := etDBGeneratorNotFound;
end;

{ TDBExceptionColumnNotFound }

constructor TDBExceptionColumnNotFound.Create(AColumnName: string);
begin
  inherited Create(Format(MsgTExceptionType[etDBColumnNotFound], [AColumnName]));
  FTypeCode := etDBColumnNotFound;
end;

{ TDBDeadlock }

constructor TDBExceptionDeadlock.Create(AMessage: string);
begin
  inherited Create(AMessage);
  FTypeCode := etDBDeadlock;
end;

{ TDBExceptionAccessViolation }

constructor TDBExceptionAccessViolation.Create(AMessage: string);
begin
  inherited Create(AMessage);
  FTypeCode := etDBAccessViolation;
end;

{ TDBExceptionDuplicatedIndex }

constructor TDBExceptionDuplicatedIndex.Create(AMessage: string);
begin
  inherited Create(AMessage);
  FTypeCode := etDBDuplicatedIndex;
end;

{ TDBDuplicatedKey }

constructor TDBExceptionDuplicatedKey.Create(const ATableName, AConstraintName: string);
begin
  inherited Create(Format(MsgTExceptionType[etDBDuplicatedKey], [AConstraintName, ATableName]));
  FTypeCode := etDBDuplicatedKey;
end;

constructor TDBExceptionDuplicatedKey.CreateFromStream(AStream: TStream);
begin
  inherited;

  // atualiza a mensagem do erro
  Self.Message := Format(MsgTExceptionType[etDBDuplicatedKey], [FConstraintName, FTableName]);
end;

procedure TDBExceptionDuplicatedKey.WriteToStream(AStream: TStream);
begin
  inherited;

end;

{ TDBExceptionSintaxError }

constructor TDBExceptionSintaxError.Create(AMessage: string);
begin
  inherited Create(AMessage);
  FTypeCode := etDBExceptionSintaxError;
end;

{ TDBExceptionUniqueKeyDuplicated }

constructor TDBExceptionDuplicatedUniqueKey.Create(AMessage: string);
begin
  inherited Create(AMessage);
  FTypeCode := etDBUniqueKeyDuplicated;
end;

end.
