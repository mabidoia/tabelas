unit uHelperFuncoes;

interface

uses
  Classes, Messages, DateUtils, Controls;

type
  TTipoNameFile = (TNFSegundo = 0, TNFStr = 1, TNFUnixTime = 2);

procedure ExecNewProcess(AProgramName: String; Wait: Boolean);
procedure ExecuteProcess(const ACommandLine, AFolder: string);
procedure ListarDiretorios(ASource: string; ADirList: TStringList);
procedure ListarArquivos(ADiretorio: string; AFileList: TStringList; AIncluirDiretorio: string = ''; AOrdenar: Boolean = true;
  AExtensao: string = 'jpeg'; AFileIgnored: string = 'md5');
procedure StartApp(Acmd: string);
procedure WriteXML(var AListaXML: TStringList; ATag, AEndTag, AValue: string);

function AcrescentarZero(ATexto: string; ATamanho: Integer): string;
function ByteToStr(var Abyte: array of Byte; ASize: Integer): string;
function CreateProcessSimple(Acmd: string; AShow: word): Boolean;
function CompletarString(AValue: string; ATamanho: Integer; ACaracter: string; ATipo: string = 'S'): string;
function ConverterHorarioNomeArquivo(ADateTime: TDateTime): String;
function ConverterNomeArquivoHorario(ANomeArquivo: String; out ACamera: Integer; ATipoArquivoSaida: TTipoNameFile = TNFStr)
  : variant; overload;
function ConverterNomeArquivoHorario(ANomeArquivo: String): variant; overload;
function ConverterNomeArquivoHorario(ANomeArquivo: String; ATipoArquivoSaida: TTipoNameFile): variant; overload;
function DatetimeToUnixTime(datetime: TDateTime; timeZone: Integer = -3): Extended;
function DeletarDiretorio(ADiretorio: string; LeaveFolder: Boolean = false): Boolean;
function FramesToTime(AFrame, AFPS: Integer): string;
function GetDosOutput(LCommandLine: string; Work: string = 'C:\'): string;
function GetUnixTimeFilePkg(ADiretorio: string; AFile: string; AExtensao: string = '.pkg'): Integer;
function IsNumerico(const ATexto: string): Boolean;
function MontaJson(modulo, ID: Integer; ANomeVeiculo: string = ''; ADescricao: string = ''; inicio: double = 0;
  termino: double = 0; ANameFile: string = ''; Delete: Integer = 0; MD5: Boolean = false): string;
function ReadXML(var AListaXML: TStringList; ATag, AEndTag: string): string;
function RemoveExtensao(AFile: string): string;
function SegundosToTime(ASegundos: Integer; AFormatoHora: string = '00:00:00'): string;
function SplitText(AStrInteira, AKey: string): TStringList;
function UnixTimeToDateTime(UnixTime: Real; timeZone: Integer = -3): TDateTime;

// Verifica se o host � de rede local ou externa
function IsHostFromInternalNetwork(AHost: string): Boolean;

implementation

uses
  uLkJSON, SysUtils, Windows, IdGlobal, RegExpr, StrUtils, uHelperLog;

function SplitText(AStrInteira, AKey: string): TStringList;
var
  i: Integer;
  LStr: string;
begin
  Result := TStringList.create;
  for i := 1 to Length(AStrInteira) do
  begin
    if AStrInteira[i] <> AKey then
      LStr := LStr + AStrInteira[i]
    else
    begin
      Result.Add(LStr);
      LStr := '';
    end;
  end;
  Result.Add(LStr);
end;

function ReadXML(var AListaXML: TStringList; ATag, AEndTag: string): string;
var
  i: Integer;
  LPosicao: Integer;
  LStr: string;
begin
  for i := 0 to AListaXML.Count - 1 do
  begin
    LStr := AListaXML[i];
    LPosicao := pos(ATag, LStr);
    if LPosicao > 0 then
    begin
      Result := Copy(LStr, LPosicao + Length(ATag), Length(LStr));
      LPosicao := pos(AEndTag, Result);
      Result := Copy(Result, 0, LPosicao);
    end;
  end;
end;

procedure WriteXML(var AListaXML: TStringList; ATag, AEndTag, AValue: string);
var
  i: Integer;
  LPosicao: Integer;
  LTagPre: string;
  LTagPos: string;
  LStr: string;
  LEncontrou: Boolean;
begin
  LEncontrou := false;
  for i := 0 to AListaXML.Count - 1 do
  begin
    LStr := AListaXML[i];
    LPosicao := pos(ATag, LStr);
    if LPosicao > 0 then
    begin
      LEncontrou := true;
      LTagPre := Copy(LStr, 0, LPosicao + Length(ATag) - 1);
      LTagPos := Copy(LStr, LPosicao + Length(ATag), Length(LStr));

      if Length(AEndTag) > 0 then
      begin
        LPosicao := pos(AEndTag, LTagPos);
        LTagPos := Copy(LTagPos, LPosicao, Length(LStr));
      end
      else
        LTagPos := '';

      AListaXML[i] := LTagPre + AValue + LTagPos;
    end;
  end;
end;

function CreateProcessSimple(Acmd: string; AShow: word): Boolean;
var
  LSUInfo: TStartupInfo;
  LProcInfo: TProcessInformation;
begin
  FillChar(LSUInfo, SizeOf(LSUInfo), #0);
  LSUInfo.cb := SizeOf(LSUInfo);
  LSUInfo.dwFlags := STARTF_USESHOWWINDOW;
  LSUInfo.wShowWindow := AShow;

  // Result := CreateProcess(nil, PChar(cmd), nil, nil, true, CREATE_NEW_CONSOLE or NORMAL_PRIORITY_CLASS, nil, nil, SUInfo, ProcInfo);
  Result := CreateProcess(nil, PChar(Acmd), nil, nil, true, CREATE_THREAD_DEBUG_EVENT or NORMAL_PRIORITY_CLASS, nil, nil, LSUInfo,
    LProcInfo);

  if (Result) then
  begin
    WaitForSingleObject(LProcInfo.hProcess, INFINITE);

    CloseHandle(LProcInfo.hProcess);
    CloseHandle(LProcInfo.hThread);
  end;
end;

function CompletarString(AValue: string; ATamanho: Integer; ACaracter, ATipo: string): string;
var
  i: Integer;
begin
  Result := '';
  if ACaracter = '' then
    ACaracter := #32;

  ATamanho := ATamanho - Length(AValue) + 1;

  if ATipo = 'S' then
  begin
    for i := 1 to ATamanho do
    begin
      if i = 1 then
        Result := Result + AValue
      else
        Result := Result + ACaracter;
    end;
  end
  else if ATipo = 'N' then
  begin
    for i := 1 to ATamanho do
    begin
      if i = ATamanho then
        Result := Result + AValue
      else
        Result := Result + ACaracter;
    end;
  end;
end;

function DatetimeToUnixTime(datetime: TDateTime; timeZone: Integer): Extended;
var
  LDia, LMes, LAno, LHora, LMin, LSeg: Integer;
  LAux: Extended;
begin
  LDia := StrToInt(FormatDateTime('D', datetime));
  LMes := StrToInt(FormatDateTime('M', datetime));
  LAno := StrToInt(FormatDateTime('YYYY', datetime));
  LHora := StrToInt(FormatDateTime('H', datetime));
  LMin := StrToInt(FormatDateTime('M', datetime));
  LSeg := StrToInt(FormatDateTime('S', datetime));

  Result := (EncodeDate(LAno, LMes, LDia) - EncodeDate(1970, 1, 1)) * 86400;
  LAux := (LHora * 3600) + (LMin * 60) + LSeg;
  Result := Result + LAux - (timeZone * 3600);
end;

function UnixTimeToDateTime(UnixTime: Real; timeZone: Integer): TDateTime;
begin
  Result := EncodeDate(1970, 1, 1) + ((UnixTime + timeZone * 3600) / 86400); { 86400=No. de segundos por dia }
end;

function ByteToStr(var Abyte: array of Byte; ASize: Integer): string;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to ASize - 1 do
  begin
    Result := Result + char(Abyte[i]);
    Abyte[i] := 0;
  end;
end;

procedure ExecuteProcess(const ACommandLine, AFolder: string);
var
  SA: TSecurityAttributes;
  SI: TStartupInfo;
  PI: TProcessInformation;
  LStdOutPipeRead, StdOutPipeWrite: THandle;
  WasOK: Boolean;
  LBuffer: array [0 .. 255] of char;
  LBytesRead: Cardinal;
  LWorkDir, LLine: String;
begin
  with SA do
  begin
    nLength := SizeOf(SA);
    bInheritHandle := true;
    lpSecurityDescriptor := nil;
  end;

  with SI do
  begin
    FillChar(SI, SizeOf(SI), 0);
    cb := SizeOf(SI);
    dwFlags := STARTF_USESHOWWINDOW or STARTF_USESTDHANDLES;
    wShowWindow := SW_HIDE;
  end;

  LWorkDir := ExtractFilePath(ACommandLine);
  WasOK := CreateProcess(nil, PChar(ACommandLine), nil, nil, true, CREATE_NEW_CONSOLE or NORMAL_PRIORITY_CLASS, nil,
    PChar(AFolder), SI, PI);
end;

function MontaJson(modulo, ID: Integer; ANomeVeiculo: string; ADescricao: string; inicio: double; termino: double;
  ANameFile: string; Delete: Integer; MD5: Boolean): string;
var
  LUlkJson: TlkJSONobject;
begin
  LUlkJson := TlkJSONobject.create;
  try
    LUlkJson.Add('MODULO', modulo);

    if Length(ANomeVeiculo) > 0 then
      LUlkJson.Add('NOME', ANomeVeiculo);

    if Length(ADescricao) > 0 then
      LUlkJson.Add('DESCRICAO', ADescricao);

    LUlkJson.Add('ID', ID);
    LUlkJson.Add('CMD', 'REQUEST');

    if (inicio > 0) and (termino > 0) then
    begin
      LUlkJson.Add('INICIO', inicio);
      LUlkJson.Add('TERMINO', termino);
    end;

    if Delete = 1 then
      LUlkJson.Add('DELETE', Delete);

    if MD5 then
    begin
      LUlkJson.Add('STREAM', 1);
      LUlkJson.Add('MD5', 1);
    end;

    if Length(Trim(ANameFile)) > 0 then
    begin
      LUlkJson.Add('STREAM', 1);
      LUlkJson.Add('SIMPLE', ANameFile);
    end;

    Result := TlkJSON.GenerateText(LUlkJson);
  finally
    LUlkJson.Free;
  end;
end;

function DeletarDiretorio(ADiretorio: string; LeaveFolder: Boolean): Boolean;
var
  LListaDiretorios: TStringList;
  i: Integer;

  function ApagaPasta(APasta: string): Boolean;
  var
    LArquivo: TSearchRec;
  begin
    if FindFirst(APasta + '*.*', 0, LArquivo) = 0 then
    begin
      repeat
        DeleteFile(PChar(APasta + LArquivo.Name));
      until FindNext(LArquivo) <> 0;
      SysUtils.FindClose(LArquivo);
    end;
    if not RemoveDir(APasta) then
      DeletarDiretorio(APasta);

    // Result := RemoveDir(APasta);
  end;

begin
  Result := false;
  if ADiretorio[Length(ADiretorio)] <> '\' then
    ADiretorio := ADiretorio + '\';

  LListaDiretorios := TStringList.create;
  ListarDiretorios(ADiretorio + '*.*', LListaDiretorios);
  for i := 0 to LListaDiretorios.Count - 1 do
    Result := ApagaPasta(ADiretorio + LListaDiretorios[i] + '\');

  if not LeaveFolder then
    Result := ApagaPasta(ADiretorio);

  FreeAndNil(LListaDiretorios);
end;

procedure ListarArquivos(ADiretorio: string; AFileList: TStringList; AIncluirDiretorio: string; AOrdenar: Boolean;
  AExtensao: string; AFileIgnored: string);
var
  LSearchRec: TSearchRec;
  LResult: Integer;
begin
  LResult := FindFirst(ADiretorio + '\' + AIncluirDiretorio + '\*.' + AExtensao, faAnyFile, LSearchRec);

  if LResult = 0 then
    while (LResult = 0) do
    begin
      if (LSearchRec.Name + ' ')[1] = '.' then
      begin
        LResult := FindNext(LSearchRec);
        Continue;
      end;

      // if LowerCase( SearchRec.Name ) <> LowerCase( AFileIgnored )    then
      if (pos(LowerCase(AFileIgnored), LowerCase(LSearchRec.Name)) = 0) and (pos('.OK', UpperCase(LSearchRec.Name)) = 0) then
        if AIncluirDiretorio <> '' then
          AFileList.Add(AIncluirDiretorio + '\' + LSearchRec.Name)
        else
          AFileList.Add(LSearchRec.Name);

      LResult := FindNext(LSearchRec);
    end;
  FindClose(LSearchRec.FindHandle);

  if AOrdenar then
    AFileList.Sort;
end;

function ConverterNomeArquivoHorario(ANomeArquivo: String): variant;
var
  LCamera: Integer;
begin
  Result := ConverterNomeArquivoHorario(ANomeArquivo, LCamera);
end;

function ConverterNomeArquivoHorario(ANomeArquivo: String; ATipoArquivoSaida: TTipoNameFile): variant;
var
  LCamera: Integer;
begin
  Result := ConverterNomeArquivoHorario(ANomeArquivo, LCamera, ATipoArquivoSaida);
end;

function ConverterNomeArquivoHorario(ANomeArquivo: String; out ACamera: Integer; ATipoArquivoSaida: TTipoNameFile): variant;
var
  LDiretorio, LArquivo: string;
  LStrSplit: TStringList;
begin
  ACamera := 0;
  if pos('\', ANomeArquivo) > 0 then
  begin
    LDiretorio := Fetch(ANomeArquivo, '\');
    ANomeArquivo := Fetch(ANomeArquivo, '\');
  end;

  ANomeArquivo := RemoveExtensao(ANomeArquivo);

  LStrSplit := SplitText(ANomeArquivo, '-');

  if LStrSplit.Count > 1 then
  begin
    ANomeArquivo := LStrSplit[0];

    if LStrSplit.Count > 2 then
      ACamera := StrToInt(LStrSplit[2])
    else
      ACamera := 0;
  end;
  FreeAndNil(LStrSplit);

  if ATipoArquivoSaida = TNFStr then
    Result := DateTimeToStr(UnixTimeToDateTime(StrToInt(ANomeArquivo)))
  else if ATipoArquivoSaida = TNFUnixTime then
    Result := StrToInt(ANomeArquivo)
  else
    Result := SecondOfTheDay(UnixTimeToDateTime(StrToInt(ANomeArquivo)));
end;

function ConverterHorarioNomeArquivo(ADateTime: TDateTime): String;
var
  LHour, LMin, LSec, LMSec, LYear, LMonth, LDay: word;
  LSHour, LSMin, LSSec, LSYear, LSMonth, LSDay: string;
begin
  DecodeTime(ADateTime, LHour, LMin, LSec, LMSec);
  DecodeDate(ADateTime, LYear, LMonth, LDay);

  LSHour := IntToStr(LHour);
  if Length(LSHour) = 1 then
    LSHour := '0' + LSHour;

  LSMin := IntToStr(LMin);
  if Length(LSMin) = 1 then
    LSMin := '0' + LSMin;

  LSSec := IntToStr(LSec);
  if Length(LSSec) = 1 then
    LSSec := '0' + LSSec;

  LSYear := IntToStr(LYear);

  LSMonth := IntToStr(LMonth);
  if Length(LSMonth) = 1 then
    LSMonth := '0' + LSMonth;

  LSDay := IntToStr(LDay);
  if Length(LSDay) = 1 then
    LSDay := '0' + LSDay;

  Result := LSYear[3] + LSYear[4] + LSMonth + LSDay + LSHour + LSMin + LSSec;
end;

function RemoveExtensao(AFile: string): string;
var
  LExtensao: string;
begin
  Result := ReverseString(AFile);
  if pos('.', Result) > 1 then
    LExtensao := Fetch(Result, '.');
  Result := ReverseString(Result);
end;

procedure StartApp(Acmd: string);
var
  LProcessInformation: TProcessInformation;
  LStartUpInfo: TStartupInfo;
  LReturnVal: LongBool;
begin
  LStartUpInfo.cb := SizeOf(TStartupInfo);
  LStartUpInfo.lpReserved := nil;
  LStartUpInfo.lpDesktop := nil;
  LStartUpInfo.lpTitle := nil;
  LStartUpInfo.dwFlags := STARTF_USESHOWWINDOW;
  LStartUpInfo.wShowWindow := SW_SHOWNORMAL;

  LReturnVal := CreateProcess(nil, PChar(Acmd), nil, nil, true, NORMAL_PRIORITY_CLASS, nil, nil, LStartUpInfo, LProcessInformation);
end;

procedure ExecNewProcess(AProgramName: String; Wait: Boolean);
var
  LStartInfo: TStartupInfo;
  LProcInfo: TProcessInformation;
  LCreateOK: Boolean;
begin
  FillChar(LStartInfo, SizeOf(TStartupInfo), #0);
  FillChar(LProcInfo, SizeOf(TProcessInformation), #0);
  LStartInfo.cb := SizeOf(TStartupInfo);
  LStartInfo.wShowWindow := SW_SHOW;

  LCreateOK := CreateProcess(nil, PChar(AProgramName), nil, nil, false, CREATE_NEW_PROCESS_GROUP + NORMAL_PRIORITY_CLASS, nil,
    nil, LStartInfo, LProcInfo);

  if LCreateOK then
  begin
    if Wait then
      WaitForSingleObject(LProcInfo.hProcess, INFINITE);
  end;
  CloseHandle(LProcInfo.hProcess);
  CloseHandle(LProcInfo.hThread);
end;

function FramesToTime(AFrame, AFPS: Integer): string;
var
  LHr, LMn, LSe: double;
begin
  try
    LHr := (AFrame div 3) div 3600;
    LMn := (AFrame div 3) div 60;
    LSe := (AFrame div 3) mod 60;
    Result := FormatFloat('00:00:00', StrToInt(AcrescentarZero(FloatToStr(LHr), 2) + AcrescentarZero(FloatToStr(LMn),
      2) + AcrescentarZero(FloatToStr(LSe), 2)));
  except
    Result := '00:00:00';
  end;
end;

function AcrescentarZero(ATexto: string; ATamanho: Integer): string;
var
  i: Integer;
begin
  Result := '';
  for i := 1 to ATamanho - Length(ATexto) do
    Result := Result + '0';
  Result := Result + ATexto;
end;

function SegundosToTime(ASegundos: Integer; AFormatoHora: string): string;
var
  LHr, LMn, LSe: double;
  LDuracaoSegundos: double;
begin
  try
    Result := '';
    LHr := 0;
    LMn := 0;
    LSe := 0;
    LDuracaoSegundos := ASegundos;

    LSe := Trunc(LDuracaoSegundos) mod 60;
    LDuracaoSegundos := Trunc(LDuracaoSegundos) / 60;
    LMn := Trunc(LDuracaoSegundos) mod 60;
    LDuracaoSegundos := Trunc(LDuracaoSegundos) / 60;
    LHr := Trunc(LDuracaoSegundos) mod 24;
    Result := FormatFloat(AFormatoHora, StrToInt(AcrescentarZero(FloatToStr(LHr), 2) + AcrescentarZero(FloatToStr(LMn),
      2) + AcrescentarZero(FloatToStr(LSe), 2)));
  except
    Result := '00:00:00';
  end;
end;

function GetUnixTimeFilePkg(ADiretorio: string; AFile: string; AExtensao: string): Integer;
var
  LArquivo: TFileStream;
  LTamanho: Integer;
  LSegundo: Smallint;
begin
  try
    Result := 0;
    try
      LArquivo := TFileStream.create(ADiretorio + '\' + AFile + AExtensao, fmOpenRead or fmShareDenyNone);
      if LArquivo.Size > 6 then
      begin
        LArquivo.Read(LTamanho, 4);
        LArquivo.Read(LSegundo, 2);

        Result := StrToInt(AFile) + LSegundo;
      end;
    finally
      FreeAndNil(LArquivo);
    end;
  except
    on E: exception do
      Logger.Error('GetUnixTimeFilePkg', nil, E);
  end;
end;

function GetDosOutput(LCommandLine: string; Work: string): string;
var
  SA: TSecurityAttributes;
  SI: TStartupInfo;
  PI: TProcessInformation;
  LStdOutPipeRead, LStdOutPipeWrite: THandle;
  WasOK, Handle: Boolean;
  LBuffer: array [0 .. 255] of AnsiChar;
  LBytesRead: Cardinal;
  LWorkDir: string;
begin
  Result := '';
  with SA do
  begin
    nLength := SizeOf(SA);
    bInheritHandle := true;
    lpSecurityDescriptor := nil;
  end;
  CreatePipe(LStdOutPipeRead, LStdOutPipeWrite, @SA, 0);
  try
    with SI do
    begin
      FillChar(SI, SizeOf(SI), 0);
      cb := SizeOf(SI);
      dwFlags := STARTF_USESHOWWINDOW or STARTF_USESTDHANDLES;
      wShowWindow := SW_HIDE;
      hStdInput := GetStdHandle(STD_INPUT_HANDLE); // don't redirect stdin
      hStdOutput := LStdOutPipeWrite;
      hStdError := LStdOutPipeWrite;
    end;
    LWorkDir := Work;
    Handle := CreateProcess(nil, PChar('cmd.exe /C ' + LCommandLine), nil, nil, true, 0, nil, PChar(LWorkDir), SI, PI);
    CloseHandle(LStdOutPipeWrite);
    if Handle then
      try
        repeat
          WasOK := ReadFile(LStdOutPipeRead, LBuffer, 255, LBytesRead, nil);
          if LBytesRead > 0 then
          begin
            LBuffer[LBytesRead] := #0;
            Result := Result + LBuffer;
          end;
        until not WasOK or (LBytesRead = 0);
        WaitForSingleObject(PI.hProcess, INFINITE);
      finally
        CloseHandle(PI.hThread);
        CloseHandle(PI.hProcess);
      end;
  finally
    CloseHandle(LStdOutPipeRead);
  end;
end;

function IsNumerico(const ATexto: string): Boolean;
begin
  Result := True;
  try
    StrToInt64(ATexto);
  except
    Result := False;
  end;
end;

function IsHostFromInternalNetwork(AHost: string): Boolean;
const
  REGEX_REDE_LOCAL = '(^10\.)|(^172\.1[6-9]\.)|(^172\.2[0-9]\.)|(^172\.3[0-1]\.)|(^192\.168\.)|(^127\.0\.0\.1)';
begin
  Result := (LowerCase(AHost) = 'localhost') or ExecRegExpr(REGEX_REDE_LOCAL, AHost);
end;

procedure ListarDiretorios(ASource: string; ADirList: TStringList);
var
  LSearchRec: TSearchRec;
  LResult: Integer;
begin
  LResult := FindFirst(ASource, faAnyFile, LSearchRec);

  if LResult = 0 then
    while (LResult = 0) do
    begin
      if (LSearchRec.Name + ' ')[1] = '.' then
      begin
        LResult := FindNext(LSearchRec);
        Continue;
      end;

      ADirList.Add(LSearchRec.Name);
      LResult := FindNext(LSearchRec);
    end;

  SysUtils.FindClose(LSearchRec);
  ADirList.Sort;
end;

end.
