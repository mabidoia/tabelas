﻿unit uHelperScheduler;

interface

uses
  Classes, uHelperLog, SysUtils, uHelperThread, uHelperClasses;

type
  TProcedure = procedure of object;

  // Timer implementado usando thread que permite executar
  // qualquer procedimento de classe em cada intervalo
  PThreadTimer = ^TThreadTimer;
  TThreadTimer = class(TThreadVeltrac)
  private
    FInterval: Cardinal;
    FMethod: TProcedure;
    FDescription: string;
    FIsSynchronized: Boolean;
  protected
    procedure Execute; override;
  public
    // controle da classe
    constructor Create(AInterval: Cardinal; AMethod: TProcedure; ADescription: string; AIsSynchronized: Boolean);
    // properties
    property Interval: Cardinal read FInterval write FInterval;
    property Method: TProcedure read FMethod;
    property Description: string read FDescription;
    property IsSynchronized: Boolean read FIsSynchronized;
  end;

  // controle das instâncias do TThreadTimer
  THelperScheduler = class
  private
    FListThreadTimer: TList;
  protected
  public
    // controle de classe
    constructor Create;
    destructor Destroy; override;
    // controle de agendamento
    procedure ToSchedule(AInterval: Cardinal; AMethod: TProcedure; ADescription: string; AIsSynchronized: Boolean = False);
  end;

var
  HelperScheduler: THelperScheduler;

implementation

uses
  IdGlobal, Windows, Forms;

{ THelperScheduler }

constructor THelperScheduler.Create;
begin
  Logger.Info('Loading THelperScheduler.Create');
  FListThreadTimer := TList.Create;
end;

destructor THelperScheduler.Destroy;
var
  i: Integer;
  ThreadTimer: PThreadTimer;
begin
//  for i := 0 to FListThreadTimer.Count - 1 do
//  begin
//    // para a thread e libera a memória do record
//    ThreadTimer := FListThreadTimer.Items[i];
//    ThreadTimer^.Terminate;
//    Dispose(ThreadTimer);
//  end;
  // libera a lista de método/thread
  FListThreadTimer.Free;

  inherited;
end;

procedure THelperScheduler.ToSchedule(AInterval: Cardinal; AMethod: TProcedure; ADescription: string; AIsSynchronized: Boolean = False);
var
  ThreadTimer: TThreadTimer;
begin
  ThreadTimer := TThreadTimer.Create(AInterval, AMethod, ADescription, AIsSynchronized);
  Logger.Info('Create thread: ' + ADescription);
  FListThreadTimer.Add(ThreadTimer);
  ThreadTimer.Resume;
end;

{ TThreadTimer }

constructor TThreadTimer.Create(AInterval: Cardinal; AMethod: TProcedure; ADescription: string; AIsSynchronized: Boolean);
begin
  inherited Create(True);
  FreeOnTerminate := True;

  // seta o intervalo do timer e o método que será executado a cada 'tick'
  FInterval := AInterval;
  FMethod := AMethod;
  FDescription := ADescription;
  FIsSynchronized := AIsSynchronized;
end;

procedure TThreadTimer.Execute;
var
  LGetTickCount: Cardinal;
begin
  while (not Application.Terminated) and (not Terminated) {$IFDEF SERVIDOR} and (not TThreadVeltrac.RemoverThreads) {$ENDIF} do
  begin
    try
      Logger.Info('TThreadTimer.Executing[%s]', [FDescription]);
      LGetTickCount := GetTickCount;
      if FIsSynchronized then
        Synchronize(FMethod)
      else
        FMethod;
      Logger.Info('TThreadTimer.Executed[%s | Seconds Spent %2.2f]: %s', [FDescription, (GetTickCount - LGetTickCount) / 1000, 'Execute process.']);

      {$IFDEF SERVIDOR}
      SleepNoLock(FInterval);
      {$ELSE}
      Sleep(FInterval);
      {$ENDIF}
    except
      on E: Exception do
      begin
        Logger.ErroStackTrace(Self, @E);
        Logger.Error('TThreadTimer.Execute[%s]', [FDescription], Self, E);
        {$IFDEF SERVIDOR}
        SleepNoLock(FInterval);
        {$ELSE}
        Sleep(FInterval);
        {$ENDIF}
      end;
    end;
  end;
  Logger.Warn('Finalize ' + FDescription);
  Terminate;
end;


end.
