unit uHelperThread;

interface

uses
  Classes, SysUtils, TlHelp32, Windows, SyncObjs;

type
  ExceptionTerminatedApplication = class(Exception)
  public
    constructor Create;
  end;

  TThreadVeltrac = class(TThread)
  strict private
    FCreationTime: TDateTime;
    FForceExecute: Boolean;

    procedure AdicionarThread;
    procedure RemoverThread;
    procedure AfterConstruction; override;
  protected
    function ExitSleep: Boolean; virtual;
    procedure SleepNoLock(const ATime: Cardinal); virtual;

    procedure SleepSegundos(Segundos: Integer);
    procedure SleepMinutos(Minutos: Integer);
    procedure SleepHoras(Horas: Integer);

    procedure Execute; override;
  public
    // controle da classe
    constructor Create(CreateSuspended: Boolean);
    destructor Destroy; override;
    // controle das threads
    class var RemoverThreads: Boolean;
    class var ThreadsRemovidas: Boolean;
    class procedure RemoverAllThreads;

    class function CountThreads: Integer;

    property CreationTime: TDateTime read FCreationTime;
    property ForceExecute: Boolean read FForceExecute write FForceExecute;

    function CanWork: Boolean;

    procedure CheckAbort;
  end;

  function OpenThread(dwDesiredAccess: DWORD; bInheritHandle: BOOL; dwThreadId: DWORD): THandle; stdcall;
  procedure PostThreadsToVRay;
  function IsThreadAlive(AThreadId: Integer): Boolean;

  procedure AddThread(AThreadId: Cardinal; ADescription: string; AAddToLog: Boolean = true);
  procedure RemoveThread(AThreadId: Cardinal; AAddToLog: Boolean = true);

implementation

uses
  IdGlobal, uIntegerList, SvcMgr, Forms,uHelperLog, uDBPoolConnection;

var
  uRCListaThreads: TCriticalSection;
  uListaThreads: TIntegerList;
  uThreadControlActive: Boolean;

type

  TThreadControleThreads = class(TThreadVeltrac)
  protected
    constructor Create;
    procedure Execute; override;
  public
    procedure PostThreadsToVRay;
  end;

var
  uThreadControleThreads: TThreadControleThreads;

procedure AddThread(AThreadId: Cardinal; ADescription: string; AAddToLog: Boolean = true);
begin
  if not uThreadControlActive then
    Exit;

  uRCListaThreads.Acquire;
  try
    uListaThreads.AddObject(AThreadId, nil);

    if (Logger <> nil) and (AAddToLog) then
      Logger.Debug('TThreadVeltrac.AdicionarThread: %s  ThreadID: %d',[ADescription, AThreadId]);
  finally
    uRCListaThreads.Release;
  end;
end;

procedure RemoveThread(AThreadId: Cardinal; AAddToLog: Boolean = true);
var
  LIndex: Integer;
begin
  if not uThreadControlActive then
    Exit;

  if uListaThreads = nil then
    Exit;

  try
    uRCListaThreads.Acquire;
    try
      LIndex := uListaThreads.IndexOf(AThreadId);
      if LIndex <> -1 then
      begin
        uListaThreads.Delete(LIndex);

      if (Logger <> nil) and AAddToLog then
        Logger.Debug('TThreadVeltrac.RemoverThread: %d ', [AThreadId]);

      end;
    finally
      uRCListaThreads.Release;
    end;
    {$IFDEF SERVIDOR}
    DBPoolConnection.NotifyRemovedThread(AThreadId);
    {$ENDIF}
  except
    on E: Exception do
      Logger.Error('TThreadVeltrac.RemoverThread ' + E.Message);
  end;
end;

function OpenThread; external kernel32 name 'OpenThread';

function IsThreadAlive(AThreadId: Integer): Boolean;
var
  I: Integer;
begin
  uRCListaThreads.Acquire;
  try
    Result := False;
    for I := 0 to uListaThreads.Count -1 do
      if uListaThreads.Items[I] = AThreadId then
      begin
        Result := True;
        Exit;
      end;
  finally
    uRCListaThreads.Release;
  end;
end;

procedure PostThreadsToVRay;
begin
  uThreadControleThreads.PostThreadsToVRay;
end;

procedure TThreadVeltrac.AdicionarThread;
begin
  if not CanWork then
    Exit;

  uRCListaThreads.Acquire;
  try
    uListaThreads.AddObject(ThreadID, Self);


      
    if Logger <> nil then
      Logger.Debug('TThreadVeltrac.AdicionarThread: %s  ThreadID: %d',[Self.ClassName,Self.ThreadID]);
  finally
    uRCListaThreads.Release;
  end;
end;

procedure TThreadVeltrac.AfterConstruction;
begin
  inherited;
  AdicionarThread;  
end;

function TThreadVeltrac.CanWork: Boolean;
begin
  Result := not ((TThreadVeltrac.RemoverThreads) or (Self.Terminated) or (Application.Terminated));
  {$IFDEF CLIENTE}
  if Result and (ControleSystem <> nil) then
    Result := not (ControleSystem.FechandoCliente);
  {$ENDIF}
end;

procedure TThreadVeltrac.CheckAbort;
begin
  if not CanWork then
    Abort;
end;

class function TThreadVeltrac.CountThreads: Integer;
begin
  Result := uListaThreads.Count -1;
end;

constructor TThreadVeltrac.Create(CreateSuspended: Boolean);
begin
  if RemoverThreads then
    Exit;

  inherited Create(True);

  FCreationTime := Now;

  if not CreateSuspended then
    Resume;
end;

destructor TThreadVeltrac.Destroy;
var
  LThreadName: string;
  LThreadId: Cardinal;
begin
  try
    if (Logger <> nil) then
    begin

        LThreadName := ClassName;

      LThreadId := ThreadID;
    end;

    RemoverThread;
    inherited;
  except
  end;
end;

procedure TThreadVeltrac.Execute;
begin
  NameThreadForDebugging(Self.ClassName, Self.ThreadID);
end;

function TThreadVeltrac.ExitSleep: Boolean;
begin
  Result := False;
end;

class procedure TThreadVeltrac.RemoverAllThreads;
var
  hSnapShot: THandle;
  ThInfo: TThreadEntry32;
  ThreadHandle: DWord;
  LPId: Cardinal;
const
  THREAD_TERMINATE = $0001;
begin
  TThreadVeltrac.RemoverThreads := True;
  Exit; //N�o � pra for�ar mais, deixa finalizar certo, depois volta isso
  hSnapShot:= CreateToolHelp32Snapshot(TH32CS_SNAPTHREAD, 0);
  LPId := GetCurrentProcessId;
  if hSnapShot <> INVALID_HANDLE_VALUE then
  begin
    ThInfo.dwSize:= SizeOf(ThInfo);
    if (Thread32First(hSnapshot, ThInfo)) then
    begin
      while (Thread32Next(hSnapshot, ThInfo)) do
        if (ThInfo.th32OwnerProcessID = LPId) and
          (ThInfo.th32ThreadID <> LPId) then
        begin
          try
            ThreadHandle := OpenThread(THREAD_TERMINATE, True,ThInfo.th32ThreadID);
            try
              TerminateThread(ThreadHandle,0);
            finally
              CloseHandle(ThreadHandle);
            end;
          except
          end;
        end;
    end;
    CloseHandle(hSnapshot);
  end;
end;

procedure TThreadVeltrac.RemoverThread;
var
  LIndex: Integer;
begin
  if (not CanWork) or (uListaThreads = nil) then
    Exit;

  try
    uRCListaThreads.Acquire;
    try
      LIndex := uListaThreads.IndexOf(ThreadID);
      if LIndex <> -1 then
      begin
        uListaThreads.Delete(LIndex);

      if Logger <> nil then
        Logger.Debug('TThreadVeltrac.RemoverThread: ' + Self.ClassName);


      end;
    finally
      uRCListaThreads.Release;
    end;
    {$IFDEF SERVIDOR}
    DBPoolConnection.NotifyRemovedThread(ThreadID);
    {$ENDIF}
  except
    on E: Exception do
      Logger.Error('TThreadVeltrac.RemoverThread ' + E.Message);
  end;
end;

procedure TThreadVeltrac.SleepHoras(Horas: Integer);
begin
  SleepMinutos(Horas * 60);
end;

procedure TThreadVeltrac.SleepMinutos(Minutos: Integer);
begin
  SleepSegundos(Minutos * 60);
end;

procedure TThreadVeltrac.SleepNoLock(const ATime: Cardinal);
const
  TIME_TO_SLEEP = 1000;
var
  Fracao: Cardinal;
  Resto: Cardinal;
  i: Cardinal;
begin
  if ATime < 1000 then
  begin
    Sleep(ATime);
    Exit;
  end;

  Fracao := ATime div TIME_TO_SLEEP;
  Resto  := ATime mod TIME_TO_SLEEP;

  for i := Fracao - 1 downto 0 do
  begin
    Sleep(TIME_TO_SLEEP);
    if (TThreadVeltrac.RemoverThreads) or (Self.Terminated) or (Application.Terminated) then
    begin
      Terminate;
      Exit;
    end
    else if Terminated or (ExitSleep) or FForceExecute then
    begin
      FForceExecute := False;
      Exit;
    end;
  end;

  Sleep(Resto);
end;

procedure TThreadVeltrac.SleepSegundos(Segundos: Integer);
begin
  SleepNoLock(Segundos * 1000);
end;

{ TThreadControleThreads }

constructor TThreadControleThreads.Create;
begin
  FreeOnTerminate := True;
  inherited Create(False);
end;

procedure TThreadControleThreads.Execute;
begin
  Sleep(2000);
  while not Terminated do
  begin
    try
      Sleep(1000);
    except
      on E: exception do
        Logger.Error('Erro no TThreadControleThreads.Execute', Self, e);
    end;
  end;
end;

procedure TThreadControleThreads.PostThreadsToVRay;
var
  I: Integer;
  LThread: TThreadVeltrac;
begin
  uRCListaThreads.Acquire;
  try

  finally
    uRCListaThreads.Release;
  end;
end;

{ ExceptionTerminatedApplication }

constructor ExceptionTerminatedApplication.Create;
begin
  inherited Create('Aplica��o foi encerrada.')
end;

initialization
  TThreadVeltrac.RemoverThreads := False;
  TThreadVeltrac.ThreadsRemovidas := False;
  uRCListaThreads := TCriticalSection.Create;
  uListaThreads := TIntegerList.Create;
  uListaThreads.Sort;
  uListaThreads.Sorted := True;
  {$IFNDEF RCITY}
  uThreadControleThreads := TThreadControleThreads.Create;
  {$ENDIF}
  uThreadControlActive := True;

end.
