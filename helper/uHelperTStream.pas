﻿unit uHelperTStream;

interface

uses
  Classes, Generics.Collections, uIntegerList, uUtil;

type
  TStreamEx = class helper for TStream
    procedure WriteOk;
    procedure WriteError;

    procedure WriteString(const S: string);
    function ReadString: string;

    procedure WriteList<T: record>(AList: TList<T>);
    procedure ReadList<T: record>(out AList: TList<T>);
    procedure LoadIntoList<T: record>(AList: TList<T>);
    procedure WriteArray<T>(AArray: TArray<T>);
    procedure ReadArray<T>(out AArray: TArray<T>);

    procedure WriteIntegerList(AList: TIntegerList);
    function ReadIntegerList: TIntegerList;

  end;

implementation

type
  // tipo da mensagem de retorno
  TMsgResponse = (
    mrOK,     // processado com sucesso. Retorna o resultado a ação executada quando houver
    mrERROR   // erro ao processar a ação solicitada. Retorna o ErrorCode no stream
  );

var
  RESPONSE_OK: TMsgResponse;
  RESPONSE_ERROR: TMsgResponse;

{ TStreamEx }

function TStreamEx.ReadString: string;
var
  len: integer;
  iString: UTF8String;
begin
  Result := '';
  Self.ReadBuffer(len, 4);
  if len > 0 then
  begin
    SetLength(iString, len);
    Self.ReadBuffer(iString[1], len);
    Result := string(iString);
  end;
end;



procedure TStreamEx.WriteError;
begin
  Self.Write(RESPONSE_ERROR, SizeOf(TMsgResponse));
end;

procedure TStreamEx.WriteIntegerList(AList: TIntegerList);
var
  ItemsCount, Value, I: Integer;
begin
  if AList = nil then
  begin
    ItemsCount := 0;
    Self.Write(ItemsCount, SizeOf(ItemsCount));
    Exit;
  end;

  ItemsCount := AList.Count;
  Self.Write(ItemsCount, SizeOf(ItemsCount));

  for I := 0 to ItemsCount - 1 do
  begin
    Value := AList.Items[I];
    Self.Write(Value, SizeOf(Value));
  end;
end;

procedure TStreamEx.WriteList<T>(AList: TList<T>);
var
  LRecordsCount, I: Integer;
  LRecord: T;
begin
  if AList = nil then
  begin
    LRecordsCount := 0;
    Self.Write(LRecordsCount, SizeOf(LRecordsCount));
    Exit;
  end;
  LRecordsCount := AList.Count;
  Self.Write(LRecordsCount, SizeOf(LRecordsCount));
  for I := 0 to AList.Count - 1 do
  begin
    LRecord := AList.Items[I];
    Self.Write(LRecord, SizeOf(T));
  end;
end;

procedure TStreamEx.WriteArray<T>(AArray: TArray<T>);
var
  LCount: Integer;
  LItem: T;
  I: Integer;
begin
  LCount := Length(AArray);
  Self.Write(LCount, SizeOf(LCount));
  for I := 0 to LCount - 1 do
  begin
    LItem := AArray[I];
    Self.Write(LItem, SizeOf(LItem));
  end;
end;



function TStreamEx.ReadIntegerList: TIntegerList;
var
  ItemsCount, Value, I: Integer;
begin
  Result := TIntegerList.Create;
  Result.Sorted := True;

  Self.Read(ItemsCount, SizeOf(ItemsCount));

  for I := ItemsCount - 1 downto 0 do
  begin
    Self.Read(Value, SizeOf(Value));
    Result.Add(Value);
  end;
end;

procedure TStreamEx.ReadList<T>(out AList: TList<T>);
var
  LRecordsCount, I: Integer;
  LRecord: T;
begin
  AList := TList<T>.Create;
  Self.Read(LRecordsCount, SizeOf(LRecordsCount));
  for I := LRecordsCount - 1 downto 0 do
  begin
    Self.Read(LRecord, SizeOf(T));
    AList.Add(LRecord);
  end;
end;

procedure TStreamEx.LoadIntoList<T>(AList: TList<T>);
var
  LRecordsCount, I: Integer;
  LRecord: T;
begin
  if AList = nil then
    Exit;

  Self.Read(LRecordsCount, SizeOf(LRecordsCount));
  for I := LRecordsCount - 1 downto 0 do
  begin
    Self.Read(LRecord, SizeOf(T));
    AList.Add(LRecord);
  end;
end;

procedure TStreamEx.ReadArray<T>(out AArray: TArray<T>);
var
  LCount, I: Integer;
  LItem: T;
begin
  Self.Read(LCount, SizeOf(LCount));
  SetLength(AArray, LCount);
  for I := 0 to LCount - 1 do
  begin
    Self.Read(LItem, SizeOf(LItem));
    AArray[I] := LItem;
  end;
end;

procedure TStreamEx.WriteOk;
begin
  Self.Write(RESPONSE_OK, SizeOf(TMsgResponse));
end;

procedure TStreamEx.WriteString(const S: string);
var
  Llen: Cardinal;
  LString: UTF8String;
begin
  LString := UTF8String(S);
  Llen := Length(LString);
  Self.WriteBuffer(Llen, 4);
  if Llen > 0 then
    Self.WriteBuffer(LString[1], Llen);
end;

initialization
  RESPONSE_OK := mrOk;
  RESPONSE_ERROR := mrError;

end.
