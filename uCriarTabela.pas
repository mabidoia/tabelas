unit uCriarTabela;

interface

uses
  Classes, Generics.Collections, superobject, SysUtils, uUtils, uHelperLog, uDBManipulation, uDBQuery, uDBHeader;

{$RTTI EXPLICIT PROPERTIES([vcPublic])}
type

  {$REGION ' Campos '}
  TTIpoCampo = (
    tcbigint,
    tcbit,
    tcchar,
    tcDate,
    tcdatetime,
    tcdatetime2,
    tcdatetimeoffset,
    tcdecimal,
    tcfloat,
    tcint,
    tcmoney,
    tcnchar,
    tcntext,
    tcnumeric,
    tcnvarchar,
    tcreal,
    tcsmalldatetime,
    tcsmallint,
    tcsmallmoney,
    tctext,
    tctime,
    tctinyint,
    tcvarbinary,
    tcvarchar
  );
  {$ENDREGION}

  TCampo = class
  private
  protected
  public
    Nome: string;
    Tipo: TTIpoCampo;
    NotNull: Boolean;
    Padrao: string;
    AutoIncremento: Boolean;
  published
  end;

  TChaveEstrangeira = class
  private
  protected
  public
    Constraint: string;
    Campo: string;
    TabelaReferenciada: string;
    CampoReferenciado: string;
    constructor Create(ConstraintName: string);
  published
  end;

  TChavePrimaria = class
  private
  protected
  public
    Campos: string;
    ConstraintName: string;
  published
  end;

  TListaCampos = class(TObjectList<TCampo>)
  private
    FTemCampoAutoIncremento: Boolean;
  protected
  public
    constructor Create;
    property TemCampoAutoIncremento: Boolean read FTemCampoAutoIncremento write FTemCampoAutoIncremento;
  published
  end;  

  TTabela = class
  private
    FModuloViagem: Boolean;
  protected
  public
    TableName: string;
    ChavePrimaria: TChavePrimaria;
    ListaCAmpos: TListaCampos;
    ListaFK: TObjectList<TChaveEstrangeira>;
    constructor Create(Tabela: string; isViagem: Boolean = False);
    function GerarTabela: TStringList;
    procedure GerarChavePrimaria(out Resultado: TStringList);
    procedure GerarChavesEstrangerias(out Resultado: TStringList);
    function StringCampoNulo(NotNull: Boolean): string;
    procedure SetCampo(Nome: string; Tipo: TTIpoCampo; NotNull: Boolean = False; Padrao: string = ''; AutoIncremento: Boolean = False);
    procedure SetChaveEstrangeira(ConstraintName, Campo, TabelaReferenciada, CampoReferenciado: string);
    procedure SetChavePrimaria(const Fields: array of string; ConstraintName: string);
    property ModuloViagem: Boolean read FModuloViagem write FModuloViagem;
  published
  end;

  TDataBaseController = class
  private
  protected
  public
      function ExistsTabela(ANomeTabela: String): Boolean;
  published
  end;

var
  strTipoCampo: array[TTIpoCampo] of string;

implementation

{ TTabela }

constructor TTabela.Create(Tabela: string; isViagem: Boolean);
begin
  ChavePrimaria := TChavePrimaria.Create;
  ListaCampos := TListaCampos.Create;
  ListaFK := TObjectList<TChaveEstrangeira>.Create;
  FModuloViagem := isViagem;
  TableName := Tabela
end;

procedure TTabela.GerarChavePrimaria(out Resultado: TStringList);
begin
  Resultado.Add(Format('ALTER TABLE %s ADD CONSTRAINT %s PRIMARY KEY (%s);',[TableName, ChavePrimaria.ConstraintName, ChavePrimaria.Campos]));  
end;

procedure TTabela.GerarChavesEstrangerias(out Resultado: TStringList);
var
  ChaveEstrangeira: TChaveEstrangeira;
begin

  for ChaveEstrangeira in ListaFK do
  begin
    Resultado.Add(Format('ALTER TABLE %s ADD CONSTRAINT %s FOREIGN KEY (%s) REFERENCES %s (%s);',
                      [TableName, ChaveEstrangeira.Constraint, ChaveEstrangeira.Campo, ChaveEstrangeira.TabelaReferenciada, ChaveEstrangeira.CampoReferenciado]));
//    if DBSettings.SGBDType in [sgbdSQLServer, sgbdPostgreSQL] then
      Resultado.Add(Format('CREATE INDEX %s ON %s (%s);',[ChaveEstrangeira.Constraint, TableName, ChaveEstrangeira.Campo]));
  end;
end;

function TTabela.GerarTabela: TStringList;
var
  SQL: string;
  Campo: TCampo;
begin
  Result := TStringList.Create;
  Result.Add('CREATE TABLE ' + LowerCase(TableName) + '(');
  for Campo in ListaCampos do
  begin

    SQL := '   ' + Campo.Nome +'  ' + strTipoCampo[Campo.Tipo] +'  ';

    if Campo.AutoIncremento then
      SQL := SQL + ' IDENTITY(1,1) '
    else if Campo.Padrao <> '' then
      SQL := SQL + ' Default ' +  Campo.Padrao + '  ';

    SQL := SQL + ' ' + StringCampoNulo(Campo.NotNull);

    if Result.Count < ListaCampos.Count then
      SQL := SQL +', ';

    Result.Add(SQL);
  end;

  Result.Add(')');

  GerarChavePrimaria(Result);

  GerarChavesEstrangerias(Result);

end;

procedure TTabela.SetCampo(Nome: string; Tipo: TTIpoCampo; NotNull: Boolean; Padrao: string; AutoIncremento: Boolean);
var
  Campo: TCampo;
begin
  Campo := TCampo.Create;
  Campo.Nome := Nome;
  Campo.Tipo := Tipo;
  Campo.NotNull := NotNull;
  Campo.Padrao := Padrao;
  if not ListaCAmpos.TemCampoAutoIncremento then
    Campo.AutoIncremento := AutoIncremento;
    
  ListaCampos.Add(Campo);
end;

procedure TTabela.SetChaveEstrangeira(ConstraintName, Campo, TabelaReferenciada, CampoReferenciado: string);
var
  Chave: TChaveEstrangeira;
begin
    Chave := TChaveEstrangeira.Create(ConstraintName);
    Chave.Campo := Campo;
    Chave.TabelaReferenciada := TabelaReferenciada;
    Chave.CampoReferenciado := CampoReferenciado;
    ListaFK.Add(Chave);
end;

procedure TTabela.SetChavePrimaria(const Fields: array of string; ConstraintName: string);
var
  FieldsLength: Integer;
  I: Integer;
  Campos: string;
begin

  FieldsLength := Length(Fields);
  for I := 0 to FieldsLength - 1 do
  begin
    if I = (FieldsLength - 1) then
      Campos := Campos + Fields[I]
    else
      Campos := Campos + Fields[I] + ',';
  end;

  ChavePrimaria.Campos := Campos;
  ChavePrimaria.ConstraintName := ConstraintName;
end;

function TTabela.StringCampoNulo(NotNull: Boolean): string;
begin
  if( NotNull) then
    Result := 'NOT NULL'
  else
    Result := 'NULL';
end;

{ TChaveEstrangeira }

constructor TChaveEstrangeira.Create(ConstraintName: string);
begin
  Constraint := ConstraintName;
end;

{ TListaCampos }

constructor TListaCampos.Create;
begin
  TemCampoAutoIncremento := False;
end;

{ TDataBaseController }

function TDataBaseController.ExistsTabela(ANomeTabela: String): Boolean;
var
  SQL: string;
  DMLQuery: TDBQuery;
begin

  case DBSettings.SGBDType of
    sgbdFirebird: SQL := Format('SELECT COUNT(*) AS quantidade '
                               + ' FROM rdb$relations '
                               + ' WHERE lower(rdb$relation_name) = ''%s''', [LowerCase(ANomeTabela)]);

    sgbdSQLServer: SQL := Format('SELECT COUNT(*) AS quantidade '
                                + ' FROM sys.objects '
                                + ' WHERE object_id = OBJECT_ID(lower(N''[dbo].[%s]'')) AND type in (N''U'') ', [LowerCase(ANomeTabela)]);

  end;

  DMLQuery := TDBQuery.Create;
  try
    try
      DMLQuery.ExecuteDirect(SQL);
      Result := DMLQuery.GetAsInteger('quantidade') = 1;
    except
      on E: Exception do
        Logger.Error('THelperUpdate.ExistsTabela', Self, E);
    end;
  finally
    DMLQuery.Free;
  end;
end;

initialization

    strTipoCampo[tcbigint]			    := 'bigint';
    strTipoCampo[tcbit]             := 'bit';
    strTipoCampo[tcchar]            := 'char';
    strTipoCampo[tcDate]            := 'Date';
    strTipoCampo[tcdatetime]        := 'datetime';
    strTipoCampo[tcdatetime2]       := 'datetime2';
    strTipoCampo[tcdatetimeoffset]  := 'datetimeoffset';
    strTipoCampo[tcdecimal]         := 'decimal';
    strTipoCampo[tcfloat]           := 'float';
    strTipoCampo[tcint]             := 'int';
    strTipoCampo[tcmoney]           := 'money';
    strTipoCampo[tcnchar]           := 'nchar';
    strTipoCampo[tcntext]           := 'ntext';
    strTipoCampo[tcnumeric]         := 'numeric';
    strTipoCampo[tcnvarchar]        := 'nvarchar';
    strTipoCampo[tcreal]            := 'real';
    strTipoCampo[tcsmalldatetime]   := 'smalldatetime';
    strTipoCampo[tcsmallint]        := 'smallint';
    strTipoCampo[tcsmallmoney]      := 'smallmoney';
    strTipoCampo[tctext]            := 'text';
    strTipoCampo[tctime]            := 'time';
    strTipoCampo[tctinyint]         := 'tinyint';
    strTipoCampo[tcvarbinary]       := 'varbinary(%s)';
    strTipoCampo[tcvarchar]         := 'varchar(%s)';

end.
