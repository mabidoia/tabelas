﻿unit uDBPoolConnection;

interface

uses
  Classes, SyncObjs, uDBHeader, uCriticalSectionVeltrac, Uni, Generics.Collections, uIntegerList;


const
  //constantes que defines as permissões utilizadas 
  GRANT_SELECT = 'SELECT';
  GRANT_SEL_UPDATE = 'SELECT,UPDATE';
  GRANT_SEL_UP_IN_DEL = 'SELECT,UPDATE,INSERT,DELETE';
  //novos usuários criados
  DB_USER_ATENDIMENTO = 'atendimento'; //permissão para SELECT
  DB_PASS_ATENDIMENTO = 'a1t5e3';
  DB_USER_SUPORTE = 'suporte'; //permissão para SELECT e UPDATE
  DB_PASS_SUPORTE = 's2u7p0';
{ TODO -ordmatos : quando acertar os novos usuários a serem criados, descomentar o código }
//  DB_USER_VELTRAC = 'veltrac'; //permissão para SELECT, UPDATE, INSERT e DELETE (usuário do sistema)
//  DB_PASS_VELTRAC = 'v4e3l9';
//  DB_USER_SYSDBA = 'SYSDBA';
//  DB_PASS_SYSDBA = 's2y6s2'; //nova senha para o usuário SYSDBA
  DB_USER_VELTRAC = 'veltrac';
  DB_PASS_VELTRAC = 'veltrac';
  DB_USER_SYSDBA = 'SYSDBA';
  DB_PASS_SYSDBA = 'masterkey';
  DB_USER_VELTRAC_PG = 'postgres';
  DB_PASS_VELTRAC_PG = 'veltrac';

type
  // associação de threads por conexão
  TThreadDBConnection = class
  private
    FDatabase: TDatabases;
    FThreadId: Cardinal;
    FConnection: TUniConnection;  // "ponteiro" para o objeto de conexão
    FShared: Integer;      // amount of objects that shares this connection
    FLastAccess: TDateTime;
    FWorking: Boolean;
    procedure SetWorking(const Value: Boolean);
  public
    // controle da classe
    constructor Create;
    destructor Destroy; override;

    property LastAcess: TDateTime read FLastAccess write FLastAccess;
    property Working: Boolean read FWorking write SetWorking;
  end;

  TDBPoolConnection = class
  private
    FHadModifications: Boolean;
    FRedirectToDBGabarito: Boolean;
    FOldCountOfConnections: Integer;
    FPoolDBConnections: TList;
    FRemovedThreadList: TIntegerList;
    FCSRemovedThreadList: TCriticalSectionVeltec;
    procedure LoadSettings;
    procedure FreeDeletedConnections(var AListThreadConnectionToBeFreed: TList);
    function IsThreadAlive(AThreadId: Integer; ARemovedThreadList: TIntegerList): Boolean;

    function GetDatabasePrincipal: string;
    function GetDatabaseIntegracao: string;
    function GetDatabaseIntegracaoGabarito: string;
    function GetDatabasePrincipalGabarito: string;
  protected
  public
    // controle da classe
    constructor Create;
    destructor Destroy; override;
    // controle do pool de conexões
    function CreateNewConnectionOnThePool(ACurrentThreadId: Cardinal; ADataBase: TDatabases; out AThreadDBConnection: TThreadDBConnection): TObject;
    function SeekUnusedCreatedConnection(ACurrentThreadId: Cardinal; ADataBase: TDatabases; out AThreadDBConnection: TThreadDBConnection): TObject;
    function GetConnectionByThread(ACurrentThreadId: Cardinal; ADataBase: TDatabases; out AThreadDBConnection: TThreadDBConnection): TObject;

    function GetDBConnection(ADatabase: TDatabases; out AThreadDBConnection: TThreadDBConnection): TObject;
    function IsConnectionActive(AConnection: TObject): Boolean;

    procedure ActivateConnection(AConnection: TObject);
    procedure FreeDBConnection(ADatabase: TDatabases);
    procedure Dump;
    procedure FreeAllConnections;
    procedure FreeIdleConnections;
    procedure FreeIdleConnectionsGabarito;
    procedure ReconnectToRenewUserAndPassword;
    procedure NotifyRemovedThread(AThreadId: Integer);

    property DatabasePrincipal: string read GetDatabasePrincipal;
    property DatabaseIntegracao: string read GetDatabaseIntegracao;
    property DatabasePrincipalGabarito: string read GetDatabasePrincipalGabarito;
    property DatabaseIntegracaoGabarito: string read GetDatabaseIntegracaoGabarito;
    property RedirectToDBGabarito: Boolean read FRedirectToDBGabarito write FRedirectToDBGabarito;
  end;

var
  DBPoolConnection: TDBPoolConnection;
  {$IFDEF UNITTESTSERVIDOR}
  SWaitDBCreation: Boolean;
  {$ENDIF}

implementation

uses
  uHelperLog, SysUtils, {$IFDEF SERVIDOR}uModelDatabase, uInicializacao, uVRayCommunication, {$ENDIF} DateUtils, uDBConnectionFirebird, uUtils,
  uDBConnectionSQLServer, IniFiles,
  Windows, SqlExpr, IBDatabase, uHelperThread, uDBConnectionCustom;

var
  DBCriticalSection: TCriticalSectionVeltec;    // controle de thread concorrentes

{ TPoolDBConnection }

procedure TDBPoolConnection.ActivateConnection(AConnection: TObject);
begin
  case DBSettings.SGBDType of
    sgbdFirebird, sgbdSQLServer: TUniConnection(AConnection).Connected := True;
  end;
end;

constructor TDBPoolConnection.Create;
begin
  Logger.Info('TDBPoolConnection.Create');
  FCSRemovedThreadList := TCriticalSectionVeltec.Create;
  FRemovedThreadList := TIntegerList.Create;

  FRedirectToDBGabarito := False;
  FOldCountOfConnections := -1;
  FPoolDBConnections := TList.Create;
  // carrega as configurações de conexão com o banco
  LoadSettings;
end;

function TDBPoolConnection.CreateNewConnectionOnThePool(ACurrentThreadId: Cardinal; ADataBase: TDatabases; out AThreadDBConnection: TThreadDBConnection): TObject;
var
  ThreadDatabase: TThreadDBConnection;
begin
  Result := nil;
  FHadModifications := True;
  // caso não tenha encontrado uma conexão no pool
  // cria uma nova conexão com o banco de dados e insere no pool
  ThreadDatabase := TThreadDBConnection.Create;
  case DBSettings.SGBDType of
    sgbdFirebird:   ThreadDatabase.FConnection := TDBConnectionFirebird.CreateDBConnection(ADatabase);
    sgbdSQLServer:  ThreadDatabase.FConnection := TDBConnectionSQLServer.CreateDBConnection(ADatabase);
  end;

  DBCriticalSection.NamedAcquire('TDBPoolConnection.GetDBConnection');
  try
    ThreadDatabase.FThreadId := ACurrentThreadId;
    ThreadDatabase.FShared := 1;
    ThreadDatabase.FDatabase := ADatabase;
    ThreadDatabase.FLastAccess := Now;
    FPoolDBConnections.Add(ThreadDatabase);

    Result := ThreadDatabase.FConnection;
    AThreadDBConnection := ThreadDatabase;
  finally
    DBCriticalSection.Release;
  end;
end;

destructor TDBPoolConnection.Destroy;
var
  i: Integer;
  ThreadDatabase: TThreadDBConnection;
begin
  FRemovedThreadList.Free;
  FCSRemovedThreadList.Free;
  // libera todas as conexões do pool
  for i := 0 to FPoolDBConnections.Count - 1 do
  begin
    ThreadDatabase := FPoolDBConnections.Items[i];
    case DBSettings.SGBDType of
      sgbdFirebird:   TDBConnectionFirebird(ThreadDatabase.FConnection).Free;
      sgbdSQLServer:  TDBConnectionSQLServer(ThreadDatabase.FConnection).Free;
    end;
  end;
  FPoolDBConnections.Free;
  FRemovedThreadList.Free;

  inherited;
end;

procedure TDBPoolConnection.Dump;
var
  i: Integer;
  ThreadDatabase: TThreadDBConnection;
begin
  DBCriticalSection.NamedAcquire('TDBPoolConnection.Dump');
  try
    Logger.Debug('TPoolDBConnection.Dump');

    for i := 0 to FPoolDBConnections.Count - 1 do
    begin
      ThreadDatabase := FPoolDBConnections.Items[i];
      Logger.Debug('[%d] Database=%d | ThreadId=%d | Shared=%d | LastAccess=%s', [
        i,
        Integer(ThreadDatabase.FDatabase),
        ThreadDatabase.FThreadId,
        ThreadDatabase.FShared,
        DateTimeToStr(ThreadDatabase.FLastAccess)
      ]);
    end;
  finally
    DBCriticalSection.Release;
  end;
end;

procedure TDBPoolConnection.FreeAllConnections;
var
  I: Integer;
  LThreadDatabase: TThreadDBConnection;
  LListThreadConnectionToBeFreed: TList;
begin
  LListThreadConnectionToBeFreed := TList.Create;
  DBCriticalSection.NamedAcquire('TDBPoolConnection.FreeAllConnections');
  try
    for I := FPoolDBConnections.Count -1 downto 0 do
    begin
      LThreadDatabase := FPoolDBConnections.Items[I];
      LListThreadConnectionToBeFreed.Add(TThreadDBConnection(FPoolDBConnections.Extract(LThreadDatabase)));
    end;
  finally
    DBCriticalSection.Release;
    FreeDeletedConnections(LListThreadConnectionToBeFreed);
  end;
end;

procedure TDBPoolConnection.FreeDBConnection(ADatabase: TDatabases);
var
  i: Integer;
  ThreadDatabase: TThreadDBConnection;
  LCurrentThreadId: Cardinal;
begin
  DBCriticalSection.NamedAcquire('TDBPoolConnection.FreeDBConnection');
  try
    // redireciona as conexão para o banco gabarito
    if FRedirectToDBGabarito then
    begin
      case ADatabase of
        dbPrincipal: ADatabase := dbPrincipalGabarito;
        dbIntegracao: ADatabase := dbIntegracaoGabarito;
        dbPrincipalGabarito: ;
        dbIntegracaoGabarito: ;
      end;
    end;

    // obtém o ID da thread atual
    LCurrentThreadId := GetCurrentThreadId;

    // procura por conexão já criada pela thread
    for i := 0 to FPoolDBConnections.Count - 1 do
    begin
      ThreadDatabase := FPoolDBConnections.Items[i];
      if (ThreadDatabase.FThreadId = LCurrentThreadId) and (ThreadDatabase.FDatabase = ADatabase) then
      begin
        // decrementa o contador de compartilhadores da mesma conexão
        Dec(ThreadDatabase.FShared);
        ThreadDatabase.FLastAccess := Now;
        Exit;
      end;
    end;
  finally
    DBCriticalSection.Release;
  end;
end;

procedure TDBPoolConnection.FreeDeletedConnections(var AListThreadConnectionToBeFreed: TList);
var
  I: Integer;
  LThreadDatabase: TThreadDBConnection;
begin
  try
    Logger.Info('TDBPoolConnection.FreeDeletedConnections: AListThreadConnectionToBeFreed.Count = %d', [AListThreadConnectionToBeFreed.Count]);
    for I := AListThreadConnectionToBeFreed.Count - 1 downto 0 do
    begin
      LThreadDatabase := AListThreadConnectionToBeFreed.Items[I];
      FreeAndNil(LThreadDatabase);
    end;
    AListThreadConnectionToBeFreed.Free;
  except
    On E: Exception do
      Logger.Error('TDBPoolConnection.FreeDeletedConnections', Self, E);
  end;
end;

procedure TDBPoolConnection.FreeIdleConnections;
var
  I: Integer;
  LThreadDatabase: TThreadDBConnection;
  LRemovedThreadsList: TIntegerList;
  LIsThreadAlive: Boolean;
  LListThreadConnectionToBeFreed: TList;
begin
  if FHadModifications then
  begin
    Logger.Info('Quantidade de conexões com o banco: [%d]',[FPoolDBConnections.Count]);
    FOldCountOfConnections := FPoolDBConnections.Count;
  end;

  FCSRemovedThreadList.NamedAcquire('TDBPoolConnection.FreeIdleConnections');
  try
    LRemovedThreadsList := FRemovedThreadList;
    FRemovedThreadList := TIntegerList.Create;
  finally
    FCSRemovedThreadList.Release;
  end;

  LListThreadConnectionToBeFreed := TList.Create;
  DBCriticalSection.NamedAcquire('TDBPoolConnection.FreeIdleConnections');
  try
    for I := FPoolDBConnections.Count -1 downto 0 do
    begin
      LThreadDatabase := FPoolDBConnections.Items[I];

      if LThreadDatabase.Working then
        Continue;

      {Se a coneção não estiver sendo usada}
      if (LThreadDatabase.FThreadId = 0) then
      begin
        {já faz mais que 4 minutos que ninguém a usa, então, vamos remove-lá}
        if (SecondsBetween(LThreadDatabase.FLastAccess, Now) > 240) then
        begin
          Logger.Debug('TDBPoolConnection.FreeIdleConnections[ThreadId=%d | Database=%d] FREE', [LThreadDatabase.FThreadId, Integer(LThreadDatabase.FDatabase)]);
          LListThreadConnectionToBeFreed.Add(TThreadDBConnection(FPoolDBConnections.Extract(LThreadDatabase)));
        end;

        {Caso contrário de boa, não precisa remover a conexão, vamos esperar pra ver se alguém vai usar}
        Continue;
      end;

      LIsThreadAlive := IsThreadAlive(LThreadDatabase.FThreadId, LRemovedThreadsList);

      {Se a thread não estiver ativa podemos mandar a conexão pro limbo}
      if not LIsThreadAlive then
      begin
        Logger.Debug('TDBPoolConnection.FreeIdleConnections[ThreadId=%d | Database=%d]: Thread not alive, connection deallocated', [LThreadDatabase.FThreadId, Integer(LThreadDatabase.FDatabase)]);
        LThreadDatabase.FThreadId := 0;
        Continue;
      end;

      {Se fizer mais que X minutos que thread não usa a conexão, retiramos tb a conexão}
      if (SecondsBetween(LThreadDatabase.FLastAccess, Now) > 180) then
      begin
        Logger.Debug('TPoolDBConnection.FreeIdleConnections[ThreadId=%d | Database=%d]: Connection not being used, connection deallocated', [LThreadDatabase.FThreadId, Integer(LThreadDatabase.FDatabase)]);
        LThreadDatabase.FThreadId := 0;
      end;

    end;
  finally
    DBCriticalSection.Release;
    FreeDeletedConnections(LListThreadConnectionToBeFreed);
    LRemovedThreadsList.Free;
  end;
end;

procedure TDBPoolConnection.FreeIdleConnectionsGabarito;
var
  I: Integer;
  ThreadDatabase: TThreadDBConnection;
begin
  if FHadModifications then
  begin
    Logger.Info('Quantidade de conexões com o banco: [%d]',[FPoolDBConnections.Count]);
    FOldCountOfConnections := FPoolDBConnections.Count;
  end;

  DBCriticalSection.NamedAcquire('TDBPoolConnection.FreeIdleConnectionsGabarito');
  try
    for I := FPoolDBConnections.Count -1 downto 0 do
    begin
      ThreadDatabase := FPoolDBConnections.Items[I];

      if ThreadDatabase.Working then
        Continue;

      if (not IsThreadAlive(ThreadDatabase.FThreadId, nil)) or (SecondsBetween(ThreadDatabase.LastAcess, Now) > 120) then
      begin
       {$IFDEF SERVIDOR}
        Logger.Debug('TPoolDBConnection.FreeIdleConnectionsGabarito[ThreadId=%d | Database=%d ] Kill', [ThreadDatabase.FThreadId, Integer(ThreadDatabase.FDatabase)]);
        ListaTransacao.KillByThreadId(ThreadDatabase.FThreadId);
       {$ENDIF}
        ThreadDatabase := FPoolDBConnections.Extract(ThreadDatabase);
        FreeAndNil(ThreadDatabase);
        Continue;
      end;

      if (ThreadDatabase.FDatabase = dbPrincipalGabarito) or (ThreadDatabase.FDatabase = dbIntegracaoGabarito) then
      begin
        Logger.Debug('TPoolDBConnection.FreeIdleConnectionsGabarito[ThreadId=%d | Database=%d] FREE', [ThreadDatabase.FThreadId, Integer(ThreadDatabase.FDatabase)]);
       {$IFDEF SERVIDOR}
        ListaTransacao.KillByThreadId(ThreadDatabase.FThreadId);
       {$ENDIF}
        ThreadDatabase := FPoolDBConnections.Extract(ThreadDatabase);
        FreeAndNil(ThreadDatabase);
      end;
    end;
  except
    on E: Exception do
      Logger.Error('Erro no TDBPoolConnection.FreeIdleConnectionsGabarito', Self, E);
  end;
  DBCriticalSection.Release;
end;

function TDBPoolConnection.GetConnectionByThread(ACurrentThreadId: Cardinal; ADataBase: TDatabases; out AThreadDBConnection: TThreadDBConnection): TObject;
var
  i: Integer;
  ThreadDatabase: TThreadDBConnection;
  ForcarMultiTransacao: Boolean;
begin
  ForcarMultiTransacao := DBSettings.SGBDType <> sgbdSQLServer;

  // procura por conexão já criada pela thread
  for i := 0 to FPoolDBConnections.Count - 1 do
  begin
    ThreadDatabase := FPoolDBConnections.Items[i];

    if (ThreadDatabase.FThreadId = ACurrentThreadId) and
       (ThreadDatabase.FDatabase = ADatabase) and
       ((not TUniConnection(ThreadDatabase.FConnection).InTransaction) or ForcarMultiTransacao) then
    begin
      // incrementa o contador de compartilhadores da mesma conexão
      Inc(ThreadDatabase.FShared);
      // retorna a conexão com o banco de dados
      Result := ThreadDatabase.FConnection;
      ThreadDatabase.FLastAccess := Now;
      AThreadDBConnection := ThreadDatabase;

      Exit;
    end;
  end;
  Result := nil;
end;

function TDBPoolConnection.GetDatabaseIntegracao: string;
begin
  case DBSettings.SGBDType of
    sgbdFirebird: Result := TDBConnectionFirebird.GetDatabaseIntegracao;
    sgbdSQLServer: Result := TDBConnectionSQLServer.GetDatabaseIntegracao;
  end;
end;

function TDBPoolConnection.GetDatabaseIntegracaoGabarito: string;
begin
  case DBSettings.SGBDType of
    sgbdFirebird: Result := TDBConnectionFirebird.GetDatabaseIntegracaoGabarito;
    sgbdSQLServer: Result := TDBConnectionSQLServer.GetDatabaseIntegracaoGabarito;
  end;
end;

function TDBPoolConnection.GetDatabasePrincipal: string;
begin
  case DBSettings.SGBDType of
    sgbdFirebird: Result := TDBConnectionFirebird.GetDatabasePrincipal;
    sgbdSQLServer: Result := TDBConnectionSQLServer.GetDatabasePrincipal;
  end;
end;

function TDBPoolConnection.GetDatabasePrincipalGabarito: string;
begin
  case DBSettings.SGBDType of
    sgbdFirebird: Result := TDBConnectionFirebird.GetDatabasePrincipalGabarito;
    sgbdSQLServer: Result := TDBConnectionSQLServer.GetDatabasePrincipalGabarito;
  end;
end;

function TDBPoolConnection.GetDBConnection(ADatabase: TDatabases; out AThreadDBConnection: TThreadDBConnection): TObject;
var
  LCurrentThreadId: Cardinal;
begin
  {$IFDEF UNITTESTSERVIDOR}
  while SWaitDBCreation do
    Sleep(1000);
  {$ENDIF}

  // obtém o ID da thread atual
  LCurrentThreadId := GetCurrentThreadId;

  // redireciona as conexão para o banco gabarito
  if FRedirectToDBGabarito then
  begin
    case ADatabase of
      dbPrincipal: ADatabase := dbPrincipalGabarito;
      dbIntegracao: ADatabase := dbIntegracaoGabarito;
      dbPrincipalGabarito: ;
      dbIntegracaoGabarito: ;
    end;
  end;

  try
    DBCriticalSection.NamedAcquire('TDBPoolConnection.GetDBConnection');
    try
      Result := GetConnectionByThread(LCurrentThreadId, ADatabase, AThreadDBConnection);
      if Result <> nil then
        Exit;

      Result := SeekUnusedCreatedConnection(LCurrentThreadId, ADatabase, AThreadDBConnection);
      if Result <> nil then
        Exit;
    finally
      DBCriticalSection.Release;
    end;

    Result := CreateNewConnectionOnThePool(LCurrentThreadId, ADatabase, AThreadDBConnection)
  finally
    if not IsConnectionActive(Result) then
      ActivateConnection(Result)
  end;
end;

function TDBPoolConnection.IsConnectionActive(AConnection: TObject): Boolean;
begin
  case DBSettings.SGBDType of
    sgbdFirebird, sgbdSQLServer: Result := TUniConnection(AConnection).Connected;
  end;
end;

function TDBPoolConnection.IsThreadAlive(AThreadId: Integer; ARemovedThreadList: TIntegerList): Boolean;
begin
 //Está na lista das threads ativas ?
  Result := uHelperThread.IsThreadAlive(AThreadId);
  if Result then
    Exit;

  //Está na lista de threads removidas ?
  if ARemovedThreadList <> nil then
  begin
    Result := ARemovedThreadList.IndexOf(AThreadId) = -1;
    if not Result then
      Exit;
  end;

  Result := True;
end;

procedure TDBPoolConnection.LoadSettings;
var
  Ini: TIniFile;
  sgbdName: string;
  DatabasePrincipalOld: string;
begin
  Ini := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'config.ini');
  try
    // caso a seção de conexão com o banco de dados não exista no config.ini
    // inclui as configurações padrão de conexão com o Firebird
    if not Ini.SectionExists('DATABASE') then
    begin
      // obtém as configurações de conexão com o banco de dados nas seções antigas
      DatabasePrincipalOld := Ini.ReadString('BANCOPRINCIPAL', 'StringConexao', '127.0.0.1:' + ExtractFilePath(ParamStr(0)) + 'BD_CLIENTE.FDB');
      // cria nova seção de conexão com o banco de dados
      Ini.WriteString('DATABASE', 'SGBD', 'Firebird');
      Ini.WriteString('DATABASE', 'DatabasePrincipal', DatabasePrincipalOld);
      Ini.WriteString('DATABASE', 'DatabaseIntegracao', '127.0.0.1:' + ExtractFilePath(ParamStr(0)) + 'Integracao\BD_INTEGRACAO.FDB');

      // remove as seções antigas de configurações de conexão com o banco de dados
      Ini.EraseSection('BANCOPRINCIPAL');
      Ini.EraseSection('BANCOAUXILIAR');

      // converte o nome definido no config.ini para o tipo do SGBD
      if sgbdName = 'FIREBIRD' then
      begin
        //seta o usuário e senha default
        Ini.WriteString('DATABASE', 'UserName', DB_USER_SYSDBA);
        Ini.WriteString('DATABASE', 'Password', DB_PASS_SYSDBA);
      end
      else if sgbdName = 'SQLSERVER' then
      begin
        //seta o usuário e senha default
        Ini.WriteString('DATABASE', 'UserName', DB_USER_VELTRAC);
        Ini.WriteString('DATABASE', 'Password', DB_PASS_VELTRAC);
      end
      else if sgbdName = 'POSTGRESQL' then
      begin
        //seta o usuário e senha default
        Ini.WriteString('DATABASE', 'UserName', DB_USER_VELTRAC_PG);
        Ini.WriteString('DATABASE', 'Password', DB_PASS_VELTRAC_PG);
      end;
    end;

    // carrega informações do database
    sgbdName := UpperCase(Ini.ReadString('DATABASE', 'SGBD', ''));

    // converte o nome definido no config.ini para o tipo do SGBD
    if sgbdName = 'FIREBIRD' then
      DBSettings.SGBDType := sgbdFirebird
    else if sgbdName = 'SQLSERVER' then
      DBSettings.SGBDType := sgbdSQLServer
    else if sgbdName = 'POSTGRESQL' then
      DBSettings.SGBDType := sgbdPostgreSQL
    else
      DBSettings.SGBDType := sgbdNone;

    // carrega as configurações básicas e específicas do SGBD
    case DBSettings.SGBDType of
      sgbdNone:
      begin
        Logger.Error('Não foi definido o tipo do banco de dados no config.ini. Defina o identificador SGBD na seção DATABASE.');
        Sleep(2000);
        Halt(HALT_ERROR);
      end;

      sgbdFirebird: TDBConnectionFirebird.LoadSettings;
      sgbdSQLServer: TDBConnectionSQLServer.LoadSettings;
    end;
  finally
    Ini.Free;
  end;
end;

procedure TDBPoolConnection.NotifyRemovedThread(AThreadId: Integer);
begin
  FCSRemovedThreadList.NamedAcquire('TDBPoolConnection.NotifyRemovedThread');
  try
    FRemovedThreadList.Add(AThreadId);
  finally
    FCSRemovedThreadList.Release;
  end;
end;

procedure TDBPoolConnection.ReconnectToRenewUserAndPassword;
begin
  // carrega as configurações básicas e específicas do SGBD
  case DBSettings.SGBDType of
    sgbdFirebird: TDBConnectionFirebird.UpdateSettingsUserPassword;
    sgbdSQLServer: TDBConnectionSQLServer.UpdateSettingsUserPassword;
  end;
end;

function TDBPoolConnection.SeekUnusedCreatedConnection(ACurrentThreadId: Cardinal; ADataBase: TDatabases; out AThreadDBConnection: TThreadDBConnection): TObject;
var
  i: Integer;
  ThreadDatabase: TThreadDBConnection;
begin
  Result := nil;
  // procura por conexão já criada porém não utilizada por ninguém
  for i := 0 to FPoolDBConnections.Count - 1 do
  begin
    ThreadDatabase := FPoolDBConnections.Items[i];
    if (ThreadDatabase.FThreadId = 0) and (ThreadDatabase.FDatabase = ADatabase) then
    begin
      ThreadDatabase.FThreadId := ACurrentThreadId;
      ThreadDatabase.FShared := 1;
      ThreadDatabase.FDatabase := ADatabase;
      ThreadDatabase.FLastAccess := Now;
      // retorna a conexão com o banco de dados
      Result := ThreadDatabase.FConnection;
      AThreadDBConnection := ThreadDatabase;
      Exit;
    end;
  end;
end;

{ TThreadDBConnection }
constructor TThreadDBConnection.Create;
begin
  FWorking := False;
end;

destructor TThreadDBConnection.Destroy;
var
  I: Integer;
  LStep: Integer;
begin
  LStep := 0;
  try
    try
      case DBSettings.SGBDType of
        sgbdFirebird,sgbdSQLServer:
        begin
          for I := TUniConnection(FConnection).TransactionCount -1 downto 0 do
            if TUniConnection(FConnection).Transactions[I].Active then
            begin
              LStep := 1;
              TUniConnection(FConnection).Transactions[I].Commit;
            end;
        end;
      end;
    finally
      LStep := 2;
      FreeAndNil(FConnection);
    end;
  except
    on E: exception do
    begin
      Logger.Error('TThreadDBConnection.Destroy: [%s] %d', [E.Message, LStep]);
      Logger.ErroStackTrace(Self, E);
    end;
  end;
  inherited;
end;

procedure TThreadDBConnection.SetWorking(const Value: Boolean);
begin
  FWorking := Value;
  FLastAccess := Now;
end;

initialization
  DBCriticalSection := TCriticalSectionVeltec.Create('DBCriticalSection');

finalization
  DBCriticalSection.Free;

end.
