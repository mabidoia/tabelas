﻿unit uDBConnectionSQLServer;

interface

uses
  uDBConnectionCustom, uHelperLog, uDBHeader, IBSQLMonitor, DB, DBAccess, IniFiles, CRAccess,Classes, uDBException, uVRayProtocol, Uni,
  SQLServerUniProvider, StdCtrls, ActiveX;

type
  TDBConnectionSQLServer = class(TDBConnectionCustom)
  private
    FUniConnection: TUniConnection;
    FUniTransaction: TUniTransaction;
    FUniQuery: TUniQuery;
    FLocalTransaction: Boolean;

  protected
    // conexão com o banco de dados
    class var FConnectionName: string;
    class var FDriverName: string;
    class var FGetDriverFunc: string;
    class var FLibraryName: string;
    class var FVendorLib: string;
    class var FHostName: string;

    function GetTransaction: TUniTransaction;
  public
    // controle da classe
    constructor Create(ADatabase: TDatabases = dbPrincipal); override;
    destructor Destroy; override;
    // controle de conexão
    class function CreateDBConnection(ADatabase: TDatabases): TUniConnection;
    class procedure CreateDatabases;
    class procedure CreateDatabase(ADatabaseName: string);
	  class procedure LoadSettings;

    function IsConnected: Boolean; override;
    procedure Reconnect; override;
    // controle de transações
    procedure BeginLocalTransaction;
    procedure BeginTransaction; override;
    procedure EndTransaction; override;
    function IsTransactionActive: Boolean; override;
    // execuções dos SQLs
    procedure ResetQuery; override;
    procedure OpenQuery; override;  // select
    procedure ExecSQL; override;    // insert, update, delete, create, drop, alter
    // properties
    property UniQuery: TUniQuery read FUniQuery;
  end;

implementation

uses
  uExceptionEx, {$IFDEF SERVIDOR}uModelDatabase, uIntegracao, uInicializacao, uModelPermissoes,{$ENDIF} SysUtils, uDBPoolConnection, IdGlobal,
  uVRayCommunication, uConfiguracao, IBDatabase, uHeaderPermissoes, SyncObjs, DateUtils;

const
  ID_BLOBFIELD = 520;

{ TDBConnectionSQLServer }

class procedure TDBConnectionSQLServer.LoadSettings;
var
  Ini: TIniFile;
begin
  // carrega as configurações básicas de conexão com o banco de dados
  inherited;
  // carrega as configurações específicas de conexão com o banco de dados
  Ini := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'config.ini');

  try
    FConnectionName := Ini.ReadString('DATABASE', 'ConnectionName', '');
    FDriverName := Ini.ReadString('DATABASE', 'DriverName', '');
    FGetDriverFunc := Ini.ReadString('DATABASE', 'GetDriverFunc', '');
    FLibraryName := Ini.ReadString('DATABASE', 'LibraryName', '');
    FVendorLib := Ini.ReadString('DATABASE', 'VendorLib', '');
    FHostName := Ini.ReadString('DATABASE', 'HostName', '');
    FUserName := Ini.ReadString('DATABASE', 'UserName', '');
    FPassword := Ini.ReadString('DATABASE', 'Password', '');
  finally
    Ini.Free;
  end;
end;

procedure TDBConnectionSQLServer.BeginLocalTransaction;
begin
  FLocalTransaction := not GetTransaction.Active;
  if not FUniTransaction.Active then
    FUniTransaction.StartTransaction;


  if FLocalTransaction then
  begin
    FFailSQL := False;
  end;
end;

procedure TDBConnectionSQLServer.BeginTransaction;
begin
  // Current transaction is guaranteed a consistent view of the data,
  // which includes only changes committed by other transactions at the start of the transaction.
  if (not GetTransaction.Active) then
    FUniTransaction.StartTransaction;

  FFailSQL := False;
  FLocalTransaction := False;
end;

constructor TDBConnectionSQLServer.Create(ADatabase: TDatabases);
begin
  CoInitialize(nil);
  FDatabase := ADatabase;

  FFailSQL := False;
  FRaiseException := True;

  // define o nome do banco de dados gabarito para validar a DDL do banco quando for atualizar o sistema
  FDatabasePrincipalGabarito := FDatabasePrincipal + '_gabarito';
  FDatabaseIntegracaoGabarito := FDatabaseIntegracao + '_gabarito';

  // obtém uma instância de conexão com o banco no pool de conexões
  // caso não tenha, cria uma nova conexão e insere no pool
  FUniConnection := TUniConnection(DBPoolConnection.GetDBConnection(ADatabase, FThreadDBConnection));
  // setar transação
  FUniTransaction := GetTransaction;
  GenerateTransactionKey;

  // a query será criada quando chamar o método SetSQL
  FUniQuery := nil;
end;

class procedure TDBConnectionSQLServer.CreateDatabase(ADatabaseName: string);
var
  DBConnection: TUniConnection;
  FilenameMDF, FilenameLDF, LNameDatabase: string;

  procedure SQLExecute(ASQL: string); overload;
  var
    LArrayOfVar: array of Variant;
  begin
    DBConnection.ExecSQL(ASQL, LArrayOfVar);
  end;

  procedure SQLExecute(ASQL: string; Args: array of const); overload;
  begin
    SQLExecute(Format(ASQL, Args));
  end;

begin
  // define caminho do arquivo do banco de dados
  FilenameMDF := Format('%s.mdf', [ADatabaseName]);
  FilenameLDF := Format('%s_log.ldf', [ADatabaseName]);

  LNameDatabase := ADatabaseName;
  while Pos('\', LNameDatabase) > 0 do
    Fetch(LNameDatabase, '\');

  Logger.Load('Criando banco de dados %s ', [LNameDatabase]);
  try
    CoInitialize(nil);
    // cria conexão com o banco MASTER que criar o novo banco
    DBConnection := TUniConnection.Create(nil);
    DBConnection.ProviderName := 'SQL Server';
    DBConnection.LoginPrompt := False;
    DBConnection.Server := FHostName;
    DBConnection.Database := 'master';
    DBConnection.Username := FUserName;
    DBConnection.Password := FPassword;
    DBConnection.Connected := True;

    // inicia criação do banco
    SQLExecute('USE [master]');
    SQLExecute('CREATE DATABASE [%s] ON  PRIMARY '
             + '( NAME = N''%s'', FILENAME = N''%s'' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB ) '
             + ' LOG ON '
             + '( NAME = N''%s'', FILENAME = N''%s'' , SIZE = 5120KB , MAXSIZE = 2048GB , FILEGROWTH = 10%%) ',
               [LNameDatabase, LNameDatabase, FilenameMDF, LNameDatabase + '_log', FilenameLDF]);

    SQLExecute('EXEC dbo.sp_dbcmptlevel @dbname=N''%s'', @new_cmptlevel=90', [LNameDatabase]);
    SQLExecute('IF (1 = FULLTEXTSERVICEPROPERTY(''IsFullTextInstalled'')) '
             + 'begin '
             + '  EXEC [%s].[dbo].[sp_fulltext_database] @action = ''enable'' '
             + 'end ', [LNameDatabase]);

    SQLExecute('ALTER DATABASE [%s] SET ANSI_NULL_DEFAULT OFF', [LNameDatabase]);
    SQLExecute('ALTER DATABASE [%s] SET ANSI_NULLS OFF', [LNameDatabase]);
    SQLExecute('ALTER DATABASE [%s] SET ANSI_PADDING OFF', [LNameDatabase]);
    SQLExecute('ALTER DATABASE [%s] SET ANSI_WARNINGS OFF', [LNameDatabase]);
    SQLExecute('ALTER DATABASE [%s] SET ARITHABORT OFF', [LNameDatabase]);
    SQLExecute('ALTER DATABASE [%s] SET AUTO_CLOSE OFF', [LNameDatabase]);
    SQLExecute('ALTER DATABASE [%s] SET AUTO_CREATE_STATISTICS ON', [LNameDatabase]);
    SQLExecute('ALTER DATABASE [%s] SET AUTO_SHRINK OFF', [LNameDatabase]);
    SQLExecute('ALTER DATABASE [%s] SET AUTO_UPDATE_STATISTICS ON', [LNameDatabase]);
    SQLExecute('ALTER DATABASE [%s] SET CURSOR_CLOSE_ON_COMMIT OFF', [LNameDatabase]);
    SQLExecute('ALTER DATABASE [%s] SET CURSOR_DEFAULT  GLOBAL', [LNameDatabase]);
    SQLExecute('ALTER DATABASE [%s] SET CONCAT_NULL_YIELDS_NULL OFF', [LNameDatabase]);
    SQLExecute('ALTER DATABASE [%s] SET NUMERIC_ROUNDABORT OFF', [LNameDatabase]);
    SQLExecute('ALTER DATABASE [%s] SET QUOTED_IDENTIFIER OFF', [LNameDatabase]);
    SQLExecute('ALTER DATABASE [%s] SET RECURSIVE_TRIGGERS OFF', [LNameDatabase]);
    SQLExecute('ALTER DATABASE [%s] SET ENABLE_BROKER', [LNameDatabase]);
    SQLExecute('ALTER DATABASE [%s] SET AUTO_UPDATE_STATISTICS_ASYNC OFF', [LNameDatabase]);
    SQLExecute('ALTER DATABASE [%s] SET DATE_CORRELATION_OPTIMIZATION OFF', [LNameDatabase]);
    SQLExecute('ALTER DATABASE [%s] SET PARAMETERIZATION SIMPLE', [LNameDatabase]);
    SQLExecute('ALTER DATABASE [%s] SET READ_WRITE', [LNameDatabase]);
    SQLExecute('ALTER DATABASE [%s] SET RECOVERY SIMPLE', [LNameDatabase]);
    DBConnection.Free;
  except
    on E: Exception do
    begin
      Logger.DBError('TDBConnectionSQLServer.CreateDatabase: [%s]', [E.Message]);
      Sleep(2000);
      Halt(HALT_ERROR);
    end;
  end;
  // ### CoUninitialize
end;

class procedure TDBConnectionSQLServer.CreateDatabases;
var
  FilenameMDF: string;
  LIni: TIniFile;

  function TryDBConnect(ADatabaseName: string; ADatabase: TDatabases): Boolean;
  var
    Database: TUniConnection;
    LNameDatabase: String;
  begin
    CoInitialize(nil);
    Database := TUniConnection.Create(nil);
    try
      Database.ProviderName := 'SQL Server';
      Database.LoginPrompt := False;
      Database.Server := FHostName;
      Database.Username := FUserName;
      Database.Password := FPassword;
      case ADatabase of
        dbPrincipal: Database.Database := ExtractFileName(FDatabasePrincipal);
        dbIntegracao: Database.Database := ExtractFileName(FDatabaseIntegracao);
        dbIntegracaoTSR: Database.Database := ExtractFileName(FDatabaseIntegracaoTSR);
      end;
      Database.Connected := True;

      Result := Database.Connected;
    except
      on E: exception do
      begin
        Result := False;
      end;
    end;
    Database.Free;
    CoUninitialize;
  end;

begin
  CoInitialize(nil);
  // verifica se o dbPRINCIPAL existe e cria se necessário
  FilenameMDF := Format('%s.mdf', [FDatabasePrincipal]);
  if not TryDBConnect(FilenameMDF, dbprincipal) then
    CreateDatabase(FDatabasePrincipal);

  {$IFDEF SERVIDOR}
  if Integracao.SaidaNoBanco then
  begin
    // verifica se o dbINTEGRACAO existe e cria se necessário
    FilenameMDF := Format('%s.mdf', [FDatabaseIntegracao]);
    if not TryDBConnect(FilenameMDF, dbintegracao) then
      CreateDatabase(FDatabaseIntegracao);
  end;

  {$REGION 'Verificação para criação do banco de integração TSR'}
  try
    LIni := TIniFile.Create(Inicializacao.CaminhoAplicacao + 'config.ini');
    if (FDatabaseIntegracaoTSR <> '') and LIni.ReadBool('INTEGRACAO_TERMINAL_SERVICOS', 'ATIVO', False) then
    begin
      FilenameMDF := Format('%s.mdf', [FDatabaseIntegracaoTSR]);
      if not TryDBConnect(FilenameMDF, dbIntegracaoTSR) then
        CreateDatabase(FDatabaseIntegracaoTSR);
    end;
  finally
    LIni.Free;
  end;
  {$ENDREGION}
  {$ENDIF}
  CoUninitialize;
end;

class function TDBConnectionSQLServer.CreateDBConnection(ADatabase: TDatabases): TUniConnection;
var
  LConnected: Boolean;
begin
  LConnected := False;
  Result := nil;

  while not LConnected do
  begin
    FreeAndNil(Result);
    Result := TUniConnection.Create(nil);
    try
      Result.ProviderName := 'SQL Server';
      Result.LoginPrompt := False;
      Result.Server := FHostName;
      Result.Username := FUserName;
      Result.Password := FPassword;
      case ADatabase of
        dbPrincipal: Result.Database := ExtractFileName(FDatabasePrincipal);
        dbIntegracao: Result.Database := ExtractFileName(FDatabaseIntegracao);
        dbMaster: Result.Database := 'master';
        dbPrincipalGabarito: Result.Database := ExtractFileName(FDatabasePrincipalGabarito);
        dbIntegracaoGabarito: Result.Database := ExtractFileName(FDatabaseIntegracaoGabarito);
        dbIntegracaoTSR: Result.Database := ExtractFileName(FDatabaseIntegracaoTSR);
      end;
      Result.SpecificOptions.Values['SQL Server.OLEDBProvider'] := 'prSQL';
      Result.Connected := True;
      LConnected := True;
    except
      on E: Exception do
      begin
        Logger.DBError('TDBConnectionSQLServer.CreateDBConnection: %s [Database: %s]', [E.Message, strTDatabases[ADatabase]]);
        Sleep(5000)
      end;
    end;
  end;
end;

destructor TDBConnectionSQLServer.Destroy;
begin
    //Se tiver algo aberto ainda
  try
    EndTransaction;
  except
    on E: Exception do
    begin
      Logger.DBError('TDBConnectionSQLServer.Destroy: [%s]', [E.Message]);
      Logger.ErroStackTrace(Self, E);
    end;
  end;

  try
    FUniQuery.Free;
  except
    on E: Exception do
    begin
      Logger.DBError('TDBConnectionSQLServer.Destroy: [%s]', [E.Message]);
      Logger.ErroStackTrace(Self, E);
    end;
  end;

  // não pode dar FREE na conexão pois é compartilhada com outras instâncias
  try
    DBPoolConnection.FreeDBConnection(FDatabase);
  except
    on E: exception do
    begin
      Logger.DBError('TDBConnectionSQLServer.Destroy: [%s]', [E.Message]);
      Logger.ErroStackTrace(Self, E);
    end;
  end;

  CoUninitialize;

  inherited;
end;

procedure TDBConnectionSQLServer.EndTransaction;
begin
  // se alguma execução de SQL falhou, cancela das atualizações da transação
  try
    if FFailSQL then
    begin
      if (FUniTransaction <> nil) and (FUniTransaction.Active) then
      begin
        FUniTransaction.Rollback;
        {$IFDEF SERVIDOR}
        ListaTransacao.Rollback(FTransactionId);
        {$ENDIF}
      end;
    end else
    begin
      if (FUniTransaction <> nil) and (FUniTransaction.Active) then
      begin
        FUniTransaction.Commit;
        {$IFDEF SERVIDOR}
        ListaTransacao.Commit(FTransactionId);
        {$ENDIF}
      end;
    end;

    // "reseta" a flag de falha na execução do SQL
    FFailSQL := False;
  except
    on E: Exception do
    begin
      try
        FFailSQL := False;
        Logger.DBError('TDBConnectionSQLServer[%d].EndTransaction: %s', [Integer(FDatabase), E.Message]);
        {$IFDEF SERVIDOR}
        ListaTransacao.Rollback(FTransactionId);
        {$ENDIF}
      finally
        Reconnect;
      end;
      raise;
    end;
  end;
end;

procedure TDBConnectionSQLServer.ExecSQL;
var
  Tentativas: Integer;
  LPerformanceCounter: TPerfCounter;
  LParametros: string;
  LDateTimeStart: TDateTime;

  procedure GerarInfoParams;
  var
    I: integer;
  begin
    if LParametros = '' then
    begin
      for I := 0 to FUniQuery.Params.Count - 1 do
      begin
        if FUniQuery.Params.Items[I].DataType <> ftBlob then
          LParametros := LParametros + Format(' (%s = %s)',[FUniQuery.Params.Items[I].Name,FUniQuery.Params.Items[I].AsString])
        else
          LParametros := LParametros + Format(' (%s = [BLOB])',[FUniQuery.Params.Items[I].Name])
      end;
    end;
  end;

begin
  // registra no log a SQL que será executada
  if LogSQL then
    if DBPoolConnection.RedirectToDBGabarito then
      Logger.Info('TDBConnectionSQLServer[%d][gab].ExecSQL: %s | SqlOriginal: %s', [Integer(FDatabase), Trim(FUniQuery.SQL.Text), Trim(FUniQuery.SQLInsert.Text)])
    else
      Logger.Info('TDBConnectionSQLServer[%d].ExecSQL: %s | SqlOriginal: %s', [Integer(FDatabase), Trim(FUniQuery.SQL.Text), Trim(FUniQuery.SQLInsert.Text)]);

  try
    LPerformanceCounter := SVRay.StartCounting;
    LDateTimeStart := Now;
    // Execute returns the number of records affected by executing the SQL statement
    BeginTransaction;
    FThreadDBConnection.Working := True;
    try
      try
        FThreadDBConnection.LastAcess := Now;
        FUniQuery.Execute;
      except
        FFailSQL := True;
        raise;
      end;
      FRowsAffected := FUniQuery.RowsAffected;
    finally
      FThreadDBConnection.Working := False;
      if FLocalTransaction then
      begin
        FLocalTransaction := False;
        EndTransaction;
      end;
    end;

    if SVRay.IsConnected then
    begin
      GerarInfoParams;
      SVRay.SendSQLInformation(stManipulation, FUniQuery.SQL.Text, LParametros, LPerformanceCounter, LDateTimeStart);
    end;
  except
    on E: EUniError do
    try
      if FLocalTransaction then
      begin
        FLocalTransaction := False;
        EndTransaction;
      end;
      GerarInfoParams;

      TDBException.ParserAndRaise(E.Message, FUniQuery.SQL.Text);
    except
      on Edb: TDBException do
      begin
        if (Pos(UNIQUE_KEY, Edb.Message) > 0) or (Pos(CHAVE_DUPLICADA, Edb.Message) > 0) or (Pos(FOREIGN_KEY, Edb.Message) > 0) then
        begin
          Logger.Warn('TDBConnectionSQLServer[%d].ExecSQL: %s [%s] (Params: %s)', [Integer(FDatabase), FUniQuery.SQL.Text, Edb.Message, LParametros]);
          // para duplicadas, não é considerado que o SQL falhou
          FFailSQL := False;
        end
        else
        begin
        // registra log de erro
          Logger.DBError('TDBConnectionSQLServer[%d].ExecSQL: %s [%s] (Params: %s)', [Integer(FDatabase), FUniQuery.SQL.Text, Edb.Message, LParametros]);
          // seta a flag indicando que a execução do SQL falhou
          FFailSQL := True;
        end;

        if FRaiseException then
          raise;
      end else
      begin
        // registra log de erro
        Logger.DBError('TDBConnectionSQLServer[%d].ExecSQL: %s [%s] (Params: %s)', [Integer(FDatabase), FUniQuery.SQL.Text, E.Message, LParametros]);
        // seta a flag indicando que a execução do SQL falhou
        FFailSQL := True;
        if FRaiseException then
          raise;
      end;
    end;

    on E: Exception do
    begin
      if FLocalTransaction then
      begin
        FLocalTransaction := False;
        EndTransaction;
      end;

      // registra log de erro
      Logger.DBError('TDBConnectionSQLServer[%d].ExecSQL: %s [%s]', [Integer(FDatabase), FUniQuery.SQL.Text, E.Message]);
      // seta a flag indicando que a execução do SQL falhou
      FFailSQL := True;
      // verifica se lança a exceção ou registra no log
      if FRaiseException then
        raise;
    end;
  end;
end;

function TDBConnectionSQLServer.GetTransaction: TUniTransaction;
begin
  try
//    if FUniConnection.InTransaction then
//      Result := TUniTransaction(FUniConnection.)
//    else
      Result := FUniConnection.Transactions[0];
  finally
    FUniTransaction := Result;
    if not FUniTransaction.Active then
    begin
      FUniTransaction.IsolationLevel := ilReadCommitted;
      FUniTransaction.StartTransaction;
    end;
  end;
end;

function TDBConnectionSQLServer.IsConnected: Boolean;
begin
  Result := Self.FUniConnection.Connected;
end;

function TDBConnectionSQLServer.IsTransactionActive: boolean;
begin
  Result := FUniTransaction.Active;
end;

procedure TDBConnectionSQLServer.OpenQuery;
var
  LPerformanceCounter: TPerfCounter;
  LParametros: string;
  LDateTimeStart: TDateTime;
  LSql: string;

  procedure GerarInfoParams;
  var
    I: integer;
  begin
    if (LParametros = '') and (FUniQuery.Params <> nil) then
    begin
      for I := 0 to FUniQuery.Params.Count - 1 do
      begin
        if FUniQuery.Params.Items[I].DataType <> ftBlob then
          LParametros := LParametros + Format(' (%s = %s)',[FUniQuery.Params.Items[I].Name,FUniQuery.Params.Items[I].AsString])
        else
          LParametros := LParametros + Format(' (%s = [BLOB])',[FUniQuery.Params.Items[I].Name])
      end;
    end;
  end;

begin
  // registra no log a SQL que será executada
  if LogSQL then
    if DBPoolConnection.RedirectToDBGabarito then
      Logger.Info('TDBConnectionSQLServer[%d][gab].OpenQuery: %s', [Integer(FDatabase), Trim(FUniQuery.SQL.Text)])
    else
      Logger.Info('TDBConnectionSQLServer[%d].OpenQuery: %s', [Integer(FDatabase), Trim(FUniQuery.SQL.Text)]);

  try
    LPerformanceCounter := SVRay.StartCounting;
    LDateTimeStart := Now;

//    BeginTransaction;
    FThreadDBConnection.Working := True;
    try
      FThreadDBConnection.LastAcess := Now;
      LSql := FUniQuery.SQL.Text;
      FUniQuery.Open;
      FRowsAffected := FUniQuery.RowsAffected;
    finally
      FThreadDBConnection.Working := False;
//      EndTransaction;
    end;
    FUniQuery.First;

    if SVRay.IsConnected then
    begin
      GerarInfoParams;
      SVRay.SendSQLInformation(stQuery, FUniQuery.SQL.Text, LParametros, LPerformanceCounter, LDateTimeStart);
    end;

  except
    on E: Exception do
    begin
      GerarInfoParams;
      // registra log de erro
      Logger.DBError('TDBConnectionSQLServer[%d].OpenQuery: %s [%s]', [Integer(FDatabase), Trim(LSql), E.Message]);
      // lança a exceção se for necessário
      if FRaiseException then
        raise
    end;
  end;
end;

procedure TDBConnectionSQLServer.Reconnect;
begin
  if (FUniTransaction <> nil) and (FUniTransaction.Active) then
    FUniTransaction.Commit;

  FUniConnection.Connected := False;
  FUniConnection.Connected := True;
end;

procedure TDBConnectionSQLServer.ResetQuery;
begin
  if FUniQuery = nil then
  begin
    FUniQuery := TUniQuery.Create(nil);
    FUniQuery.Connection := FUniConnection;
    FUniQuery.Transaction := GetTransaction;
    FUniQuery.DisableControls;
  end;

  FUniQuery.SQL.Clear;
end;

end.
