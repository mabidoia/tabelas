﻿unit uDBConnectionPostgreSql;

interface

uses
  uDBConnectionCustom, SqlExpr, SysUtils, uHelperLog, uExceptionEx, DBXCommon,
  uDBHeader, {$IFDEF SERVIDOR}uIntegracao,{$ENDIF} IdGlobal, uDBException, DB, ZConnection,
  ZAbstractRODataset, ZAbstractDataset, ZDataset, ZDbcIntfs, uDBPoolConnection;

type
  TDBConnectionPostgreSql = class(TDBConnectionCustom)
  private
    FPostgresConnection: TZConnection;
    //FTransaction: TPgTransaction;
    FPostgresQuery: TZQuery;
  protected
    // conexão com o banco de dados
    class var FConnectionName: string;
    class var FHostName: string;
    class var FUser: string;
    class var FPassword: string;
    class var FPorta: Integer;
    class var FProtocol: string;
  public
    // controle da classe
    constructor Create(ADatabase: TDatabases = dbPrincipal); override;
    destructor Destroy; override;
    // controle de conexão
    procedure Reconnect; override;
    function IsConnected: Boolean; override;
    class procedure LoadSettings;
    class function CreateDBConnection(ADatabase: TDatabases): TZConnection;
    class procedure CreateDatabase(ADatabaseName: string);
    class procedure CreateDatabases;
    // controle de transações
    procedure BeginTransaction; override;
    procedure EndTransaction; override;
    function IsTransactionActive: boolean; override;
    // execuções dos SQLs
    procedure ResetQuery; override;
    procedure OpenQuery; override;  // select
    procedure ExecSQL; override;    // insert, update, delete, create, drop, alter
    //properties
    property SQLQuery: TZQuery read FPostgresQuery;
  end;


implementation

uses
 IniFiles, Windows;

{ TDBConnectionSQLServer }

procedure TDBConnectionPostgreSql.BeginTransaction;
begin
  // Current transaction is guaranteed a consistent view of the data,
  // which includes only changes committed by other transactions at the start of the transaction.
  if not FPostgresConnection.InTransaction then
  begin
    FPostgresConnection.TransactIsolationLevel := tiReadCommitted;
    FPostgresConnection.StartTransaction;
  end;

  FFailSQL := False;
end;

constructor TDBConnectionPostgreSql.Create(ADatabase: TDatabases);
begin
  FDatabase := ADatabase;
  FFailSQL := False;
  FRaiseException := True;

  // define o nome do banco de dados gabarito para validar a DDL do banco quando for atualizar o sistema
  FDatabasePrincipalGabarito := FDatabasePrincipal + '_gabarito';
  FDatabaseIntegracaoGabarito := FDatabaseIntegracao + '_gabarito';

  // obtém uma instância de conexão com o banco no pool de conexões
  // caso não tenha, cria uma nova conexão e insere no pool
  FPostgresConnection := TZConnection(DBPoolConnection.GetDBConnection(ADatabase, FThreadDBConnection));
  // a query será criada quando chamar o método SetSQL
  FPostgresQuery := nil;
  GenerateTransactionKey;
end;

class procedure TDBConnectionPostgreSql.CreateDatabase(ADatabaseName: string);
var
  DBConnection: TZConnection;

  procedure SQLExecute(ASQL: string); overload;
  begin
    DBConnection.ExecuteDirect(ASQL);
  end;

  procedure SQLExecute(ASQL: string; Args: array of const); overload;
  begin
    SQLExecute(Format(ASQL, Args));
  end;

begin
  Logger.Debug('Criando banco de dados %s ', [ADatabaseName]);
  try
    // cria conexão com o banco MASTER que criar o novo banco
    DBConnection := TZConnection.Create(nil);
    DBConnection.Name := FConnectionName;
    DBConnection.LoginPrompt := False;
    DBConnection.HostName := FHostName;
    DBConnection.DataBase := 'postgres';// conecta no banco postgres para criar o novo banco de dados
    DBConnection.User := FUserName;
    DBConnection.Password := FPassword;
    DBConnection.Port := FPorta;
    DBConnection.Protocol := FProtocol;
    DBConnection.Connect;

    // inicia criação do banco
    SQLExecute(
      'CREATE DATABASE %s '
    + ' WITH OWNER = "postgres" '
    + ' TEMPLATE=template0'
    + ' ENCODING = ''WIN1252'' '
    + ' TABLESPACE = pg_default '
    + ' LC_COLLATE = ''Portuguese_Brazil.1252'' '
    + ' LC_CTYPE = ''Portuguese_Brazil.1252'' '
    + ' CONNECTION LIMIT = -1;', [ADatabaseName]);
    DBConnection.Free;
  except
    on E: Exception do
    begin
      Logger.Error('TDBConnectionPostgreSql.CreateDatabase');
      Sleep(2000);
      Halt(HALT_ERROR);
    end;
  end;
end;

class procedure TDBConnectionPostgreSql.CreateDatabases;
var
 LIni: TIniFile;

  function TryDBConnect(ADatabase: TDatabases): Boolean;
  var
    Database: TZConnection;
    LNameDatabase: String;
  begin
    Database := TZConnection.Create(nil);
    try
      Database.Name := FConnectionName;
      Database.LoginPrompt := False;
      Database.HostName := FHostName;
      Database.Port := FPorta;
      Database.Protocol := FProtocol;
      case ADatabase of
        dbPrincipal: Database.Database := FDatabasePrincipal;
        dbIntegracao: Database.Database := FDatabaseIntegracao;
        dbIntegracaoTSR: Database.Database := FDatabaseIntegracaoTSR;
      end;
      Database.User := FUserName;
      Database.Password := FPassword;
      Database.Connect;

      Result := Database.Connected;
    except
      Result := False;
    end;
    Database.Free;
  end;

begin
  // verifica se o dbPRINCIPAL existe e cria se necessário
  if not TryDBConnect(dbPrincipal) then
    CreateDatabase(FDatabasePrincipal);

  {$IFDEF SERVIDOR}
  if Integracao.SaidaNoBanco and not TryDBConnect(dbIntegracao) then
    CreateDatabase(FDatabaseIntegracao);

  {$REGION 'Verificação para criação do banco de integração TSR'}
  try
    LIni := TIniFile.Create(Inicializacao.CaminhoAplicacao + 'config.ini');
    if (FDatabaseIntegracaoTSR <> '') and LIni.ReadBool('INTEGRACAO_TERMINAL_SERVICOS', 'ATIVO', False) then
    begin
      if not TryDBConnect(dbIntegracaoTSR) then
        CreateDatabase(FDatabaseIntegracaoTSR);
    end;
  finally
    LIni.Free;
  end;
  {$ENDREGION}
  {$ENDIF}
end;

class function TDBConnectionPostgreSql.CreateDBConnection(ADatabase: TDatabases): TZConnection;
begin
  Result := TZConnection.Create(nil);
  try
    Result.Name := FConnectionName;
    Result.LoginPrompt := False;
    Result.HostName := FHostName;
    Result.Port := FPorta;
    Result.Protocol := FProtocol;
    case ADatabase of
      dbPrincipal: Result.Database := FDatabasePrincipal;
      dbIntegracao: Result.Database := FDatabaseIntegracao;
      dbMaster: Result.Database := 'postgres';
      dbPrincipalGabarito: Result.Database := FDatabasePrincipalGabarito;
      dbIntegracaoGabarito: Result.Database := FDatabaseIntegracaoGabarito;
      dbIntegracaoTSR: Result.Database := FDatabaseIntegracaoTSR;
    end;
    Result.User := FUserName;
    Result.Password := FPassword;
    Result.Connect;
  except
    on E: Exception do
    begin
      Logger.Error('TDBConnectionPostgreSql.CreateDBConnection');
      Sleep(2000);
      Halt(HALT_ERROR);
    end;
  end;
end;

destructor TDBConnectionPostgreSql.Destroy;
begin
  //Se tiver algo aberto ainda
  try
    EndTransaction;
  except
    on E: exception do
      Logger.Error('TDBConnectionPostgreSql.Destroy', Self, E);
  end;

  try
    FPostgresQuery.Free;
  except
    on E: exception do
      Logger.Error('TDBConnectionPostgreSql.Destroy', Self, E);
  end;

  // não pode dar FREE na conexão pois é compartilhada com outras instâncias
  try
    DBPoolConnection.FreeDBConnection(FDatabase);
  except
    on E: exception do
      Logger.Error('TDBConnectionPostgreSql.Destroy', Self, E);
  end;

  inherited;
end;

procedure TDBConnectionPostgreSql.EndTransaction;
begin
  // se alguma execução de SQL falhou, cancela das atualizações da transação
  try

    if FFailSQL then
    begin
      if FPostgresConnection.InTransaction then
        FPostgresConnection.Rollback;
     {$IFDEF SERVIDOR}
      ListaTransacao.Rollback(FTransactionId);
     {$ENDIF}
    end else
    begin
      if FPostgresConnection.InTransaction then
        FPostgresConnection.Commit;
     {$IFDEF SERVIDOR}
      ListaTransacao.Commit(FTransactionId);
     {$ENDIF}
    end;

    // "reseta" a flag de falha na execução do SQL
    FFailSQL := False;
  except
    on E: Exception do
    begin
      FFailSQL := False;
      Logger.Error('TDBConnectionPostgreSql[%d].EndTransaction: %s', [Integer(FDatabase), E.Message]);
     {$IFDEF SERVIDOR}
      ListaTransacao.Rollback(FTransactionId);
     {$ENDIF}      
      raise;
    end;
  end;
end;

procedure TDBConnectionPostgreSql.ExecSQL;
var
  LParametros: string;
  I: integer;
begin
  // registra no log a SQL que será executada
  if LogSQL then
    if DBPoolConnection.RedirectToDBGabarito then
      Logger.Info('TDBConnectionPostgreSql[%d][gab].ExecSQL: %s', [Integer(FDatabase), Trim(FPostgresQuery.SQL.Text)])
    else
      Logger.Info('TDBConnectionPostgreSql[%d].ExecSQL: %s', [Integer(FDatabase), Trim(FPostgresQuery.SQL.Text)]);

  try
    FThreadDBConnection.LastAcess := Now;
    // Execute returns the number of records affected by executing the SQL statement
    FThreadDBConnection.Working := True;
    try
      FPostgresQuery.ExecSQL;
    finally
      FThreadDBConnection.Working := False;
    end;
  except
    on E: Exception do
      try
        LParametros := '';
        for I := 0 to FPostgresQuery.Params.Count - 1 do
        begin
          if FPostgresQuery.Params.Items[I].DataType <> ftBlob then
            LParametros := LParametros + Format(' (%s = %s)',[FPostgresQuery.Params.Items[I].DisplayName,FPostgresQuery.Params.Items[I].Value])
          else
            LParametros := LParametros + Format(' (%s = [BYTEA])',[FPostgresQuery.Params.Items[I].DisplayName])
        end;
        
        TDBException.ParserAndRaise(E.Message, Trim(FPostgresQuery.SQL.Text));
      except
        on E: TDBException do
        begin
          if (Pos(UNIQUE_KEY, E.Message) > 0) or (Pos(CHAVE_DUPLICADA, E.Message) > 0) or (Pos(FOREIGN_KEY, E.Message) > 0) then
          begin
            Logger.Warn('TDBConnectionPostgreSql[%d].ExecSQL: %s [%s] (Params: %s)', [Integer(FDatabase), Trim(FPostgresQuery.SQL.Text), E.Message, LParametros]);
            // para duplicadas, não é considerado que o SQL falhou
            FFailSQL := False;
          end
          else
          begin
            // registra log de erro
            Logger.Error('TDBConnectionPostgreSql[%d].ExecSQL: %s [%s] (Params: %s)', [Integer(FDatabase), Trim(FPostgresQuery.SQL.Text), E.Message, LParametros]);
            // seta a flag indicando que a execução do SQL falhou
            FFailSQL := True;
          end;

          if FRaiseException then
            raise
        end else
        begin
          // registra log de erro
          Logger.Error('TDBConnectionPostgreSql[%d].ExecSQL: %s [%s] (Params: %s)', [Integer(FDatabase), Trim(FPostgresQuery.SQL.Text), E.Message, LParametros]);
          // seta a flag indicando que a execução do SQL falhou
          FFailSQL := True;
          if FRaiseException then
            raise
        end;
      end;

//    on E: Exception do
//    begin
//      if (Pos(UNIQUE_KEY, E.Message) > 0) or (Pos(CHAVE_DUPLICADA, E.Message) > 0) or (Pos(FOREIGN_KEY, E.Message) > 0)then
//        Logger.Warn('TDBConnectionPostgreSql[%d].ExecSQL: %s [%s]', [Integer(FDatabase), Trim(FPostgresQuery.SQL.Text), E.Message])
//      else
//      // registra log de erro
//        Logger.Error('TDBConnectionPostgreSql[%d].ExecSQL: %s [%s]', [Integer(FDatabase), Trim(FPostgresQuery.SQL.Text), E.Message]);
//      // seta a flag indicando que a execução do SQL falhou
//      FFailSQL := True;
//      if FRaiseException then
//        raise
//    end;
  end;
end;

function TDBConnectionPostgreSql.IsConnected: Boolean;
begin
  Result := FPostgresConnection.Connected;
end;

function TDBConnectionPostgreSql.IsTransactionActive: boolean;
begin
  Result := FPostgresConnection.InTransaction;
end;

class procedure TDBConnectionPostgreSql.LoadSettings;
var
  Ini: TIniFile;
begin
  inherited;
  // carrega as configurações específicas de conexão com o banco de dados
  Ini := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'config.ini');
  try
    FConnectionName := Ini.ReadString('DATABASE', 'ConnectionName', '');
    FHostName := Ini.ReadString('DATABASE', 'HostName', '');
    FUserName := Ini.ReadString('DATABASE', 'UserName', '');
    FProtocol := Ini.ReadString('DATABASE', 'Protocol', '');
    FPassword := Ini.ReadString('DATABASE', 'Password', '');
    FPorta := Ini.ReadInteger('DATABASE', 'Port', 5432);
  finally
    Ini.Free;
  end;
end;

procedure TDBConnectionPostgreSql.OpenQuery;
var
  I: Integer;
  LParametros: string;
begin
  // registra no log a SQL que será executada
  if LogSQL then
    Logger.Info('TDBConnectionPostgreSql[%d].OpenQuery: %s', [Integer(FDatabase), Trim(FPostgresQuery.SQL.Text)]);

  try
    FThreadDBConnection.LastAcess := Now;
    FThreadDBConnection.Working := True;
    try
      FPostgresQuery.Open;
      FPostgresQuery.First;
    finally
      FThreadDBConnection.Working := False;
    end;
  except
    on E: Exception do
    begin
      // registra log de erro
      LParametros := '';
      for I := 0 to FPostgresQuery.Params.Count - 1 do
      begin
        if FPostgresQuery.Params.Items[I].DataType <> ftBlob then
          LParametros := LParametros + Format(' (%s = %s)',[FPostgresQuery.Params.Items[I].DisplayName,FPostgresQuery.Params.Items[I].Value])
        else
          LParametros := LParametros + Format(' (%s = [BYTEA])',[FPostgresQuery.Params.Items[I].DisplayName])
      end;

      Logger.Error('TDBConnectionPostgreSql[%d].OpenQuery: %s [%s]. (Params: %s)', [Integer(FDatabase), Trim(FPostgresQuery.SQL.Text), E.Message, LParametros]);
      // lança a exceção se for necessário
      if FRaiseException then
        raise;
    end;
  end;
end;

procedure TDBConnectionPostgreSql.Reconnect;
begin
  FPostgresConnection.Disconnect;
  FPostgresConnection.Connect;
end;

procedure TDBConnectionPostgreSql.ResetQuery;
begin
  FPostgresQuery.Free;
  // cria query para executar os SQLs
  FPostgresQuery := TZQuery.Create(nil);
  FPostgresQuery.Connection := FPostgresConnection;
end;

end.
