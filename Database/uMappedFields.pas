unit uMappedFields;

interface

uses
  RTTI, uPreProcessedReportDBStructure, Generics.Collections;

type
{Os conceito de mappedfield foi feito para evitar que as verifica��es de que, qual campo
  � o que em cada lugar que fosse utilizado. Por exemplo, qual � o campo de primary key ?
  Ao invez de eu ficar procurando sempre, eu crio essa classe mapeada, onde fica armazenado
  as informa��es dos campos da entity. Essa classe fica na classe Repository}
  TMappedRttiField = record
    Field: TRttiField;
    DBAttr: TAttrDBField;
    IsPrimaryKey: Boolean;
    IsBlobField: Boolean;
    IsDate: Boolean;
    IsDateTime: Boolean;
  end;

  TMappedFields = TArray<TMappedRttiField>;

  TRttiMappedClass = class(TObject)
  strict private
    {Mapa dos campos padr�o da RTTI}
    FRttiFields: TArray<TRttiField>;
    {Aqui s�o os campos da RTTI por�m com algumas informa��es adicionais sobre cada um deles}
    FMappedFields: TMappedFields;
    {Esse � um array com os campos do objetos ordenados pelo melhor tipo de filtro. � utilizado
    para realizar as Get do repository com o filtro mais r�pido (em integer por exemplo) randando
    primeiro do que os filtros mais lentos (string por exemplo)}
    FMappedFieldsOrdererToFilter: TMappedFields;
    {Array apenas com os campos que s�o vinculados a campos no banco de dados}
    FDBFields: TMappedFields;
    {Atalho para a primaty key}
    FPrimaryKeyField: TMappedRttiField;
    {Atalho para o campo de filtro de data}
    FDateTimeFilter: TMappedRttiField;
    {Atributo com o nome da tabela}
    FDBAttrClassField: TAttrDBTable;
    {Eles perecisam ficar declarados para manter o contexto, se remover esses itens,
      os RTTI fields s�o removidos automaticamente}
    FContext: TRttiContext;
    FObjType: TRttiType;
    FEntityClass: TClass;

    procedure MapFields(var AObject: TObject);

    procedure GetPrimaryKeyField;
    procedure GetDBFields;
    procedure GetDBAttr;
    procedure GetDateTimeField;
    procedure GetFieldsOrderByFilter;
  public
    procedure ProcessClass(var AObject: TObject);

    property DBAttrClassField: TAttrDBTable read FDBAttrClassField;
    property RttiFields: TMappedFields read FMappedFields;
    property FieldsOrdererToFilter: TMappedFields read FMappedFieldsOrdererToFilter;
    property DBFields: TMappedFields read FDBFields;
    property PrimaryKeyField: TMappedRttiField read FPrimaryKeyField;
    property DateTimeFilter: TMappedRttiField read FDateTimeFilter;
    property EntityClass: TClass read FEntityClass write FEntityClass;
  end;

implementation

uses
  TypInfo, Types;

{ TRttiMappedClass }

procedure TRttiMappedClass.GetDateTimeField;
var
  iField: Integer;
  MapField: TMappedRttiField;
  LCustomAttribute: TObject;
begin
  for iField := 0 to Length(FDBFields) - 1 do
  begin
    MapField := FDBFields[iField];

    if MapField.Field.FieldType.TypeKind <> tkFloat then
      Continue;

    for LCustomAttribute in MapField.Field.GetAttributes do
      if (LCustomAttribute is TAttrDBField) then
        if TAttrDBField(LCustomAttribute).IsDateTimeFilter then
        begin
          FDateTimeFilter := FDBFields[iField];
          Exit;
        end;
  end;
end;

procedure TRttiMappedClass.GetDBAttr;
var
  LCustomAttribute: TCustomAttribute;
  LFields: TArray<TRttiField>;
  iField: Integer;
  LRttiField: TRttiField;
begin
  LFields := FRttiFields;
  for iField := 0 to Length(LFields) - 1 do
  begin
    LRttiField := LFields[iField];
    for LCustomAttribute in LRttiField.GetAttributes do
      if LCustomAttribute is TAttrDBTable then
      begin
        FDBAttrClassField := TAttrDBTable(LCustomAttribute);
        Exit;
      end;
  end;
end;

procedure TRttiMappedClass.GetDBFields;
var
  FieldsList: TList<TMappedRttiField>;
  iField: Integer;
  I: Integer;
  LRttiField: TMappedRttiField;
begin
  FieldsList := TList<TMappedRttiField>.Create;
  try
    for iField := 0 to Length(FMappedFields) - 1 do
    begin
      LRttiField := FMappedFields[iField];
      if LRttiField.DBAttr <> nil then
        FieldsList.Add(LRttiField);
    end;

    SetLength(FDBFields, FieldsList.Count);
    for I := 0 to FieldsList.Count -1 do
      FDBFields[I] := FieldsList.Items[I];
  finally
    FieldsList.Free;
  end;
end;

type
  TypesOfValues = set of TTypeKind;

procedure TRttiMappedClass.GetFieldsOrderByFilter;
var
  List: TList<Integer>;
  I: Integer;

  procedure AddTypeOf(Types: TypesOfValues);
  var
    I: Integer;
  begin
    for I := 0 to Length(FMappedFields) -1 do
      if FMappedFields[I].Field.FieldType.TypeKind in Types then
        List.Add(I);
  end;
begin
  List := TList<Integer>.Create;
  AddTypeOf([tkEnumeration]);
  AddTypeOf([tkInteger,tkInt64]);
  AddTypeOf([tkFloat]);
  AddTypeOf([tkUString, tkDynArray, tkWString, tkString, tkWChar, tkChar, tkLString]);

  SetLength(FMappedFieldsOrdererToFilter, List.Count);
  for I := 0 to List.Count -1 do
    FMappedFieldsOrdererToFilter[I] := FMappedFields[List.Items[I]];

  List.Free;
end;

procedure TRttiMappedClass.GetPrimaryKeyField;
var
  iField: Integer;
begin
  for iField := 0 to Length(FDBFields) - 1 do
  begin
    FDBFields[iField].IsPrimaryKey := FDBFields[iField].DBAttr.IsPrimaryKey;
    if FDBFields[iField].DBAttr.IsPrimaryKey then
      FPrimaryKeyField := FDBFields[iField];
  end;
end;

procedure TRttiMappedClass.MapFields(var AObject: TObject);
var
  FieldsList: TList<TMappedRttiField>;
  iField: Integer;
  LRttiField: TRttiField;
  MapField: TMappedRttiField;
  LCustomAttribute: TObject;
  I: Integer;
begin
  FContext := TRttiContext.Create;
  FObjType := FContext.GetType(AObject.ClassType);
  FRttiFields := FObjType.GetFields;

  FieldsList := TList<TMappedRttiField>.Create;
  try
    for iField := 0 to Length(FRttiFields) - 1 do
    begin
      LRttiField := FRttiFields[iField];

      MapField.Field := LRttiField;
      MapField.IsPrimaryKey := False;
      MapField.DBAttr := nil;
      MapField.IsBlobField := False;
      MapField.IsDate := (LRttiField.FieldType.Name = 'TDate');
      MapField.IsDateTime := (LRttiField.FieldType.Name = 'TDateTime');

      for LCustomAttribute in LRttiField.GetAttributes do
        if (LCustomAttribute is TAttrDBField) then
          MapField.DBAttr := TAttrDBField(LCustomAttribute)
        else if (LCustomAttribute is TIsBlobField) then
          MapField.IsBlobField := True;

      FieldsList.Add(MapField);
    end;

    SetLength(FMappedFields, FieldsList.Count);
    for I := 0 to FieldsList.Count -1 do
      FMappedFields[I] := FieldsList.Items[I];
  finally
    FieldsList.Free;
  end;
end;

procedure TRttiMappedClass.ProcessClass(var AObject: TObject);
begin
  FEntityClass := AObject.ClassType;
  MapFields(AObject);
  GetDBFields;
  GetPrimaryKeyField;
  GetDBAttr;
  GetDateTimeField;
  GetFieldsOrderByFilter;
end;

end.
