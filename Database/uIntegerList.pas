﻿unit uIntegerList;

{
Copyright 1999 Cecilia M. Gross
Bug reports and suggestions to cmsmith@msn.com

Class:   TIntList
Descr:   TIntList class works like TStringList except with integers instead of strings.
Notes:   None of the integer list classes that I have seen that purport to work like TStringList
         include the associated object list. This one does.

         TStringList-like features include:
            -- Associated objects for every integer entry.
            -- Assignement capabilities.
            -- OnChange and OnChanging events.
            -- Same methods for sorting and excluding or including duplicates apply.
            -- Most method names are the same. (except eg: LoadIntegers instead of LoadStrings)
            -- File and streaming methods.
               Note: SaveToFile in TStringList creates a text file list of the strings.
               SaveToFile in TIntList creates a binary file of integers. To save
               the integers to a text file similar to what TStringList does, use SaveToTextFile.

         TIntList was put together by carefully modifying the TStrings and TStringList code
         to work with integers instead of strings.

Class:   TObjIntList (inherits from TIntList)
Descr:   Overrides several methods to automatically manage (create and destroy) associated objects.
Notes:   TIntList works with object, just like TStringList does. But like TStringList, the user
         of the class must manage the creation and destruction of the objects that go in the list.
         TObjIntList manages the creation and destruction of objects in the object list.
         The user of the class just needs to be sure to assign a method that creates
         and returns an associated object. Alternatively, a descendant of the class just needs
         to override one method: AutoCreateObject.

         A descendant class MAY override the virtual AutoCreateObject method:
         An overriden AutoCreateObject method should create and return (but not free)
         the object that is to be associated when an integer is added to the list.

         Alternatively, an OnAutoCreateObject method can be assigned by the user of
         a TObjIntList object. OnAutoCreateObject is a little unorthodox in that
         it is an event that is a function, rather than a procedure. Once again, this
         method should simply create and return an object. It should not actually do the assigning
         to the object list.

         Note: Do one or the other, but not both. In otherwords, create a descendant class
         that overrides the AutoCreateObject method OR, use this class directly and assign
         the OnAutoCreateObject event.

         A descendant class MAY override the virtual AutoFreeObject method:
         Do this if anything needs to be done to the object before it is freed.

         Alternatively, an OnAutoFreeObject method can be assigned. This method should do anything
         that might need to be done before the object is freed. It should NOT actually free
         the object.

         Don't use this class if
         1) you don't need the objects array or
         2) you are already managing the construction and destruction of referenced objects.

         TObjIntList was  writtento insure there will always be an associate object. Thus, even
         when calling LoadFromFile or LoadFromTextFile, the AutoCreateObject method will be called
         when each item is added to the list.
}
interface
uses classes;
const
   SIndexOutOfRange = 'Index out of range';
   SDuplicateInteger =   'Duplicate integer';
   SInsertNotAllowed = 'Insert not allowed in sorted list';
type
{ TIntList class }
  EIntListError   = class( EListError );

  PIntegerItem = ^TIntegerItem;
  TIntegerItem = record
    FInteger: integer;
    FObject: TObject;
  end;
  PIntegerItemList = ^TIntegerItemList;
  TIntegerItemList = array[0..MaxListSize div 2] of TIntegerItem;

  TIntegerList = class(TPersistent)
  private
    FUpdateCount: Integer;
    FList: PIntegerItemList;
    FCount: Integer;
    FCapacity: Integer;
    FSorted: Boolean;
    FDuplicates: TDuplicates;
    FOnChange: TNotifyEvent;
    FOnChanging: TNotifyEvent;
    procedure ExchangeItems(Index1, Index2: Integer);
    procedure Grow;
    procedure QuickSort(L, R: Integer);
    procedure InsertItem(Index: Integer; value : integer);
    procedure SetSorted(Value: Boolean);
    function GetItemsCSV: string;

  protected
    procedure Error(const Msg: string);
    procedure Changed; virtual;
    procedure Changing; virtual;
    function  Get(Index: Integer): integer; virtual;
    function  GetCapacity: Integer; virtual;
    function  GetCount: Integer; virtual;
    function  GetObject(Index: Integer): TObject; virtual;
    procedure Put(Index: Integer; value : integer); virtual;
    procedure PutObject(Index: Integer; AObject: TObject); virtual;
    procedure SetUpdateState(Updating: Boolean); virtual;
    procedure SetCapacity(NewCapacity: Integer); virtual;

  public
    destructor Destroy; override;
    function  Add(value : integer): Integer; virtual;
    function  AddObject(value : integer; AObject: TObject): Integer; virtual;
    procedure Append(value : integer);
    procedure AddIntegers(integers: TIntegerList); virtual;
    procedure Assign(Source: TPersistent); override;
    procedure BeginUpdate;
    procedure Clear; virtual;
    procedure Delete(Index: Integer); virtual;
    procedure EndUpdate;
    function  Equals(intList : TIntegerList): Boolean;
    procedure Exchange(Index1, Index2: Integer); virtual;
    function  Find(N : integer; var Index: Integer): Boolean; virtual;
    function  IndexOfObject(AObject: TObject): Integer;
    procedure InsertObject(Index: Integer; value: integer; AObject: TObject); virtual;
    function  IndexOf(N : integer): Integer; virtual;
    procedure Insert(Index: Integer; value: integer); virtual;

    procedure LoadFromFile(const FileName: string); virtual;
    procedure LoadFromStream(Stream: TStream); virtual;
    procedure SaveToFile(const FileName: string); virtual;
    procedure SaveToStream(Stream: TStream); virtual;

    procedure LoadFromTextFile(const FileName: string); virtual;
    procedure LoadFromTextStream(Stream: TStream); virtual;
    procedure SaveToTextFile(const FileName: string); virtual;
    procedure SaveToTextStream(Stream: TStream); virtual;

    function SaveToText(ADelimiter: Char): string;
    procedure LoadFromString(AString: string; ADelimiter: Char);

    procedure Move(CurIndex, NewIndex: Integer); virtual;
    procedure Sort; virtual;

    property Capacity: Integer read GetCapacity write SetCapacity;
    property Count: Integer read GetCount;
    property Objects[Index: Integer]: TObject read GetObject write PutObject;
    property Items[Index: Integer]: integer read Get write Put; default;
    property ItemsCSV: string read GetItemsCSV;
    property Duplicates: TDuplicates read FDuplicates write FDuplicates;
    property Sorted: Boolean read FSorted write SetSorted;
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
    property OnChanging: TNotifyEvent read FOnChanging write FOnChanging;

  end;

TAutoCreateObjectEvent = function (Sender: TObject; index: integer) : TObject of object;
TAutoFreeObjectEvent = procedure (Sender: TObject; index : integer) of object;

TObjIntList = class(TIntegerList)
 private
   FIsUpdating : boolean;
   FOnAutoCreateObject: TAutoCreateObjectEvent;
   FOnAutoFreeObject: TAutoFreeObjectEvent;
 protected
   procedure SetUpdateState(Updating: Boolean); override;
   // abstract method -- decending classes should return the object to be added.
   function  AutoCreateObject( index : integer ) : TObject; virtual;
   procedure AutoAddObject( index : integer ); virtual;
   procedure AutoFreeObject(index : integer ); virtual;
 public
   destructor Destroy; override;
   function  AddObject(value : integer; AObject: TObject): Integer; override;
   procedure InsertObject(Index: Integer; value: integer; AObject: TObject); override;
   function  Add(value : integer): Integer; override;
   procedure Insert(Index: Integer; value: integer); override;
   procedure Delete(Index: Integer); override;
   procedure Clear; override;
   property OnAutoCreateObject : TAutoCreateObjectEvent read FOnAutoCreateObject write FOnAutoCreateObject;
   property OnAutoFreeObject : TAutoFreeObjectEvent read FOnAutoFreeObject write FOnAutoFreeObject;
 end;

implementation
uses SysUtils, IdGlobal;

{TIntList}

function TIntegerList.AddObject(value : integer; AObject: TObject): Integer;
   begin
   Result := Add(value);
   PutObject(Result, AObject);
   end;

procedure TIntegerList.Append(value: integer);
   begin
   Add(value);
   end;

procedure TIntegerList.AddIntegers(integers: TIntegerList);
   var
      I: Integer;
   begin
   BeginUpdate;
   TRY
   for I := 0 to integers.Count - 1 do
      AddObject(integers[I], integers.Objects[I]);
   FINALLY
   EndUpdate;
   END;
   end;

procedure TIntegerList.Assign(Source: TPersistent);
   begin
   if Source is TIntegerList then
      begin
      BeginUpdate;
      TRY
      Clear;
      AddIntegers(TIntegerList(Source));
      FINALLY
      EndUpdate;
      END;
      end
   else
      inherited Assign(Source);
   end;

procedure TIntegerList.BeginUpdate;
begin
  if FUpdateCount = 0 then SetUpdateState(True);
  Inc(FUpdateCount);
end;

procedure TIntegerList.EndUpdate;
   begin
   Dec(FUpdateCount);
   if FUpdateCount = 0 then SetUpdateState(False);
   end;

function TIntegerList.Equals(intList : TIntegerList): Boolean;
   var
      I, Count: Integer;
   begin
   Result := False;
   Count := GetCount;
   if Count <> intlist.GetCount then
      Exit;
   for I := 0 to Count - 1 do
      begin
      if Get(I) <> intlist.Get(I) then
         Exit;
      end;
   Result := True;
   end;


procedure TIntegerList.Error(const Msg: string);
   begin
   raise EIntListError.Create(msg);
   end;

function TIntegerList.IndexOfObject(AObject: TObject): Integer;
   begin
   for Result := 0 to GetCount - 1 do
   if GetObject(Result) = AObject then
      Exit;
   Result := -1;
   end;

procedure TIntegerList.InsertObject(Index: Integer; value : integer; AObject: TObject);
begin
  Insert(Index, Value);
  PutObject(Index, AObject);
end;


procedure TIntegerList.Move(CurIndex, NewIndex: Integer);
   var
      TempObject: TObject;
      TempInt: integer;
   begin
   if CurIndex <> NewIndex then
      begin
      BeginUpdate;
      TRY
      TempInt := Get(CurIndex);
      TempObject := GetObject(CurIndex);
      // beginUpdate keeps Changing and Changed events from
      // being called within Delete
      Delete(CurIndex);
      InsertObject(NewIndex, TempInt, TempObject);
      FINALLY
      EndUpdate;
      END;
      end;
   end;

procedure TIntegerList.Clear;
   begin
   if FCount <> 0 then
      begin
      Changing;
      FCount := 0;
      SetCapacity(0);
      Changed;
      end;
   end;

destructor TIntegerList.Destroy;
   begin
   FOnChange := nil;
   FOnChanging := nil;
   inherited Destroy;
   FCount := 0;
   SetCapacity(0);
   end;

function TIntegerList.Add(value : integer): Integer;
   begin
   if not Sorted then
      Result := FCount
   else if Find(value, Result) then
      begin
      case Duplicates of
         dupIgnore: Exit;
         dupError:  Error(SDuplicateInteger);
         end;
      end;
   InsertItem(Result, value);
   end;

procedure TIntegerList.Changed;
   begin
   if (FUpdateCount = 0) and Assigned(FOnChange) then FOnChange(Self);
   end;

procedure TIntegerList.Changing;
begin
  if (FUpdateCount = 0) and Assigned(FOnChanging) then FOnChanging(Self);
end;


procedure TIntegerList.Delete(Index: Integer);
begin
  if (Index < 0) or (Index >= FCount) then
    Error(SIndexOutOfRange);
  Changing;
  Dec(FCount);
  if Index < FCount then
    System.Move(FList^[Index + 1], FList^[Index],
       (FCount - Index) * SizeOf(TIntegerItem));
  Changed;
end;

procedure TIntegerList.Exchange(Index1, Index2: Integer);
   begin
   if (Index1 < 0) or (Index1 >= FCount) then Error(SIndexOutOfRange);
   if (Index2 < 0) or (Index2 >= FCount) then Error(SIndexOutOfRange);
   Changing;
   ExchangeItems(Index1, Index2);
   Changed;
   end;

procedure TIntegerList.ExchangeItems(Index1, Index2: Integer);
var
  Temp: Integer;
  Item1, Item2: PIntegerItem;
begin
  Item1 := @FList^[Index1];
  Item2 := @FList^[Index2];
  Temp := Integer(Item1^.FInteger);
  Integer(Item1^.FInteger) := Integer(Item2^.FInteger);
  Integer(Item2^.FInteger) := Temp;
  Temp := Integer(Item1^.FObject);
  NativeInt(Item1^.FObject) := NativeInt(Item2^.FObject);
  NativeInt(Item2^.FObject) := Temp;
end;

function TIntegerList.Find(N : integer; var Index: Integer): Boolean;
   var
      L, H, I: Integer;
   begin
   Result := False;
   L := 0;
   H := FCount - 1;

   while L <= H do
      begin
      I := (L + H) shr 1;
      if FList^[I].FInteger < N then
         L := I + 1
      else
         begin
         H := I - 1;
         if FList^[I].FInteger = N then
            begin
            Result := True;
            if Duplicates <> dupAccept then
               L := I;
            end;
         end;
      end;
   Index := L;
   end;

function TIntegerList.Get(Index: Integer): integer;
begin
  if (Index < 0) or (Index >= FCount) then
    Error(SIndexOutOfRange);
  Result := FList^[Index].FInteger;
end;

function TIntegerList.GetCapacity: Integer;
   begin
   Result := FCapacity;
   end;

function TIntegerList.GetCount: Integer;
   begin
   Result := FCount;
   end;

function TIntegerList.GetItemsCSV: string;
var
  i: Integer;
begin
  Result := '';

  for i := 0 to FCount - 2 do
    Result := Result + IntToStr(FList^[i].FInteger) + ',';

  if FCount > 0 then
    Result := Result + IntToStr(FList^[FCount - 1].FInteger);
end;

function TIntegerList.GetObject(Index: Integer): TObject;
  begin
  if (Index < 0) or (Index >= FCount) then Error(SIndexOutOfRange);
  Result := FList^[Index].FObject;
  end;

procedure TIntegerList.Grow;
var
  Delta: Integer;
begin
  if FCapacity > 64 then Delta := FCapacity div 4 else
    if FCapacity > 8 then Delta := 16 else
      Delta := 4;
  SetCapacity(FCapacity + Delta);
end;

function TIntegerList.IndexOf(N : integer): Integer;
   begin
   if not Sorted then
      begin
      for Result := 0 to GetCount - 1 do
         begin
         if Get(Result) = N then
            Exit;
         end;
      Result := -1;
      end
   else if not Find(N, Result) then
      Result := -1;
   end;

procedure TIntegerList.Insert(Index: Integer; value : integer);
   begin
   if Sorted then
      Error(SInsertNotAllowed);
   if (Index < 0) or (Index > FCount) then
      Error(SIndexOutOfRange);
   InsertItem(Index, value);
   end;

procedure TIntegerList.InsertItem(Index: Integer; value : integer);
   begin
   Changing;
   if FCount = FCapacity then
      Grow;
   if Index < FCount then
      System.Move(FList^[Index], FList^[Index + 1], (FCount - Index) * SizeOf(TIntegerItem));
   with FList^[Index] do
      begin
      FInteger := 0;
      FObject := nil;
      FInteger := value;
      end;
   Inc(FCount);
   Changed;
   end;

procedure TIntegerList.Put(Index: Integer; value : integer);
   begin
   if Sorted then
      Error(SInsertNotAllowed);
   if (Index < 0) or (Index >= FCount) then
      Error(SIndexOutOfRange);
   Changing;
   FList^[Index].FInteger := value;
   Changed;
   end;

procedure TIntegerList.PutObject(Index: Integer; AObject: TObject);
begin
  if (Index < 0) or (Index >= FCount) then Error(SIndexOutOfRange);
  Changing;
  FList^[Index].FObject := AObject;
  Changed;
end;

procedure TIntegerList.QuickSort(L, R: Integer);
var
  I, J: Integer;
  P: integer;
begin
  repeat
    I := L;
    J := R;
    P := FList^[(L + R) shr 1].FInteger;
    repeat
      while FList^[I].FInteger < P do
         Inc(I);
      while FList^[J].FInteger > P do
         Dec(J);
      if I <= J then
         begin
         ExchangeItems(I, J);
         Inc(I);
         Dec(J);
         end;
    until I > J;
    if L < J then QuickSort(L, J);
    L := I;
   until I >= R;
  end;

procedure TIntegerList.SetCapacity(NewCapacity: Integer);
   begin
   ReallocMem(FList, NewCapacity * SizeOf(TIntegerItem));
   FCapacity := NewCapacity;
   end;

procedure TIntegerList.SetSorted(Value: Boolean);
begin
  if FSorted <> Value then
  begin
    if Value then Sort;
    FSorted := Value;
  end;
end;

procedure TIntegerList.SetUpdateState(Updating: Boolean);
   begin
   if Updating then
      Changing
   else
      Changed;
   end;

procedure TIntegerList.Sort;
   begin
   if not Sorted and (FCount > 1) then
      begin
      Changing;
      QuickSort(0, FCount - 1);
      Changed;
      end;
   end;

procedure TIntegerList.LoadFromFile(const FileName: string);
   var
      Stream: TStream;
   begin
   Stream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
   TRY
   LoadFromStream(Stream);
   FINALLY
   Stream.Free;
   END;
   end;

// load integers from an integer stream
procedure TIntegerList.LoadFromStream(Stream: TStream);
   var
      iTmp : integer;
      iSizeInt : integer;
   begin
   BeginUpdate;
   Clear;
   TRY
   iSizeInt := sizeOf(Integer);
   // when number of bytes read is less, then we are at eof
   while Stream.Read(iTmp, iSizeInt) = iSizeInt do
      Add( iTmp );
   FINALLY
   EndUpdate;
   END;
   end;

procedure TIntegerList.LoadFromString(AString: string; ADelimiter: Char);
begin
  AString := Trim(AString);
  while AString <> '' do
    Add(StrToInt(Fetch(AString,ADelimiter)));
end;

procedure TIntegerList.SaveToFile(const FileName: string);
   var
      Stream: TStream;
   begin
   Stream := TFileStream.Create(FileName, fmCreate);
   TRY
   SaveToStream(Stream);
   FINALLY
   Stream.Free;
   END;
   end;

procedure TIntegerList.SaveToStream(Stream: TStream);
   var
      i, iTot : integer;
      iTmp : integer;
   begin
   iTot := count - 1;
   for i := 0 to iTot do
      begin
      iTmp := get(i);
      Stream.Write(iTmp, SizeOf(Integer));
      end;
   end;

procedure TIntegerList.LoadFromTextStream(Stream: TStream);
   var
      Size: Integer;
      S: string;
   procedure SetTextStr(const Value: string);
      var
         P, Start: PChar;
         S: string;
      begin
      BeginUpdate;
      TRY
      Clear;
      P := Pointer(Value);
      if P <> nil then
         while P^ <> #0 do
            begin
            Start := P;
            while not (P^ in [#0, #10, #13]) do Inc(P);
            SetString(S, Start, P - Start);
            Add(strToInt(S));
            if P^ = #13 then Inc(P);
            if P^ = #10 then Inc(P);
            end;
      FINALLY
      EndUpdate;
      END;
      end;

   // FUNCTION BEGINS HERE
   begin
   BeginUpdate;
   TRY
   Size := Stream.Size - Stream.Position;
   SetString(S, nil, Size);
   Stream.Read(Pointer(S)^, Size);
   SetTextStr(S);
   FINALLY
   EndUpdate;
   END;
   end;

procedure TIntegerList.LoadFromTextFile(const FileName: string);
   var
      Stream: TStream;
   begin
   Stream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
   TRY
   LoadFromTextStream(Stream);
   FINALLY
   Stream.Free;
   END;
   end;

procedure TIntegerList.SaveToTextStream(Stream: TStream);
   var
      S: string;
   function GetTextStr: string;
      var
         I, L, Size, Count: Integer;
         P: PChar;
         S: string;
      begin
      Count := GetCount;
      Size := 0;
      for I := 0 to Count - 1 do
         Inc(Size, Length( intToStr(Get(I)) ) + 2);

      SetString(Result, nil, Size);
      P := Pointer(Result);
      for I := 0 to Count - 1 do
         begin
         S := intToStr(Get(I));
         L := Length(S);
         if L <> 0 then
            begin
            System.Move(Pointer(S)^, P^, L);
            Inc(P, L);
            end;
         P^ := #13;
         Inc(P);
         P^ := #10;
         Inc(P);
         end;
      end;
   // FUNCTION BEGINS HERE
   begin
   S := GetTextStr;
   Stream.WriteBuffer(Pointer(S)^, Length(S));
   end;

function TIntegerList.SaveToText(ADelimiter: Char): string;
var
  I: integer;
begin
  Result := '';

  if Count > 0 then
  begin
    Result := IntToStr(Items[0]);

    for I := 1 to Count - 1 do
      Result := Result + ADelimiter + IntToStr(Items[I]);
  end;
end;

procedure TIntegerList.SaveToTextFile(const FileName: string);
   var
      Stream: TStream;
   begin
   Stream := TFileStream.Create(FileName, fmCreate);
   TRY
   SaveToTextStream(Stream);
   FINALLY
   Stream.Free;
   END;
   end;

{TObjIntList}
function  TObjIntList.AutoCreateObject( index : integer ) : TObject;
   begin
   if assigned( FOnAutoCreateObject ) then
      result := FOnAutoCreateObject( self, index )
   else
      result := nil;
   end;

procedure TObjIntList.AutoAddObject(Index: Integer);
   begin
   if (Index >= 0) and (index < Count ) then
      begin
      // in case where duplicates are not allowed,
      // we don't want to create a new object
      // So, if object is already assigned then we know not to create a new one.
      if not assigned( Objects[index ]) then
         Objects[index] := AutoCreateObject(index);
      end;
   end;

// protected method that frees the associated object
procedure TObjIntList.AutoFreeObject( index : integer );
   begin
   // make sure index is in range
   if (Index >= 0) and (index < Count ) then
      begin
      // call destructor of object
      if assigned(objects[index]) then
         begin
         if assigned( FOnAutoFreeObject ) then
            FOnAutoFreeObject( self, index );
         // thanks to polymorphism, the correct free method will
         // be called, regardless of the type of object.
         objects[index].free;
         end;
      end;
   end;

// When user calls free, associated objects are sure to be freed.
destructor TObjIntList.Destroy;
   begin
   Clear;
   inherited Destroy;
   end;

// Free associated objects before clearing the integer list
procedure TObjIntList.Clear;
   var
      i, iTot : integer;
   begin
   // remove the objects before clearing
   iTot := count - 1;
   for i := 0 to iTot do
      AutoFreeObject(i);
   inherited Clear;
   end;

// Sets internal flag so that AutoFreeObject is not called when the Move method calls Delete.
procedure TObjIntList.SetUpdateState( Updating : boolean ) ;
   begin
   FIsUpdating := Updating;
   inherited SetUpdateState( updating );
   end;

// After adding the value, create the associated object
function TObjIntList.Add(value : integer) : integer;
   begin
   result := inherited Add( value );
   AutoAddObject( result );
   end;

// After inserting the value, create the associated object
procedure TObjIntList.Insert(Index: Integer; value: integer);
   begin
   inherited Insert(index, value );
   // note, if insert failed (e.g. we're sorted) then
   // autoaddobject won't be called so we're ok
   AutoAddObject( index );
   end;

// Override Delete so that we first free the associated object
// (We don't free the associated object if Delete was called by Move)
procedure TObjIntList.Delete(Index: Integer);
   begin
   // we don't want to delete objects if Delete was
   // called by the Move method
   if FIsUpdating then
      inherited Delete( index )
   else
      begin
      AutoFreeObject( index );
      inherited Delete(index);
      end;
   end;

// override AddObject so that we DON'T do the automatic object create
function TObjIntList.AddObject(value : integer; AObject: TObject): Integer;
   begin
   Result := inherited Add(value);
   PutObject(Result, AObject);
   end;

// override InsertObject so that we DON'T do the automatic object create
procedure TObjIntList.InsertObject(Index: Integer; value : integer; AObject: TObject);
   begin
   inherited Insert(Index, Value);
   PutObject(Index, AObject);
   end;


end.




