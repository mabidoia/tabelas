﻿unit uPreProcessedReportDBStructure;
{$RTTI EXPLICIT METHODS([vcPublic, vcPublished]) PROPERTIES([vcPublic, vcPublished]) FIELDS([vcProtected])}
{$M+}

interface

uses
  Generics.Collections, superobject, DBClient, FieldByNameSpeedUp;

const
  PP_VEHICLE_FIELD = 'VEICULO_ID';
  PP_DRIVER_FIELD = 'MOTORISTA_ID';
  PP_TRIP_FIELD = 'VIAGEM_ID';
  PP_DATE = 'DATA';
  PP_ISDEPRECATED = 'ISDEPRECATED';

  PARAMS_INITIAL_DATE = 'DATA_INICIAL';
  PARAMS_FINAL_DATE = 'DATA_FINAL';

type
  { Classe de atributos referente aos campos no banco de dados }
  TAttrDataSet = class(TCustomAttribute)
  private
    FFieldName: string;
  public
    constructor Create(AFieldName: string);
    property FieldName: string read FFieldName;
  end;

  { Classe de atributos referente a tabela no banco de dados }
  TAttrDBTable = class(TCustomAttribute)
  private
    FTableName: string;
    //Esse é o campo onde vai contar o nome do relatório na tela de reprocessamento de relatório pré processados
    FDisplayName: string;
  public
    constructor Create(ATableName: string; ADisplayName: string);

    property TableName: string read FTableName;
    property DisplayName: string read FDisplayName;
  end;

  TIsBlobField = class(TCustomAttribute)
  end;

  { Classe de atributos referente aos campos no banco de dados }
  TAttrDBField = class(TCustomAttribute)
  private
    FFieldName: string;
    FInsertInDataSet: Boolean;
    FIsPrimaryKey: Boolean;
    FIsDateTimeFilter: Boolean;
  public
    constructor Create(AFieldName: string; AInsertInDataSet: Boolean = True; AIsPrimaryKey: Boolean = False; AIsDateTimeFilter: Boolean = False);

    property FieldName: string read FFieldName;
    property InsertInDataSet: Boolean read FInsertInDataSet write FInsertInDataSet;
    property IsPrimaryKey: Boolean read FIsPrimaryKey;
    property IsDateTimeFilter: Boolean read FIsDateTimeFilter;
  end;

  { Classe base de item do relatório, referente a tupla no banco }
 [TAttrDBTable('NONE', 'NONE')]
  TReportItem = class(TObject)
  private
    function GetIsDeprecated: Boolean;
    procedure SetIsDeprecated(const Value: Boolean);

    function MustInsertInDataSet: Boolean;
  protected
    [TAttrDBField(PP_VEHICLE_FIELD)]
    FVeiculoId: integer;
    [TAttrDBField(PP_DRIVER_FIELD)]
    FMotoristaId: integer;
    [TAttrDBField(PP_TRIP_FIELD)]
    FViagemId: integer;
    [TAttrDBField(PP_DATE)]
    FDataRelatorio: TDate;
    [TAttrDBField(PP_ISDEPRECATED)]
    FIsDeprecated: Integer;
  public
    constructor Create;

    function Clone: TReportItem;
    procedure CloneTo(ReportItem: TReportItem);
  published
    property VeiculoId: integer read FVeiculoId write FVeiculoId;
    property MotoristaId: integer read FMotoristaId write FMotoristaId;
    property ViagemId: integer read FViagemId write FViagemId;
    property Data: TDate read FDataRelatorio write FDataRelatorio;
    property IsDeprecated: Boolean read GetIsDeprecated write SetIsDeprecated;

    class function GetTableName<T: class, constructor>: string;

    procedure InsertItemOnDataSet(AClientDataSet: TClientDataSet; AFieldList: iFieldList);
    procedure LoadFromDataSet(AClientDataSet: TClientDataSet; AFieldList: iFieldList);
  end;

implementation

uses
  DBXJSONReflect, DBXJSON, Rtti, TypInfo, uHelperLog, DB, SysUtils, Classes;

{ TFieldItem }

constructor TAttrDBField.Create(AFieldName: string; AInsertInDataSet: Boolean = True; AIsPrimaryKey: Boolean = False; AIsDateTimeFilter: Boolean = False);
begin
  FFieldName := AFieldName;
  FInsertInDataSet := AInsertInDataSet;
  FIsPrimaryKey := AIsPrimaryKey;
  FIsDateTimeFilter := AIsDateTimeFilter;
end;

{ TAttrTable }

constructor TAttrDBTable.Create(ATableName: string; ADisplayName: string);
begin
  FTableName := ATableName;
  FDisplayName := ADisplayName;
end;

{ TReportItem }
function TReportItem.Clone: TReportItem;
var
  LRttiContext: TRttiContext;
  LRttiType: TRttiType;
  LRttiFields: TArray<TRttiField>;
  LRttiField: TRttiField;
  iField: Integer;
  LValue: TValue;
begin
  Result := TReportItem(Self.ClassType.Create);

  LRttiContext := TRttiContext.Create;
  LRttiType := LRttiContext.GetType(Self.ClassType);
  LRttiFields := LRttiType.GetFields;
  for iField := 0 to Length(LRttiFields) - 1 do
  begin
    LRttiField := LRttiFields[iField];
    LValue := LRttiField.GetValue(Self);
    LRttiField.SetValue(Result, LValue);
  end;
end;

procedure TReportItem.CloneTo(ReportItem: TReportItem);
var
  LRttiContext: TRttiContext;
  TypeBase: TRttiType;
  FieldsBase: TArray<TRttiField>;
  LRttiField: TRttiField;
  iField: Integer;
  LValue: TValue;
  TypeComp: TRttiType;
  FieldsComp: TArray<TRttiField>;
  iFieldBase: Integer;
  BaseField: TRttiField;
  iFieldComp: Integer;
  CompField: TRttiField;
  BaseValue: TValue;
begin
  LRttiContext := TRttiContext.Create;
  TypeBase := LRttiContext.GetType(Self.ClassType);
  FieldsBase := TypeBase.GetFields;

  TypeComp := LRttiContext.GetType(ReportItem.ClassType);
  FieldsComp := TypeComp.GetFields;

  for iFieldBase := 0 to Length(FieldsBase) -1 do
  begin
    BaseField := FieldsBase[iFieldBase];
    for iFieldComp := 0 to Length(FieldsComp) -1 do
    begin
      CompField := FieldsComp[iFieldComp];

      if BaseField.FieldType <> CompField.FieldType then
        Continue;

      if BaseField.Name <> CompField.Name then
        Continue;

      BaseValue := BaseField.GetValue(Self);
      CompField.SetValue(ReportItem, BaseValue);
      Continue;
    end;
  end;
end;

constructor TReportItem.Create;
begin
  FDataRelatorio := -1;
  FVeiculoId := -1;
  FMotoristaId := -1;
  FViagemId := -1;
end;

function TReportItem.GetIsDeprecated: Boolean;
begin
  Result := FIsDeprecated = 1;
end;

class function TReportItem.GetTableName<T>: string;
var
  LRttiContext: TRttiContext;
  LRttiType: TRttiType;
  LCustomAttribute: TCustomAttribute;
  LType: T;
begin
  LType := T.Create;
  try
    LRttiContext := TRttiContext.Create;
    LRttiType := LRttiContext.GetType(LType.ClassType);
    for LCustomAttribute in LRttiType.GetAttributes do
      if LCustomAttribute is TAttrDBTable then
      begin
        Result := TAttrDBTable(LCustomAttribute).TableName;
        Break;
      end;
  finally
    LType.Free;
  end;
end;

procedure TReportItem.InsertItemOnDataSet(AClientDataSet: TClientDataSet; AFieldList: iFieldList);
var
  LRttiContext: TRttiContext;
  LRttiType: TRttiType;
  LRttiFields: TArray<TRttiField>;
  LRttiField: TRttiField;
  LCustomAttribute: TObject;
  iField: Integer;
  LField: TField;
  iProperty: Integer;
  LRttiProperties: TArray<TRttiProperty>;
  LRttiProperty: TRttiProperty;
  ValorCampo: TValue;
  ValorString: string;

  function GetVariantFromValue(ValorCampo: TValue): Variant;
  begin
    if ValorCampo.IsOrdinal and (ValorCampo.DataSize = 1) then {É tipo enumerao}
      Result := ValorCampo.AsOrdinal
    else
      Result := ValorCampo.AsVariant;
  end;

  function MustUseThisAtribute(Attribute: TObject): Boolean;
  begin
    Result := False;
    if (LCustomAttribute is TAttrDBField) then
    begin
      if not TAttrDBField(LCustomAttribute).InsertInDataSet then
        Exit;
    end
    else if not (LCustomAttribute is TAttrDataSet) then
      Exit;
    Result := True;
  end;

  function GetField(FieldName: string; out Field: TField): Boolean;
  begin
    Result := True;
    LField := AFieldList.FieldByName(FieldName);
    if LField = nil then
    begin
      Logger.Error('%s.InsertItemOnDataSet: Campo %s não encontrado.',[Self.ClassName, FieldName], Self, nil);
      Result := False;
    end;
  end;

  procedure StoreBlob(FieldName: string; Conteudo: string);
  var
    Blob: TStream;
    StringMemory: TStringStream;
  begin
    Blob := AClientDataSet.CreateBlobStream(LField, bmWrite);
    try
      Blob.Seek(0, soFromBeginning);

      StringMemory := TStringStream.Create(Conteudo);
      StringMemory.Position := 0;
      try
        Blob.CopyFrom(StringMemory, StringMemory.Size) ;
      finally
         StringMemory.Free ;
      end;
    finally
      Blob.Free ;
    end;
  end;

begin
  try
    LRttiContext := TRttiContext.Create;
    LRttiType := LRttiContext.GetType(Self.ClassType);
    LRttiFields := LRttiType.GetFields;

    AClientDataSet.Append;

    for iField := 0 to Length(LRttiFields) - 1 do
    begin
      LRttiField := LRttiFields[iField];
      for LCustomAttribute in LRttiField.GetAttributes do
      begin
        if not MustUseThisAtribute(LCustomAttribute) then
          Continue;

        if not GetField(TAttrDBField(LCustomAttribute).FieldName, LField) then
          Continue;

        if LField.IsBlob then
        begin
          ValorString := LRttiField.GetValue(Self).AsString;
          StoreBlob(LField.FieldName, ValorString );
          Continue;
        end;

        ValorCampo := LRttiField.GetValue(Self);
        LField.Value := GetVariantFromValue(ValorCampo);
      end;
    end;

    LRttiProperties := LRttiType.GetProperties;
    for iProperty := 0 to Length(LRttiProperties) - 1 do
    begin
      LRttiProperty := LRttiProperties[iProperty];
      for LCustomAttribute in LRttiProperty.GetAttributes do
      begin
        if not MustUseThisAtribute(LCustomAttribute) then
          Continue;

        if not GetField(TAttrDBField(LCustomAttribute).FieldName, LField) then
          Continue;

        if LField.IsBlob then
        begin
          ValorString := LRttiField.GetValue(Self).AsString;
          StoreBlob(LField.FieldName, ValorString );
          Continue;
        end;

        ValorCampo := LRttiProperty.GetValue(Self);
        LField.Value := GetVariantFromValue(ValorCampo);
      end;
    end;

    AClientDataSet.Post;
  except
    on E: Exception do
      Logger.Error('%s.InsertItemOnDataSet',[Self.ClassName], Self, E);
  end;
end;

procedure TReportItem.LoadFromDataSet(AClientDataSet: TClientDataSet; AFieldList: iFieldList);
var
  LRttiContext: TRttiContext;
  LRttiType: TRttiType;
  LRttiFields: TArray<TRttiField>;
  LRttiField: TRttiField;
  LCustomAttribute: TObject;
  iField: Integer;
  LField: TField;
  iProperty: Integer;
  LRttiProperties: TArray<TRttiProperty>;
  LRttiProperty: TRttiProperty;
  ValorCampo: TValue;
  DataSetField: TField;
  LFieldPointer: Pointer;
  LTypedSmallInt: SmallInt;
  StringBlob: string;

  function MustUseThisAtribute(Attribute: TObject): Boolean;
  begin
    Result := False;
    if (LCustomAttribute is TAttrDBField) then
    begin
      if not TAttrDBField(LCustomAttribute).InsertInDataSet then
        Exit;
    end
    else if not (LCustomAttribute is TAttrDataSet) then
      Exit;
    Result := True;
  end;

  function GetVariantFromValue(ValorCampo: TValue): Variant;
  begin
    if ValorCampo.IsOrdinal and (ValorCampo.DataSize = 1) then {É tipo enumerao}
      Result := ValorCampo.AsOrdinal
    else
      Result := ValorCampo.AsVariant;
  end;

  function RestoreBlob: string;
  var
    Blob : TStream;
    MemString: TStringStream;
  begin
    Blob := AClientDataSet.CreateBlobStream(DataSetField, bmRead);
    try
      Blob.Seek(0, soFromBeginning);
      MemString := TStringStream.Create;
      try
        MemString.CopyFrom(Blob, Blob.Size);
        Result := MemString.DataString;
      finally
        MemString.Free;
      end;
    finally
      Blob.Free ;
    end;
  end;

begin
  try
    LRttiContext := TRttiContext.Create;
    LRttiType := LRttiContext.GetType(Self.ClassType);
    LRttiFields := LRttiType.GetFields;

    for iField := 0 to Length(LRttiFields) - 1 do
    begin
      LRttiField := LRttiFields[iField];
      for LCustomAttribute in LRttiField.GetAttributes do
      begin
        if not MustUseThisAtribute(LCustomAttribute) then
          Continue;

        DataSetField := AFieldList.FieldByName(TAttrDBField(LCustomAttribute).FieldName);
        if DataSetField = nil then
          Continue;

        if DataSetField.IsNull then
          Continue;

        if DataSetField.IsBlob then
        begin
          StringBlob := RestoreBlob;
          LRttiField.SetValue(Self, RestoreBlob);
          Continue;
        end;

        ValorCampo := TValue.FromVariant(DataSetField.Value);
        if LRttiField.FieldType.TypeKind = tkEnumeration then
        begin
          if DataSetField.DataType = ftBoolean then
            ValorCampo := DataSetField.AsBoolean
          else
          begin
            ValorCampo := DataSetField.AsInteger;
            LFieldPointer := Pointer(PByte(Self) + LRttiField.Offset);
            LTypedSmallInt := ValorCampo.AsInteger;
            Move(LTypedSmallInt, LFieldPointer^, LRttiField.FieldType.TypeSize);
            Continue;
          end;
        end;

        LRttiField.SetValue(Self, ValorCampo);
      end;
    end;

    LRttiProperties := LRttiType.GetProperties;
    for iProperty := 0 to Length(LRttiProperties) - 1 do
    begin
      LRttiProperty := LRttiProperties[iProperty];

      if not LRttiProperty.IsWritable then
        Continue;

      for LCustomAttribute in LRttiProperty.GetAttributes do
      begin
        if not MustUseThisAtribute(LCustomAttribute) then
          Continue;

        DataSetField := AFieldList.FieldByName(TAttrDBField(LCustomAttribute).FieldName);
        if DataSetField = nil then
          Continue;

        ValorCampo := TValue.FromVariant(DataSetField.Value);

        //if LRttiProperty.PropertyType.TypeKind = tkEnumeration then
        //Não é possível utilizar o método de setar Tipo para properties, por enquanto
        //não tem suporte até alguém descobrir como fazer isso.

        LRttiProperty.SetValue(Self, ValorCampo);
      end;
    end;

  except
    on E: Exception do
      Logger.Error('%s.LoadFromDataSet',[Self.ClassName], Self, E);
  end;
end;

function TReportItem.MustInsertInDataSet: Boolean;
begin

end;

procedure TReportItem.SetIsDeprecated(const Value: Boolean);
begin
  FIsDeprecated := Integer(Value);
end;

{ TAttrDataSet }

constructor TAttrDataSet.Create(AFieldName: string);
begin
  FFieldName := AFieldName;
end;

end.
