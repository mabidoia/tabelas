﻿unit uDBQueryCustom;

interface

uses
  Classes, Controls, uDBCustom, DB, DBClient;

type
  TDBQueryCustomField = class helper for TField
  public
    function GetAsBoolean: Boolean;
    function GetAsInteger: Integer;
    function GetAsInt64: Int64;
    function GetAsFloat: Real;
    function GetAsString: string;
    function GetAsDateTime: TDateTime;
    procedure GetAsStream(out AMemoryStream: TMemoryStream);
  end;

  TDBQueryCustom = class(TDBCustom)
  protected
    FMinDateTime: TDateTime;
    FMaxDateTime: TDateTime;

    // controle de acesso as properties
    function GetEof: Boolean; virtual; abstract;
    function GetFieldCount: Integer; virtual; abstract;
    function GetRecordCount: Integer; virtual; abstract;
    function GetFields: TFields; virtual; abstract;
    function GetFilter: string; virtual; abstract;
    function GetFiltered: Boolean; virtual; abstract;
    procedure SetFilter(const Value: string); virtual; abstract;
    procedure SetFiltered(const Value: Boolean); virtual; abstract;
  public
    procedure SetMinMaxDataTime(ADateTime: TDateTime);
    // controle de output
    function GetAsBoolean(AFieldName: string): Boolean; virtual; abstract;
    function GetAsInteger(AFieldName: string): Integer; virtual; abstract;
    function GetAsInt64(AFieldName: string): Int64; virtual; abstract;
    function GetAsFloat(AFieldName: string): Real; virtual; abstract;
    function GetAsString(AFieldName: string): string; virtual; abstract;
    function GetAsDateTime(AFieldName: string): TDateTime; virtual; abstract;
    function GetAsTime(AFieldName: string): TTime; virtual; abstract;
    procedure GetAsStream(AFieldName: string; out LMemoryStream: TMemoryStream); virtual; abstract;
    function IsNull(AFieldName: string): Boolean; virtual; abstract;
    function GetColumns: string; virtual; abstract;
    function Locate(const KeyFields: string; const KeyValues: Variant; Options: TLocateOptions): Boolean; virtual; abstract;
    function FieldByName(AFieldName: string): TField; virtual; abstract;
    procedure SetQueryRange(AMinDateTime, AMaxDateTime: TDateTime); virtual; abstract;
    // controle de navegação
    procedure Prior; virtual; abstract;
    procedure Next; virtual; abstract;
    procedure Last; virtual; abstract;
    procedure First; virtual; abstract;

    // controle adicional
    procedure InsertIntoClientDataSet(ADataSet: TClientDataSet); virtual; abstract;
    // properties
    property Eof: Boolean read GetEof;                  // Indicates whether a dataset is positioned at the last record
    property FieldCount: Integer read GetFieldCount;
    property RecordCount: Integer read GetRecordCount;
    property Fields: TFields read GetFields;
    property Filter: string read GetFilter write SetFilter;
    property Filtered: Boolean read GetFiltered write SetFiltered;

    property MinDateTime: TDateTime read FMinDateTime write FMinDateTime ;
    property MaxDateTime: TDateTime read FMaxDateTime write FMaxDateTime;
  end;

implementation

uses
  SysUtils;
{ TDBQueryCustom }

procedure TDBQueryCustom.SetMinMaxDataTime(ADateTime: TDateTime);
begin
  if FMinDateTime = 0 then
    FMinDateTime := ADateTime;

  if FMaxDateTime = 0 then
    FMaxDateTime := ADateTime;

  if ADateTime > FMaxDateTime then
    FMaxDateTime := ADateTime;

  if (FMinDateTime > 0) and (ADateTime < FMaxDateTime) then
    FMaxDateTime := ADateTime;

  if ADateTime < FMinDateTime then
    FMinDateTime := ADateTime;
end;

{ TDBQueryCustomField }
function TDBQueryCustomField.GetAsBoolean: Boolean;
begin
  if Self.IsNull then
  begin
    Result := False;
    Exit;
  end;

  try
    Result := Self.AsBoolean;
  except
    try
      if ((Self.AsString = '1') or (Self.AsString = '0')) then
        Result := Self.GetAsInteger > 0
      else if ((Self.AsString = 'TRUE') or (Self.AsString = 'FALSE')) then
        Result := Self.AsBoolean
      else
        Result := Self.AsBytes[0] <> 0;
    except
      Result := False;
    end;
  end;
end;

function TDBQueryCustomField.GetAsDateTime: TDateTime;
begin
  if Self.IsNull then
    Result := 0
  else
    Result := Self.AsDateTime;
end;

function TDBQueryCustomField.GetAsFloat: Real;
begin
  if Self.IsNull then
    Result := 0
  else
    Result := Self.AsFloat;
end;

function TDBQueryCustomField.GetAsInt64: Int64;
begin
  if Self.IsNull then
    Result := -1
  else
    Result := StrToInt64(Self.AsString);
end;

function TDBQueryCustomField.GetAsInteger: Integer;
begin
  if Self.IsNull then
    Result := -1
  else
    Result := Self.AsInteger
end;

procedure TDBQueryCustomField.GetAsStream(out AMemoryStream: TMemoryStream);
begin
  AMemoryStream := TMemoryStream.Create;
  if not TBlobField(Self).IsNull then
  begin
    TBlobField(Self).SaveToStream(AMemoryStream);
    AMemoryStream.Position := 0;
  end;
end;

function TDBQueryCustomField.GetAsString: string;
begin
  if Self.IsNull then
    Result := ''
  else
    Result := Self.AsString;
end;

end.
