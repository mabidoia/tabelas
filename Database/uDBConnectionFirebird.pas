﻿unit uDBConnectionFirebird;

interface

uses
  uDBConnectionCustom, uHelperLog, uDBHeader, IBSQLMonitor, DB, DBAccess,
  Classes, uDBException,  Uni, InterBaseUniProvider, StdCtrls;

type
  TUniConnectionFirebird = class(TUniConnection)
  private
    FUses: Integer;
  public
    procedure IncUses;
    procedure DecUses;

    property NumberOfUses: Integer read FUses;
  end;

  TDBConnectionFirebird = class(TDBConnectionCustom)
  private
    FUniConnection: TUniConnectionFirebird;
    FUniTransaction: TUniTransaction;
    FUniQuery: TUniQuery;
    FUniSQL: TUniSQL;
    FLocalTransaction: Boolean;
    FListSQLExecuted: TList;
    procedure ClearListIBSQLExecuted;
    function ProcessSQLOfTransaction: Boolean;
    procedure CreateTransaction;
    function GetTransaction: TUniTransaction;
  protected
  public
    // controle da classe
    constructor Create(ADatabase: TDatabases = dbPrincipal); override;
    destructor Destroy; override;
    // controle de conexão
    class function CreateDBConnection(ADatabase: TDatabases): TUniConnectionFirebird;
    class procedure CreateDatabases;
    class procedure CreateDatabase(ADatabaseName: string);

    function IsConnected: Boolean; override;
    procedure Reconnect; override;
    // controle de transações
    procedure BeginLocalTransaction;
    procedure BeginTransaction; override;
    procedure EndTransaction; override;
    function IsTransactionActive: Boolean; override;
    // execuções dos SQLs
    procedure ResetQuery; override;
    procedure OpenQuery; override;  // select
    procedure ExecSQL; override;    // insert, update, delete, create, drop, alter
    // properties
    property UniQuery: TUniQuery read FUniQuery;
    property UniSQL: TUniSQL read FUniSQL;
  end;

implementation

uses
  uExceptionEx, {$IFDEF SERVIDOR}uModelDatabase, uIntegracao, uInicializacao, uHeaderPermissoes, {$ENDIF} SysUtils, uDBPoolConnection, IdGlobal,
  IBDatabase, IniFiles, CRAccess;

const
  ID_BLOBFIELD = 520;

{ TDBConnectionFirebird }

procedure TDBConnectionFirebird.BeginLocalTransaction;
begin
  FLocalTransaction := not FUniTransaction.Active;
  if not FUniTransaction.Active then
    FUniTransaction.StartTransaction;
  if FLocalTransaction then
  begin
    FFailSQL := False;
    ClearListIBSQLExecuted;
  end;
end;

procedure TDBConnectionFirebird.BeginTransaction;
begin
  // Current transaction is guaranteed a consistent view of the data,
  // which includes only changes committed by other transactions at the start of the transaction.
  if not FUniTransaction.Active then
  begin
    FUniTransaction.StartTransaction;
    ClearListIBSQLExecuted;
  end;
  FFailSQL := False;
  FLocalTransaction := False;
end;

procedure TDBConnectionFirebird.ClearListIBSQLExecuted;
var
  i: Integer;
begin
  // libera memória dos IBSQLs executados na transação
  for i := FListSQLExecuted.Count - 1 downto 0 do
    TObject(FListSQLExecuted.Items[i]).Free;

  FListSQLExecuted.Clear;
end;

constructor TDBConnectionFirebird.Create(ADatabase: TDatabases);
begin
  FDatabase := ADatabase;

  FFailSQL := False;
  FRaiseException := True;

  // define o nome do banco de dados gabarito para validar a DDL do banco quando for atualizar o sistema
  FDatabasePrincipalGabarito := StringReplace(FDatabasePrincipal, '.fdb', '_gabarito.fdb', [rfReplaceAll, rfIgnoreCase]);
  FDatabasePrincipalGabarito := StringReplace(FDatabasePrincipalGabarito, '.gdb', '_gabarito.gdb', [rfReplaceAll, rfIgnoreCase]);
  FDatabaseIntegracaoGabarito := StringReplace(FDatabaseIntegracao, '.fdb', '_gabarito.fdb', [rfReplaceAll, rfIgnoreCase]);
  FDatabaseIntegracaoGabarito := StringReplace(FDatabaseIntegracaoGabarito, '.gdb', '_gabarito.gdb', [rfReplaceAll, rfIgnoreCase]);

  // obtém uma instância de conexão com o banco no pool de conexões
  // caso não tenha, cria uma nova conexão e insere no pool
  FUniConnection := TUniConnectionFirebird(DBPoolConnection.GetDBConnection(ADatabase, FThreadDBConnection));
  FUniConnection.IncUses;
  // a query será criada quando chamar o método SetSQL
  FUniTransaction := GetTransaction;
  if Assigned(FUniTransaction) then
    GenerateTransactionKey
  else
  begin
    FUniQuery := nil;
    FUniSQL := nil;
    CreateTransaction;
  end;

  // lista de todos os IBSQLs executados na transação atual
  // é utilizado para reprocessar os SQL quando ocorrer deadlock
  FListSQLExecuted := TList.Create;
end;

class procedure TDBConnectionFirebird.CreateDatabase(ADatabaseName: string);
var
  Database: TIBDatabase;
begin
  Database := TIBDatabase.Create(nil);
  try
    Database.DatabaseName := ADatabaseName;
    Database.SQLDialect := 3;
    Database.Params.Add('USER ''' + FUserName + '''');
    Database.Params.Add('PASSWORD ''' + FPassword + '''');
    Database.Params.Add('PAGE_SIZE 16384');
    Database.Params.Add('DEFAULT CHARACTER SET ISO8859_1');
    Database.CreateDatabase;
  except
    on E: Exception do
    begin
      Logger.DBError('TDBConnectionFirebird.CreateDatabase[%s]: %s', [ADatabaseName, E.Message]);
      Sleep(2000);
      Halt(HALT_ERROR);
    end;
  end;
  FreeAndNil(Database);
end;

class procedure TDBConnectionFirebird.CreateDatabases;
var
 LIni: TIniFile;

  function TryDBConnect(ADatabaseName: string): Boolean;
  var
    Database: TUniConnection;
  begin
    Database := TUniConnection.Create(nil);
    try
      Database.ProviderName := 'InterBase';
      Database.Database := ADatabaseName;
      Database.LoginPrompt := False;
      Database.Username := FUserName;
      Database.Password := FPassword;
      Database.Connected := True;
      Result := Database.Connected;
    except
      Result := False;
    end;
    Database.Free;
  end;

begin
  if not TryDBConnect(FDatabasePrincipal) then
    CreateDatabase(FDatabasePrincipal);

  {$IFDEF SERVIDOR}
  if Integracao.SaidaNoBanco and not TryDBConnect(FDatabaseIntegracao) then
    CreateDatabase(FDatabaseIntegracao);

  {$REGION 'Verificação para criação do banco de integração TSR'}
  try
    LIni := TIniFile.Create(Inicializacao.CaminhoAplicacao + 'config.ini');
    if (FDatabaseIntegracaoTSR <> '') and LIni.ReadBool('INTEGRACAO_TERMINAL_SERVICOS', 'ATIVO', False) then
    begin
      if not TryDBConnect(FDatabaseIntegracaoTSR) then
        CreateDatabase(FDatabaseIntegracaoTSR);
    end;
  finally
    LIni.Free;
  end;
  {$ENDREGION}
  {$ENDIF}
end;

class function TDBConnectionFirebird.CreateDBConnection(ADatabase: TDatabases): TUniConnectionFirebird;
begin
  // cria conexão com o banco de dados
  Result := TUniConnectionFirebird.Create(nil);
  Result.ProviderName := 'InterBase';
  case ADatabase of
    dbPrincipal: Result.Database := FDatabasePrincipal;
    dbIntegracao: Result.Database := FDatabaseIntegracao;
    dbPrincipalGabarito: Result.Database := FDatabasePrincipalGabarito;
    dbIntegracaoGabarito: Result.Database := FDatabaseIntegracaoGabarito;
    dbIntegracaoTSR: Result.Database := FDatabaseIntegracaoTSR;
  end;
  Result.LoginPrompt := False;
  Result.Username := FUserName;
  Result.Password := FPassword;
  try
    Result.Connected := True;
  except
    on E: exception do
      Logger.Error('TDBConnectionFirebird.CreateDBConnection', nil, E);

  end;
end;

procedure TDBConnectionFirebird.CreateTransaction;
begin
  if FUniTransaction <> nil then
    Exit;

  FUniTransaction := TUniTransaction.Create(nil);
  FUniTransaction.AddConnection(FUniConnection);
  GenerateTransactionKey;
end;

destructor TDBConnectionFirebird.Destroy;
begin
  try
    EndTransaction;
    FUniConnection.DecUses;
    if (FUniConnection.NumberOfUses = 0) and (FUniTransaction <> nil) and (FUniTransaction.Active) then
      FUniTransaction.Commit;
  except
    on E: exception do
    begin
      Logger.DBError('TDBConnectionFirebird.Destroy: [%s]', [E.Message]);
      Logger.ErroStackTrace(Self, E);
    end;
  end;

  // libera memória dos IBSQLs executados na transação
  ClearListIBSQLExecuted;
  FListSQLExecuted.Free;

  try
    FUniQuery.Free;
  except
    on E: exception do
    begin
      Logger.DBError('TDBConnectionFirebird.Destroy: [%s]', [E.Message]);
      Logger.ErroStackTrace(Self, E);
    end;
  end;

  try
    DBPoolConnection.FreeDBConnection(FDatabase);
  except
    on E: exception do
    begin
      Logger.DBError('TDBConnectionFirebird.Destroy: [%s]', [E.Message]);
      Logger.ErroStackTrace(Self, E);
    end;
  end;

  inherited;
end;

procedure TDBConnectionFirebird.EndTransaction;
begin
  // se alguma execução de SQL falhou, cancela as atualizações da transação
  try
    if not FUniTransaction.Active then
    begin
      FFailSQL := False;
      Exit;
    end;

    if FFailSQL then
    begin
      FUniTransaction.Rollback;
      {$IFDEF SERVIDOR}
      ListaTransacao.Rollback(FTransactionId);
      {$ENDIF}
    end else
    begin
      if (FUniTransaction <> nil) and (FUniTransaction.Active) then
      begin
        FUniTransaction.CommitRetaining;
        {$IFDEF SERVIDOR}
        ListaTransacao.Commit(FTransactionId);
        {$ENDIF}
      end;
    end;
    // "reseta" a flag de falha na execução do SQL
    FFailSQL := False;
  except
    on E: Exception do
    begin
      FFailSQL := False;
      Logger.DBError('TDBConnectionFirebird[%d| TransactionId: %d].EndTransaction: %s', [Integer(FDatabase), FTransactionId, E.Message]);
      ClearListIBSQLExecuted;
      {$IFDEF SERVIDOR}
      ListaTransacao.Rollback(FTransactionId);
      {$ENDIF}
      raise;
    end;
  end;
end;

procedure TDBConnectionFirebird.ExecSQL;
var
  Tentativas: Integer;
  LParametros: string;
  LDateTimeStart: TDateTime;

  procedure GerarInfoParams;
  var
    I: integer;
  begin
    if LParametros = '' then
    begin
      for I := 0 to FUniSQL.Params.Count - 1 do
      begin
        if FUniSQL.Params.Items[I].DataType <> ftBlob then
          LParametros := LParametros + Format(' (%s = %s)',[FUniSQL.Params.Items[I].Name,FUniSQL.Params.Items[I].AsString])
        else
          LParametros := LParametros + Format(' (%s = [BLOB])',[FUniSQL.Params.Items[I].Name])
      end;
    end;
  end;

begin
  // registra no log a SQL que será executada
  if LogSQL then
    if DBPoolConnection.RedirectToDBGabarito then
      Logger.Info('TDBConnectionFirebird[%d][gab].ExecSQL: %s', [Integer(FDatabase), Trim(FUniSQL.SQL.Text)])
    else
      Logger.Info('TDBConnectionFirebird[%d | TransactionId: %d].ExecSQL: %s', [Integer(FDatabase), FTransactionId, Trim(FUniSQL.SQL.Text)]);

  try
    LDateTimeStart := Now;
    // Execute returns the number of records affected by executing the SQL statement
    FThreadDBConnection.LastAcess := Now;
    FThreadDBConnection.Working := True;
    try
      try
        FUniSQL.Execute;
      except
        FFailSQL := True;
        raise;
      end;
    finally
      FThreadDBConnection.Working := False;
      if FLocalTransaction then
      begin
        FLocalTransaction := False;
        EndTransaction;
      end;
    end;

  except
    on E: Exception do
    begin
      try
        if FLocalTransaction then
        begin
          FLocalTransaction := False;
          EndTransaction;
        end;
        
        TDBException.ParserAndRaise(E.Message, FUniSQL.SQL.Text);
      except
        on E: TDBExceptionDeadlock do
        begin
          GerarInfoParams;
          Tentativas := 0;
          while Tentativas < 10 do
          begin
            Inc(Tentativas);
            Logger.Debug('TDBConnectionFirebird[%d | TransactionId: %d].ExecSQL: %s [%s] Sleep(200) and retry %d | Params [%s]', [
              Integer(FDatabase), FTransactionId, Trim(FUniSQL.SQL.Text), E.Message, Tentativas, LParametros]);
            Sleep(200);
            if ProcessSQLOfTransaction then
              Break;
          end;
        end;

        on E: TDBExceptionAccessViolation do
        begin
          Tentativas := 0;
          while Tentativas < 10 do
          begin
            Inc(Tentativas);
            Logger.Debug('TDBConnectionFirebird[%d| TransactionId: %d].ExecSQL: %s [%s] Sleep(200) and retry %d ', [
              Integer(FDatabase), FTransactionId, Trim(FUniSQL.SQL.Text), E.Message, Tentativas]);
            Sleep(200);
            if ProcessSQLOfTransaction then
              Break;
          end;
        end;

        on E: TDBException do
        begin
          // registra log de erro
          if (Pos(UNIQUE_KEY, E.Message) > 0) or (Pos(CHAVE_DUPLICADA, E.Message) > 0) or (Pos(FOREIGN_KEY, E.Message) > 0) then
          begin
            Logger.DBWarn('TDBConnectionFirebird[%d].ExecSQL: %s [%s]', [Integer(FDatabase), Trim(FUniSQL.SQL.Text), E.Message]);
            // para duplicadas, não é considerado que o SQL falhou
            FFailSQL := False;
          end
          else
          begin
            Logger.DBError('TDBConnectionFirebird[%d | TransactionId: %d].ExecSQL: %s [%s]', [Integer(FDatabase), FTransactionId, Trim(FUniSQL.SQL.Text), E.Message]);
            // seta a flag indicando que a execução do SQL falhou
            FFailSQL := True;
          end;

          if FRaiseException then
            raise
        end else
        begin
          // registra log de erro
          Logger.DBError('TDBConnectionFirebird[%d | TransactionId: %d].ExecSQL: %s [%s]', [Integer(FDatabase), FTransactionId, Trim(FUniSQL.SQL.Text), E.Message]);
          // seta a flag indicando que a execução do SQL falhou
          FFailSQL := True;
          if FRaiseException then
            raise
        end;
      end;
    end;
  end;
end;

function TDBConnectionFirebird.GetTransaction: TUniTransaction;
begin
  try
    if FUniConnection.InTransaction then
    begin
      Result := TUniTransaction(FUniConnection.Transactions[0])
    end
    else
    begin
      if FUniConnection.TransactionCount = 0 then
      begin
        Result := nil;
        Exit;
      end;

      Result := FUniConnection.Transactions[0];
    end;
  finally
    if Assigned(Result) and (not Result.Active) then
    begin
      Result.IsolationLevel := ilReadCommitted;
      Result.StartTransaction;
    end;
  end;
end;

function TDBConnectionFirebird.IsConnected: Boolean;
begin
  Result := Self.FUniConnection.Connected;
end;

function TDBConnectionFirebird.IsTransactionActive: boolean;
begin
  Result := FUniTransaction.Active;
end;

procedure TDBConnectionFirebird.OpenQuery;
var
  LParametros: string;
  LDateTimeStart: TDateTime;

  procedure GerarInfoParams;
  var
    I: integer;
  begin
    if (LParametros = '') and (FUniQuery.Params <> nil) then
    begin
      for I := 0 to FUniQuery.Params.Count - 1 do
      begin
        if FUniQuery.Params.Items[I].DataType <> ftBlob then
          LParametros := LParametros + Format(' (%s = %s)',[FUniQuery.Params.Items[I].Name,FUniQuery.Params.Items[I].AsString])
        else
          LParametros := LParametros + Format(' (%s = [BLOB])',[FUniQuery.Params.Items[I].Name])
      end;
    end;
  end;
  
begin
  // registra no log a SQL que será executada
  if LogSQL then
    if DBPoolConnection.RedirectToDBGabarito then
      Logger.Info('TDBConnectionFirebird[%d][gab].OpenQuery: %s | SqlOriginal: %s', [Integer(FDatabase), Trim(FUniQuery.SQL.Text), Trim(FUniQuery.SQLInsert.Text)])
    else
      Logger.Info('TDBConnectionFirebird[%d].OpenQuery: %s | SqlOriginal: %s', [Integer(FDatabase), Trim(FUniQuery.SQL.Text), Trim(FUniQuery.SQLInsert.Text)]);

  try
    LDateTimeStart := Now;

    //Open the query
    FThreadDBConnection.LastAcess := Now;
    FThreadDBConnection.Working := True;
    try
      FUniQuery.Open;
      FUniQuery.First;
    finally
      FThreadDBConnection.Working := False;
    end;


  except
    on E: Exception do
    begin
      // registra log de erro
      Logger.DBError('TDBConnectionFirebird[%d].OpenQuery: %s [%s]', [Integer(FDatabase), Trim(FUniQuery.SQL.Text), E.Message]);
      // lança a exceção se for necessário
      if FRaiseException then
        raise
    end;
  end;
end;

function TDBConnectionFirebird.ProcessSQLOfTransaction: Boolean;
var
  i: Integer;
begin
  try
    if FUniTransaction.Active then
      FUniTransaction.Rollback;
    FUniTransaction.StartTransaction;

    for i := 0 to FListSQLExecuted.Count - 1 do
      TUniSQL(FListSQLExecuted.Items[i]).Execute;

    if FLocalTransaction then
    begin
      FLocalTransaction := False;
      FUniTransaction.Commit;
    end;
    Result := True;
  except
    on E: Exception do
    begin
      try
        TDBException.ParserAndRaise(E.Message, FUniSQL.SQL.Text);
      except
        on E: TDBExceptionDeadlock do
        begin
          Result := False;
        end;

        on E: TDBException do
        begin
          // registra log de erro
          if E is TDBExceptionDuplicatedKey then
            Logger.DBWarn('TDBConnectionFirebird[%d].ExecSQL: %s [%s]', [Integer(FDatabase), Trim(FUniSQL.SQL.Text), E.Message])
          else
            Logger.DBError('TDBConnectionFirebird[%d].ExecSQL: %s [%s]', [Integer(FDatabase), FUniSQL.SQL.Text, E.Message]);
          // seta a flag indicando que a execução do SQL falhou
          FFailSQL := True;
          if FRaiseException then
            raise
        end else
        begin
          // registra log de erro
//          Logger.Error('TDBConnectionFirebird[%d].ExecSQL: %s [%s]', [Integer(FDatabase), FIBSQL.SQL.Text, E.Message]);
          // seta a flag indicando que a execução do SQL falhou
          FFailSQL := True;
          if FRaiseException then
            raise
        end;
      end;
    end;
  end;
end;

procedure TDBConnectionFirebird.Reconnect;
var
  LTransactionActive: Boolean;
begin
  LTransactionActive := FUniTransaction.Active;
  try
    FUniTransaction.Commit;
  except
  end;
  FUniConnection.Connected := False;
  FUniConnection.Connected := True;
  if LTransactionActive then
    FUniTransaction.StartTransaction;
end;

procedure TDBConnectionFirebird.ResetQuery;
begin
  FUniQuery.Free;

  // cria query para executar os SQLs
  FUniQuery := TUniQuery.Create(nil);
  FUniQuery.Connection := FUniConnection;
  FUniQuery.Transaction := FUniTransaction;
  FUniQuery.UniDirectional := True;
  FUniQuery.DisableControls;

  FUniSQL := TUniSQL.Create(nil);
  FUniSQL.Connection := FUniConnection;
  FUniSQL.Transaction := FUniTransaction;

  FListSQLExecuted.Add(FUniSQL);
end;

{ TUniConnectionFirebird }

procedure TUniConnectionFirebird.DecUses;
begin
  Dec(FUses);
end;

procedure TUniConnectionFirebird.IncUses;
begin
  Inc(FUses);
end;

end.
