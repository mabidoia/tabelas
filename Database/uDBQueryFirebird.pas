﻿unit uDBQueryFirebird;

interface

uses
  uDBQueryCustom, uDBConnectionFirebird, Classes, Controls, uUtils, FieldByNameSpeedUp,
  uDBConnectionCustom, uDBHeader, DB, DBClient, uHelperLog;

type
  TDBQueryFirebird = class(TDBQueryCustom)
  private
    FDBConnection: TDBConnectionFirebird;
    FFields: iFieldList;
    function GetFieldCount: Integer; override;
    function GetRecordCount: Integer; override;
    function GetFields: TFields; override;
  protected
    // controle de acesso as properties
    function GetEof: Boolean; override;
    function GetFilter: string; override;
    function GetFiltered: Boolean; override;
    function GetLogSQL: Boolean; override;
    function GetRaiseException: Boolean; override;
    procedure SetLogSQL(const Value: Boolean); override;
    procedure SetRaiseException(const Value: Boolean); override;
    procedure SetFilter(const Value: string); override;
    procedure SetFiltered(const Value: Boolean); override;
    function GetTransactionId: Int64; override;
  public
    // controle da classe
    constructor Create(ADatabase: TDatabases);
    destructor Destroy; override;
    // controle de transações
    procedure BeginTransaction; override;
    procedure EndTransaction; override;
    // processamento do sql
    procedure Execute(HasReturn: Boolean = False; VerificarTabela: Boolean = True); override;
    procedure ExecuteDirect(SQL: string); override;
    procedure ExecuteDirect(SQL: string; Args: array of const); override;
    // controle de input
    procedure SetSQL(ASQL: string); overload; override;
    procedure SetSQL(ASQL: string; Args: array of const); overload; override;
    procedure SetParam(AParam: string; Value: Boolean); override;
    procedure SetParam(AParam: string; Value: Integer; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: Int64; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: Real; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: string; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: TMemoryStream; ForceNull: Boolean = True); override;
    procedure SetParamTime(AParam: string; Value: TTime; ForceNull: Boolean = True); override;
    procedure SetParamDate(AParam: string; Value: TDate; ForceNull: Boolean = True); override;
    procedure SetParamDateTime(AParam: string; Value: TDateTime; ForceNull: Boolean = True); override;
    // controle de output
    function GetAsBoolean(AFieldName: string): Boolean; override;
    function GetAsInteger(AFieldName: string): Integer; override;
    function GetAsInt64(AFieldName: string): Int64; override;
    function GetAsFloat(AFieldName: string): Real; override;
    function GetAsString(AFieldName: string): string; override;
    function GetAsDateTime(AFieldName: string): TDateTime; override;
    procedure GetAsStream(AFieldName: string; out AMemoryStream: TMemoryStream); override;
    function IsNull(AFieldName: string): Boolean; override;
    function GetColumns: string; override;
    function Locate(const KeyFields: string; const KeyValues: Variant; Options: TLocateOptions): Boolean; override;
    function FieldByName(AFieldName: string): TField; override;
    // controle de navegação
    procedure Prior; override;
    procedure Next; override;
    procedure Last; override;
    procedure First; override;
    // controle adicional
    procedure InsertIntoClientDataSet(ADataSet: TClientDataSet); override;
    // properties
    property Eof: Boolean read GetEof;                  // Indicates whether a dataset is positioned at the last record
    property FieldCount: Integer read GetFieldCount;
    property RecordCount: Integer read GetRecordCount;
    property Fields: TFields read GetFields;
    property Filter: string read GetFilter write SetFilter;
    property Filtered: Boolean read GetFiltered write SetFiltered;
  end;

implementation

uses
  SysUtils, SqlTimSt, Variants, Windows;

{ TDBQueryFirebird }

procedure TDBQueryFirebird.BeginTransaction;
begin
  FDBConnection.BeginTransaction;
end;

constructor TDBQueryFirebird.Create(ADatabase: TDatabases);
begin
  FDBConnection := TDBConnectionFirebird.Create(ADatabase);
  FMinDateTime := 0;
  FMaxDateTime := Now;
end;

destructor TDBQueryFirebird.Destroy;
begin
  try
    FDBConnection.Free;
  except
    on E: exception do
      Logger.DBError('Erro no FDBConnection.Free: ' + E.Message);
  end;
  inherited;
end;

procedure TDBQueryFirebird.EndTransaction;
begin
  FDBConnection.EndTransaction;
end;

procedure TDBQueryFirebird.Execute(HasReturn: Boolean; VerificarTabela: Boolean);
begin
  try
    if VerificarTabela then
    begin
      FDBConnection.UniQuery.SQLInsert.Text := FDBConnection.UniQuery.SQL.Text;
    end;

    FDBConnection.OpenQuery;
    FFields := GetFieldList(FDBConnection.UniQuery);
  except
    on E: exception do
      Logger.DBError('Erro no TDBQueryFirebird.Execute: ' + E.Message);
  end;
end;

procedure TDBQueryFirebird.ExecuteDirect(SQL: string; Args: array of const);
begin
  ExecuteDirect(Format(SQL, Args));
end;

procedure TDBQueryFirebird.ExecuteDirect(SQL: string);
begin
  SetSQL(SQL);
  Execute;
end;

function TDBQueryFirebird.FieldByName(AFieldName: string): TField;
begin
  Result := FFields.FieldByName(AFieldName);
end;

procedure TDBQueryFirebird.First;
begin
  FDBConnection.UniQuery.First;
end;

function TDBQueryFirebird.GetAsBoolean(AFieldName: string): Boolean;
begin
  Result := FieldByName(AFieldName).GetAsBoolean;
end;

function TDBQueryFirebird.GetAsDateTime(AFieldName: string): TDateTime;
begin
  Result := FieldByName(AFieldName).GetAsDateTime;
end;

function TDBQueryFirebird.GetAsFloat(AFieldName: string): Real;
begin
  Result := FieldByName(AFieldName).GetAsFloat;
end;

function TDBQueryFirebird.GetAsInt64(AFieldName: string): Int64;
begin
  Result := FieldByName(AFieldName).GetAsInt64;
end;

function TDBQueryFirebird.GetAsInteger(AFieldName: string): Integer;
begin
  Result := FieldByName(AFieldName).GetAsInteger;
end;

procedure TDBQueryFirebird.GetAsStream(AFieldName: string; out AMemoryStream: TMemoryStream);
begin
  FieldByName(AFieldName).GetAsStream(AMemoryStream);
end;

function TDBQueryFirebird.GetAsString(AFieldName: string): string;
begin
  Result := FieldByName(AFieldName).GetAsString;
end;

function TDBQueryFirebird.GetColumns: string;
var
  I: Integer;
begin
  Result := '';

  for I := 0 to FDBConnection.UniQuery.FieldCount - 1 do
    Result := Result + FDBConnection.UniQuery.Fields[i].FieldName + ',';
end;

function TDBQueryFirebird.GetEof: Boolean;
begin
  Result := FDBConnection.UniQuery.Eof;
end;

function TDBQueryFirebird.GetFieldCount: Integer;
begin
  Result := FDBConnection.UniQuery.FieldCount;
end;

function TDBQueryFirebird.GetRecordCount: Integer;
begin
  Result := FDBConnection.UniQuery.RecordCount;
end;

function TDBQueryFirebird.GetFields: TFields;
begin
  Result := FDBConnection.UniQuery.Fields;
end;

function TDBQueryFirebird.GetFilter: string;
begin
  Result := FDBConnection.UniQuery.Filter;
end;

function TDBQueryFirebird.GetFiltered: Boolean;
begin
  Result := FDBConnection.UniQuery.Filtered;
end;

function TDBQueryFirebird.GetLogSQL: Boolean;
begin
  Result := FDBConnection.LogSQL;
end;

function TDBQueryFirebird.GetRaiseException: Boolean;
begin
  Result := FDBConnection.RaiseException;
end;

function TDBQueryFirebird.GetTransactionId: Int64;
begin
  Result := FDBConnection.TransactionId;
end;

procedure TDBQueryFirebird.InsertIntoClientDataSet(ADataSet: TClientDataSet);
var
  i: integer;
begin
  for i := 0 to FDBConnection.UniQuery.FieldDefs.Count - 1 do
  begin
    if FDBConnection.UniQuery.FieldDefs[i].DataType = ftWideString  then
    begin
      ADataSet.FieldDefs.Add(
      FDBConnection.UniQuery.FieldDefs[i].Name,
      ftString,
      FDBConnection.UniQuery.FieldDefs[i].Size,
      FDBConnection.UniQuery.FieldDefs[i].Required
      );
    end
    else
    ADataSet.FieldDefs.Add(
      FDBConnection.UniQuery.FieldDefs[i].Name,
      FDBConnection.UniQuery.FieldDefs[i].DataType,
      FDBConnection.UniQuery.FieldDefs[i].Size,
      FDBConnection.UniQuery.FieldDefs[i].Required
    );
  end;
  ADataSet.CreateDataSet;

  // povoa o dataset com o resultado da query
  while not Eof do
  begin
    // cria novo registro do CDS
    ADataSet.Append;
    // insere os valores dos campos no CDS
    for i := 0 to ADataSet.Fields.Count - 1 do
      if not FDBConnection.UniQuery.Fields[i].IsNull then
      begin
        if FDBConnection.UniQuery.FieldDefs[i].DataType = ftWideString then
          ADataSet.Fields[i].Value := WideStringToString(FDBConnection.UniQuery.Fields[i].Value, CP_ACP)
        else
          ADataSet.Fields[i].Value := FDBConnection.UniQuery.Fields[i].Value;
      end;
    // salva o registro no CDS
    ADataSet.Post;
    // carrega próximo resultado da consulta
    Next;
  end;
end;

function TDBQueryFirebird.IsNull(AFieldName: string): Boolean;
begin
  Result := FFields.FieldByName(AFieldName).IsNull;
end;

procedure TDBQueryFirebird.Last;
begin
  FDBConnection.UniQuery.Last;
end;

function TDBQueryFirebird.Locate(const KeyFields: string; const KeyValues: Variant; Options: TLocateOptions): Boolean;
begin
  Result := FDBConnection.UniQuery.Locate(KeyFields, KeyValues, Options);
end;

procedure TDBQueryFirebird.Next;
begin
  FDBConnection.UniQuery.Next;
end;

procedure TDBQueryFirebird.Prior;
begin
  FDBConnection.UniQuery.Prior;
end;

procedure TDBQueryFirebird.SetParam(AParam: string; Value: Real; ForceNull: Boolean = True);
begin
  FDBConnection.UniQuery.ParamByName(AParam).AsFloat := Value;
end;

procedure TDBQueryFirebird.SetParam(AParam: string; Value: Integer; ForceNull: Boolean = True);
begin
  FDBConnection.UniQuery.ParamByName(AParam).AsInteger := Value;
end;

procedure TDBQueryFirebird.SetParam(AParam: string; Value: Boolean);
begin
  if Value then
    FDBConnection.UniQuery.ParamByName(AParam).AsSmallInt := 1
  else
    FDBConnection.UniQuery.ParamByName(AParam).AsSmallInt := 0;
end;

procedure TDBQueryFirebird.SetParam(AParam, Value: string; ForceNull: Boolean = True);
begin
  FDBConnection.UniQuery.ParamByName(AParam).AsString := Value;
end;

procedure TDBQueryFirebird.SetFilter(const Value: string);
begin
  FDBConnection.UniQuery.Filter := Value;
end;

procedure TDBQueryFirebird.SetFiltered(const Value: Boolean);
begin
  FDBConnection.UniQuery.Filtered := Value;
end;

procedure TDBQueryFirebird.SetLogSQL(const Value: Boolean);
begin
  FDBConnection.LogSQL := Value;
end;

procedure TDBQueryFirebird.SetParam(AParam: string; Value: TMemoryStream; ForceNull: Boolean = True);
var
  LParam: TParam;
begin
  Value.Position := 0;
  LParam := FDBConnection.UniQuery.ParamByName(AParam);
  LParam.ParamType := ptInput;
  LParam.DataType := ftBlob;

  if Value.Size > 0 then
    LParam.SetBlobData(Value.Memory, Value.Size)
  else
    LParam.Clear;
end;

procedure TDBQueryFirebird.SetParam(AParam: string; Value: Int64; ForceNull: Boolean = True);
begin
  FDBConnection.UniQuery.ParamByName(AParam).Value := Value;
end;

procedure TDBQueryFirebird.SetParamDate(AParam: string; Value: TDate; ForceNull: Boolean = True);
begin
  FDBConnection.UniQuery.ParamByName(AParam).AsDate := Value;
  SetMinMaxDataTime(Value);
end;

procedure TDBQueryFirebird.SetParamDateTime(AParam: string; Value: TDateTime; ForceNull: Boolean = True);
begin
  FDBConnection.UniQuery.ParamByName(AParam).AsDateTime := Value;
  SetMinMaxDataTime(Value);
end;

procedure TDBQueryFirebird.SetParamTime(AParam: string; Value: TTime; ForceNull: Boolean = True);
begin
  FDBConnection.UniQuery.ParamByName(AParam).AsTime := Value;
end;

procedure TDBQueryFirebird.SetRaiseException(const Value: Boolean);
begin
  FDBConnection.RaiseException := Value;
end;

procedure TDBQueryFirebird.SetSQL(ASQL: string; Args: array of const);
begin
  SetSQL(Format(ASQL, Args));
end;

procedure TDBQueryFirebird.SetSQL(ASQL: string);
begin
  FDBConnection.ResetQuery;
  FMinDateTime := 0;
  FMaxDateTime := Now;
  FDBConnection.UniQuery.SQL.Text := ASQL;
end;

end.
