﻿unit uDBQuerySQLServer;

interface

uses
  uDBQueryCustom, uDBConnectionSQLServer, Classes, Controls, uUtils, FieldByNameSpeedUp,
  uDBConnectionCustom, uDBHeader, DB, DBClient, uHelperLog;

type
  TDBQuerySQLServer = class(TDBQueryCustom)
  private
    FDBConnection: TDBConnectionSQLServer;
    FFields: iFieldList;
    function GetFieldCount: Integer; override;
    function GetRecordCount: Integer; override;
    function GetFields: TFields; override;
  protected
    // controle de acesso as properties
    function GetEof: Boolean; override;
    function GetFilter: string; override;
    function GetFiltered: Boolean; override;
    function GetLogSQL: Boolean; override;
    function GetRaiseException: Boolean; override;
    procedure SetLogSQL(const Value: Boolean); override;
    procedure SetRaiseException(const Value: Boolean); override;
    procedure SetFilter(const Value: string); override;
    procedure SetFiltered(const Value: Boolean); override;
    function GetTransactionId: Int64; override;
  public
    // controle da classe
    constructor Create(ADatabase: TDatabases);
    destructor Destroy; override;
    // controle de transações
    procedure BeginTransaction; override;
    procedure EndTransaction; override;
    // processamento do sql
    procedure Execute(HasReturn: Boolean = False; VerificarTabela: Boolean = True); override;
    procedure ExecuteDirect(SQL: string); override;
    procedure ExecuteDirect(SQL: string; Args: array of const); override;
    // controle de input
    procedure SetSQL(ASQL: string); overload; override;
    procedure SetSQL(ASQL: string; Args: array of const); overload; override;
    procedure SetParam(AParam: string; Value: Boolean); override;
    procedure SetParam(AParam: string; Value: Integer; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: Int64; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: Real; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: string; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: TMemoryStream; ForceNull: Boolean = True); override;
    procedure SetParamTime(AParam: string; Value: TTime; ForceNull: Boolean = True); override;
    procedure SetParamDate(AParam: string; Value: TDate; ForceNull: Boolean = True); override;
    procedure SetParamDateTime(AParam: string; Value: TDateTime; ForceNull: Boolean = True); override;
    // controle de output
    function GetAsBoolean(AFieldName: string): Boolean; override;
    function GetAsInteger(AFieldName: string): Integer; override;
    function GetAsInt64(AFieldName: string): Int64; override;
    function GetAsFloat(AFieldName: string): Real; override;
    function GetAsString(AFieldName: string): string; override;
    function GetAsDateTime(AFieldName: string): TDateTime; override;
    procedure GetAsStream(AFieldName: string; out AMemoryStream: TMemoryStream); override;
    function IsNull(AFieldName: string): Boolean; override;
    function GetColumns: string; override;
    function Locate(const KeyFields: string; const KeyValues: Variant; Options: TLocateOptions): Boolean; override;
    function FieldByName(AFieldName: string): TField; override;
    // controle de navegação
    procedure Next; override;
    procedure Prior; override;
    procedure Last; override;
    procedure First; override;
    // controle adicional
    procedure InsertIntoClientDataSet(ADataSet: TClientDataSet); override;
    // properties
    property Eof: Boolean read GetEof;                  // Indicates whether a dataset is positioned at the last record
    property FieldCount: Integer read GetFieldCount;
    property RecordCount: Integer read GetRecordCount;
    property Fields: TFields read GetFields;
    property Filter: string read GetFilter write SetFilter;
    property Filtered: Boolean read GetFiltered write SetFiltered;
  end;

implementation

uses
  SysUtils, SqlTimSt, Variants, Windows;

{ TDBQuerySQLServer }

procedure TDBQuerySQLServer.BeginTransaction;
begin
  FDBConnection.BeginTransaction;
end;

constructor TDBQuerySQLServer.Create(ADatabase: TDatabases);
begin
  FDBConnection := TDBConnectionSQLServer.Create(ADatabase);
  FMinDateTime := 0;
  FMaxDateTime := Now;
end;

destructor TDBQuerySQLServer.Destroy;
begin
  FDBConnection.Free;

  inherited;
end;

procedure TDBQuerySQLServer.EndTransaction;
begin
  FDBConnection.EndTransaction;
end;

procedure TDBQuerySQLServer.Execute(HasReturn: Boolean; VerificarTabela: Boolean);
begin
  try
    if VerificarTabela then
    begin
      FDBConnection.UniQuery.SQLInsert.Text := FDBConnection.UniQuery.SQL.Text;
    end;

    FDBConnection.OpenQuery;
    FFields := GetFieldList(FDBConnection.UniQuery);
  except
    on E: exception do
      Logger.DBError(Format('Erro no TDBQuerySQLServer.Execute: [%s][SQL = %s]',[E.Message, FDBConnection.UniQuery.SQL.Text]));
  end;
end;

procedure TDBQuerySQLServer.ExecuteDirect(SQL: string; Args: array of const);
begin
  ExecuteDirect(Format(SQL, Args));
end;

procedure TDBQuerySQLServer.ExecuteDirect(SQL: string);
begin
  SetSQL(SQL);
  Execute;
end;

function TDBQuerySQLServer.FieldByName(AFieldName: string): TField;
begin
  Result := FFields.FieldByName(AFieldName);
end;

procedure TDBQuerySQLServer.First;
begin
  FDBConnection.UniQuery.First;
end;

function TDBQuerySQLServer.GetAsBoolean(AFieldName: string): Boolean;
var
  LField: TField;
begin
  LField := FFields.FieldByName(AFieldName);

  Result := LField.GetAsBoolean;
end;

function TDBQuerySQLServer.GetAsDateTime(AFieldName: string): TDateTime;
var
  LField: TField;
begin
  LField := FFields.FieldByName(AFieldName);

  Result := LField.GetAsDateTime;
end;

function TDBQuerySQLServer.GetAsFloat(AFieldName: string): Real;
var
  LField: TField;
begin
  LField := FFields.FieldByName(AFieldName);

  Result := LField.GetAsFloat;
end;

function TDBQuerySQLServer.GetAsInt64(AFieldName: string): Int64;
var
  LField: TField;
begin
  LField := FFields.FieldByName(AFieldName);

  Result := LField.GetAsInt64;
end;

function TDBQuerySQLServer.GetAsInteger(AFieldName: string): Integer;
var
  LField: TField;
begin
  LField := FFields.FieldByName(AFieldName);

  Result := LField.GetAsInteger;
end;

procedure TDBQuerySQLServer.GetAsStream(AFieldName: string; out AMemoryStream: TMemoryStream);
var
  LField: TField;
begin
  LField := FFields.FieldByName(AFieldName);

  LField.GetAsStream(AMemoryStream);
end;

function TDBQuerySQLServer.GetAsString(AFieldName: string): string;
var
  LField: TField;
begin
  LField := FFields.FieldByName(AFieldName);

  Result := LField.GetAsString;
end;

function TDBQuerySQLServer.GetColumns: string;
var
  I: Integer;
begin
  Result := '';

  for I := 0 to FDBConnection.UniQuery.FieldCount - 1 do
    Result := Result + FDBConnection.UniQuery.Fields[i].FieldName + ',';
end;

function TDBQuerySQLServer.GetEof: Boolean;
begin
  Result := FDBConnection.UniQuery.Eof;
end;

function TDBQuerySQLServer.GetFieldCount: Integer;
begin
  Result := FDBConnection.UniQuery.FieldCount;
end;

function TDBQuerySQLServer.GetFields: TFields;
begin
  Result := FDBConnection.UniQuery.Fields;
end;

function TDBQuerySQLServer.GetFilter: string;
begin
  Result := FDBConnection.UniQuery.Filter;
end;

function TDBQuerySQLServer.GetFiltered: Boolean;
begin
  Result := FDBConnection.UniQuery.Filtered;
end;

function TDBQuerySQLServer.GetLogSQL: Boolean;
begin
  Result := FDBConnection.LogSQL;
end;

function TDBQuerySQLServer.GetRaiseException: Boolean;
begin
  Result := FDBConnection.RaiseException;
end;

function TDBQuerySQLServer.GetRecordCount: Integer;
begin
  Result := FDBConnection.UniQuery.RecordCount;
end;

function TDBQuerySQLServer.GetTransactionId: Int64;
begin
  Result := FDBConnection.TransactionId;
end;

procedure TDBQuerySQLServer.InsertIntoClientDataSet(ADataSet: TClientDataSet);
var
  i: integer;
begin
  for i := 0 to FDBConnection.UniQuery.Fields.Count - 1 do
  begin
    if FDBConnection.UniQuery.Fields[i].DataType = ftWideString  then
    begin
      ADataSet.FieldDefs.Add(
      FDBConnection.UniQuery.Fields[i].FieldName,
      ftString,
      FDBConnection.UniQuery.Fields[i].Size,
      FDBConnection.UniQuery.Fields[i].Required
      );
    end
    else
    ADataSet.FieldDefs.Add(
      FDBConnection.UniQuery.Fields[i].FieldName,
      FDBConnection.UniQuery.Fields[i].DataType,
      FDBConnection.UniQuery.Fields[i].Size,
      FDBConnection.UniQuery.Fields[i].Required
    );
  end;
  ADataSet.CreateDataSet;

  // povoa o dataset com o resultado da query
  while not Eof do
  begin
    // cria novo registro do CDS
    ADataSet.Append;
    // insere os valores dos campos no CDS
    for i := 0 to ADataSet.Fields.Count - 1 do
      if not FDBConnection.UniQuery.Fields[i].IsNull then
      begin
        if FDBConnection.UniQuery.FieldDefs[i].DataType = ftWideString then
          ADataSet.Fields[i].Value := WideStringToString(FDBConnection.UniQuery.Fields[i].Value, CP_ACP)
        else
          ADataSet.Fields[i].Value := FDBConnection.UniQuery.Fields[i].Value;
      end;
    // salva o registro no CDS
    ADataSet.Post;
    // carrega próximo resultado da consulta
    Next;
  end;
end;

function TDBQuerySQLServer.IsNull(AFieldName: string): Boolean;
begin
  Result := FFields.FieldByName(AFieldName).IsNull;
end;

procedure TDBQuerySQLServer.Last;
begin
  FDBConnection.UniQuery.Last;
end;

function TDBQuerySQLServer.Locate(const KeyFields: string; const KeyValues: Variant; Options: TLocateOptions): Boolean;
begin
  Result := FDBConnection.UniQuery.Locate(KeyFields, KeyValues, Options);
end;

procedure TDBQuerySQLServer.Next;
begin
  FDBConnection.UniQuery.Next;
end;

procedure TDBQuerySQLServer.Prior;
begin
  FDBConnection.UniQuery.Prior;
end;

procedure TDBQuerySQLServer.SetParam(AParam: string; Value: Real; ForceNull: Boolean = True);
begin
  FDBConnection.UniQuery.ParamByName(AParam).AsFloat := Value;
end;

procedure TDBQuerySQLServer.SetParam(AParam: string; Value: Integer; ForceNull: Boolean = True);
begin
  FDBConnection.UniQuery.ParamByName(AParam).AsInteger := Value;
end;

procedure TDBQuerySQLServer.SetParam(AParam: string; Value: Boolean);
begin
  if Value then
    FDBConnection.UniQuery.ParamByName(AParam).AsSmallInt := 1
  else
    FDBConnection.UniQuery.ParamByName(AParam).AsSmallInt := 0;
end;

procedure TDBQuerySQLServer.SetParam(AParam, Value: string; ForceNull: Boolean = True);
begin
  FDBConnection.UniQuery.ParamByName(AParam).AsString := Value;
end;

procedure TDBQuerySQLServer.SetFilter(const Value: string);
begin
  FDBConnection.UniQuery.Filter := Value;
end;

procedure TDBQuerySQLServer.SetFiltered(const Value: Boolean);
begin
  FDBConnection.UniQuery.Filtered := Value;
end;

procedure TDBQuerySQLServer.SetLogSQL(const Value: Boolean);
begin
  FDBConnection.LogSQL := Value;
end;

procedure TDBQuerySQLServer.SetParam(AParam: string; Value: TMemoryStream; ForceNull: Boolean = True);
var
  LParam: TParam;
begin
  Value.Position := 0;
  LParam := FDBConnection.UniQuery.ParamByName(AParam);
  LParam.ParamType := ptInput;
  LParam.DataType := ftBlob;

  if Value.Size > 0 then
    LParam.SetBlobData(Value.Memory, Value.Size)
  else
    LParam.Clear;
end;

procedure TDBQuerySQLServer.SetParam(AParam: string; Value: Int64; ForceNull: Boolean = True);
begin
  FDBConnection.UniQuery.ParamByName(AParam).Value := Value;
end;

procedure TDBQuerySQLServer.SetParamDate(AParam: string; Value: TDate; ForceNull: Boolean = True);
begin
  FDBConnection.UniQuery.ParamByName(AParam).AsDate := Value;
  SetMinMaxDataTime(Value);
end;

procedure TDBQuerySQLServer.SetParamDateTime(AParam: string; Value: TDateTime; ForceNull: Boolean = True);
begin
  FDBConnection.UniQuery.ParamByName(AParam).AsDateTime := Value;
  SetMinMaxDataTime(Value);
end;

procedure TDBQuerySQLServer.SetParamTime(AParam: string; Value: TTime; ForceNull: Boolean = True);
begin
  FDBConnection.UniQuery.ParamByName(AParam).AsTime := Value;
end;

procedure TDBQuerySQLServer.SetRaiseException(const Value: Boolean);
begin
  FDBConnection.RaiseException := Value;
end;

procedure TDBQuerySQLServer.SetSQL(ASQL: string; Args: array of const);
begin
  SetSQL(Format(ASQL, Args));
end;

procedure TDBQuerySQLServer.SetSQL(ASQL: string);
begin
  FDBConnection.ResetQuery;
  FMinDateTime := 0;
  FMaxDateTime := Now;
  FDBConnection.UniQuery.SQL.Text := ASQL;
end;

end.
