﻿unit uDBManipulationCustom;

interface

uses
  uDBCustom, uDBHeader;

type
  TDBManipulationCustom = class(TDBCustom)
  protected
    function GetFailSQL: Boolean; virtual; abstract;
    function GetRowsAffected: Integer; virtual; abstract;
  public
    // controle de conexão
    procedure Reconnect; virtual; abstract;
    // processamento do sql
    procedure ExecuteBlock(SQL: string); virtual; abstract;
    procedure SetSQLInsert(ASQL, AReturnField: string); overload; virtual; abstract;
    // controle de output (returning)
    function GetAsInteger(AFieldName: string): Integer; virtual; abstract;
    property FailSQL: Boolean read GetFailSQL;
    property RowsAffected: Integer read GetRowsAffected;
  end;

implementation

{ TDBManipulationCustom }

end.


