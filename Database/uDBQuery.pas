﻿unit uDBQuery;

interface

uses
  uDBQueryCustom, Classes, Controls, uDBHeader, DB, DBClient, Windows;

type
  TDBQuery = class(TDBQueryCustom)
  private
    //Só utilizada pela query que converte as posições
    FConvertPositions: Boolean;
    FDBQuery: TDBQueryCustom;
    function GetFieldCount: Integer; override;
    function GetRecordCount: Integer; override;
    function GetFields: TFields; override;
  protected
    // controle de acesso as properties
    function GetEof: Boolean; override;
    function GetFilter: string; override;
    function GetFiltered: Boolean; override;
    function GetLogSQL: Boolean; override;
    function GetRaiseException: Boolean; override;
    procedure SetLogSQL(const Value: Boolean); override;
    procedure SetRaiseException(const Value: Boolean); override;
    procedure SetFilter(const Value: string); override;
    procedure SetFiltered(const Value: Boolean); override;
  public
    // controle da classe
    constructor Create(ADatabase: TDatabases = dbPrincipal);
    destructor Destroy; override;
    // controle de transações
    procedure BeginTransaction; override;
    procedure EndTransaction; override;
    // processamento do sql
    procedure Execute(HasReturn: Boolean = False; VerificarTabela: Boolean = True); override;
    procedure ExecuteDirect(SQL: string); override;
    procedure ExecuteDirect(SQL: string; Args: array of const); override;
    // controle de input
    procedure SetSQL(ASQL: string); overload; override;
    procedure SetSQL(ASQL: string; Args: array of const); overload; override;
    procedure SetParam(AParam: string; Value: Boolean); override;
    procedure SetParam(AParam: string; Value: Integer; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: Int64; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: Real; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: string; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: TMemoryStream; ForceNull: Boolean = True); override;
    procedure SetParamTime(AParam: string; Value: TTime; ForceNull: Boolean = True); override;
    procedure SetParamDate(AParam: string; Value: TDate; ForceNull: Boolean = True); override;
    procedure SetParamDateTime(AParam: string; Value: TDateTime; ForceNull: Boolean = True); override;
    procedure SetQueryRange(AMinDateTime, AMaxDateTime: TDateTime);
    // controle de output
    function GetAsBoolean(AFieldName: string): Boolean; override;
    function GetAsInteger(AFieldName: string): Integer; override;
    function GetAsInt64(AFieldName: string): Int64; override;
    function GetAsFloat(AFieldName: string): Real; override;
    function GetAsString(AFieldName: string): string; override;
    function GetAsDateTime(AFieldName: string): TDateTime; override;
    function GetAsTime(AFieldName: string): TTime; override;
    procedure GetAsStream(AFieldName: string; out AMemoryStream: TMemoryStream); override;
    function IsNull(AFieldName: string): Boolean; override;
    function GetColumns: string; override;
    function Locate(const KeyFields: string; const KeyValues: Variant; Options: TLocateOptions): Boolean; override;
    function FieldByName(AFieldName: string): TField; override;
    // controle de navegação
    procedure Prior; override;
    procedure Next; override;
    procedure Last; override;
    procedure First; override;

    // controle adicional
    procedure InsertIntoClientDataSet(ADataSet: TClientDataSet); override;
    // properties
    property Eof: Boolean read GetEof;                  // Indicates whether a dataset is positioned at the last record
    property FieldCount: Integer read GetFieldCount;
    property RecordCount: Integer read GetRecordCount;
    property Fields: TFields read GetFields;
    property Filter: string read GetFilter write SetFilter;
    property Filtered: Boolean read GetFiltered write SetFiltered;
    property ConvertPosition: Boolean read FConvertPositions write FConvertPositions;
  end;

implementation

uses
  uDBQueryFirebird, uDBQuerySQLServer, uDBConnectionCustom;

{ TDBQuery }

procedure TDBQuery.BeginTransaction;
begin
  FDBQuery.BeginTransaction;
end;

constructor TDBQuery.Create(ADatabase: TDatabases);
begin
  case DBSettings.SGBDType of
    sgbdFirebird:  FDBQuery := TDBQueryFirebird.Create(ADatabase);
    sgbdSQLServer: FDBQuery := TDBQuerySQLServer.Create(ADatabase);
  end;
end;

destructor TDBQuery.Destroy;
begin
  FDBQuery.Free;

  inherited;
end;

procedure TDBQuery.EndTransaction;
begin
  FDBQuery.EndTransaction;
end;

procedure TDBQuery.Execute(HasReturn: Boolean; VerificarTabela: Boolean);
begin
  FDBQuery.Execute(HasReturn, VerificarTabela);
end;

procedure TDBQuery.ExecuteDirect(SQL: string; Args: array of const);
begin
  FDBQuery.ExecuteDirect(SQL, Args);
end;

procedure TDBQuery.ExecuteDirect(SQL: string);
begin
  FDBQuery.ExecuteDirect(SQL);
end;

function TDBQuery.FieldByName(AFieldName: string): TField;
begin
  Result := FDBQuery.FieldByName(AFieldName);
end;

procedure TDBQuery.First;
begin
  FDBQuery.First;
end;

function TDBQuery.GetAsBoolean(AFieldName: string): Boolean;
begin
  Result := FDBQuery.GetAsBoolean(AFieldName);
end;

function TDBQuery.GetAsDateTime(AFieldName: string): TDateTime;
begin
  Result := FDBQuery.GetAsDateTime(AFieldName);
end;

function TDBQuery.GetAsFloat(AFieldName: string): Real;
begin
  Result := FDBQuery.GetAsFloat(AFieldName);
end;

function TDBQuery.GetAsInt64(AFieldName: string): Int64;
begin
  Result := FDBQuery.GetAsInt64(AFieldName);
end;

function TDBQuery.GetAsInteger(AFieldName: string): Integer;
begin
  Result := FDBQuery.GetAsInteger(AFieldName);
end;

procedure TDBQuery.GetAsStream(AFieldName: string; out AMemoryStream: TMemoryStream);
begin
  FDBQuery.GetAsStream(AFieldName, AMemoryStream);
end;

function TDBQuery.GetAsString(AFieldName: string): string;
begin
  Result := FDBQuery.GetAsString(AFieldName);
end;

function TDBQuery.GetAsTime(AFieldName: string): TTime;
begin
  Result := FDBQuery.GetAsTime(AFieldName);
end;

function TDBQuery.GetColumns: string;
begin
  Result := FDBQuery.GetColumns;
end;

function TDBQuery.GetEof: Boolean;
begin
  Result := FDBQuery.Eof;
end;

function TDBQuery.GetFieldCount: Integer;
begin
  Result := FDBQuery.FieldCount;
end;

function TDBQuery.GetFields: TFields;
begin
  Result := FDBQuery.Fields;
end;

function TDBQuery.GetFilter: string;
begin
  Result := FDBQuery.Filter;
end;

function TDBQuery.GetFiltered: Boolean;
begin
  Result := FDBQuery.Filtered;
end;

function TDBQuery.GetLogSQL: Boolean;
begin
  Result := FDBQuery.LogSQL;
end;

function TDBQuery.GetRaiseException: Boolean;
begin
  Result := FDBQuery.RaiseException;
end;

function TDBQuery.GetRecordCount: Integer;
begin
  Result := FDBQuery.RecordCount;
end;

procedure TDBQuery.InsertIntoClientDataSet(ADataSet: TClientDataSet);
begin
  FDBQuery.InsertIntoClientDataSet(ADataSet);
end;

function TDBQuery.IsNull(AFieldName: string): Boolean;
begin
  Result := FDBQuery.IsNull(AFieldName);
end;

procedure TDBQuery.Last;
begin
  FDBQuery.Last;
end;

function TDBQuery.Locate(const KeyFields: string; const KeyValues: Variant; Options: TLocateOptions): Boolean;
begin
  Result := FDBQuery.Locate(KeyFields, KeyValues, Options);
end;

procedure TDBQuery.Next;
begin
  FDBQuery.Next;
end;

procedure TDBQuery.Prior;
begin
  FDBQuery.Prior;
end;

procedure TDBQuery.SetParam(AParam: string; Value: Real; ForceNull: Boolean = True);
begin
  FDBQuery.SetParam(AParam, Value);
end;

procedure TDBQuery.SetParam(AParam: string; Value: Integer; ForceNull: Boolean = True);
begin
  FDBQuery.SetParam(AParam, Value);
end;

procedure TDBQuery.SetParam(AParam: string; Value: Boolean);
begin
  FDBQuery.SetParam(AParam, Value);
end;

procedure TDBQuery.SetParam(AParam, Value: string; ForceNull: Boolean = True);
begin
  FDBQuery.SetParam(AParam, Value);
end;

procedure TDBQuery.SetFilter(const Value: string);
begin
  FDBQuery.Filter := Value;
end;

procedure TDBQuery.SetFiltered(const Value: Boolean);
begin
  FDBQuery.Filtered := Value;
end;

procedure TDBQuery.SetLogSQL(const Value: Boolean);
begin
  FDBQuery.LogSQL := Value;
end;

procedure TDBQuery.SetParam(AParam: string; Value: TMemoryStream; ForceNull: Boolean = True);
begin
  FDBQuery.SetParam(AParam, Value);
end;

procedure TDBQuery.SetParam(AParam: string; Value: Int64; ForceNull: Boolean = True);
begin
  FDBQuery.SetParam(AParam, Value);
end;

procedure TDBQuery.SetParamDate(AParam: string; Value: TDate; ForceNull: Boolean = True);
begin
  FDBQuery.SetParamDate(AParam, Value);
end;

procedure TDBQuery.SetParamDateTime(AParam: string; Value: TDateTime; ForceNull: Boolean = True);
begin
  FDBQuery.SetParamDateTime(AParam, Value);
end;

procedure TDBQuery.SetParamTime(AParam: string; Value: TTime; ForceNull: Boolean = True);
begin
  FDBQuery.SetParamTime(AParam, Value);
end;

procedure TDBQuery.SetQueryRange(AMinDateTime, AMaxDateTime: TDateTime);
begin
  FDBQuery.MinDateTime := AMinDateTime;
  FDBQuery.MaxDateTime := AMaxDateTime;
end;

procedure TDBQuery.SetRaiseException(const Value: Boolean);
begin
  FDBQuery.RaiseException := Value;
end;

procedure TDBQuery.SetSQL(ASQL: string; Args: array of const);
begin
  FDBQuery.SetSQL(ASQL, Args);
end;

procedure TDBQuery.SetSQL(ASQL: string);
begin
  FDBQuery.SetSQL(ASQL);
end;

end.
