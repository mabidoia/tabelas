unit uCriticalSectionVeltrac;

interface

uses
  SyncObjs, uHelperLog, Windows, Classes, Generics.Collections, uDomains;

const
  //At� quantos milleseconds v�o ser tolerados para n�o ir para o log ?
  MS_LIMIT_LOG_RC = 10;

{$DEFINE DEBUGRC}
type
  TCriticalSectionVeltec = class;

  TThreadCriticalSectionWatcher = class(TThread)
  strict private
    FRCList: TList<TCriticalSectionVeltec>;
    FCriticalSection: TCriticalSection;

    procedure RemoveUnsed;
    procedure LogCriticalSections;
  protected
    procedure Execute; override;
  public
    constructor Create;
    destructor Destroy; override;

    procedure Add(var ACriticalSectionVeltec: TCriticalSectionVeltec);
    procedure Remove(var ACriticalSectionVeltec: TCriticalSectionVeltec);
  end;

  TCriticalSectionVeltec = class(TCriticalSection)
  private
    FReleaseUnsed: Boolean;
    FLastTryGetInTime: Int64;
    FCurrentThreadId: Cardinal;
    FName: string;
    FIsLocked: Boolean;
    FSectionIn: string;
    FIsSectionName: Boolean;
    FTimeIn: Int64;
    FTickCountIn: Cardinal;

    procedure GetTimeToGetIn(AStartTime, AEndTime: Int64);
    procedure GetTimeSpentIn(AStartTime, AEndTime: Int64);
  public
    constructor Create(ARelaseUnsed: Boolean = False); overload;
    constructor Create(AName: string; ARelaseUnsed: Boolean = False); overload;
    destructor Destroy; override;

    procedure Acquire; override;
    procedure NamedAcquire(ASection: string);
    procedure Release; override;
  end;

implementation

uses
  SysUtils, Forms;

var
  SPerformanceFrequency: Int64;
  SThreadCriticalSectionWatcher: TThreadCriticalSectionWatcher;

{ TCriticalSectionVeltec }

procedure TCriticalSectionVeltec.Acquire;
{$IFDEF DEBUGRC}
var
  LTimeToGetInt: Int64;
{$ENDIF}
begin
  {$IFDEF DEBUGRC}
  QueryPerformanceCounter(LTimeToGetInt);
  FLastTryGetInTime := LTimeToGetInt;
  {$ENDIF}
  FSection.Enter;
  {$IFDEF DEBUGRC}
  FIsSectionName := False;
  FIsLocked := True;
  FTickCountIn := GetTickCount;
  FCurrentThreadId := GetCurrentThreadId;
  QueryPerformanceCounter(FTimeIn);
  GetTimeToGetIn(LTimeToGetInt, FTimeIn);
  {$ENDIF}
end;

constructor TCriticalSectionVeltec.Create(AName: string; ARelaseUnsed: Boolean);
begin
  Create(ARelaseUnsed);
  FName := AName;
end;

destructor TCriticalSectionVeltec.Destroy;
begin
  {$IFDEF CLIENTE}
  if Application.Terminated then
    Exit;
  {$ENDIF}
  SThreadCriticalSectionWatcher.Remove(Self);
  inherited;
end;

constructor TCriticalSectionVeltec.Create(ARelaseUnsed: Boolean);
begin
  inherited Create;
  {$IFDEF DEBUGRC}
  FReleaseUnsed := ARelaseUnsed;
  FIsSectionName := False;


  {$IFDEF CLIENTE}
  if Application.Terminated then
    Exit;
  {$ENDIF}

  if SThreadCriticalSectionWatcher = nil then
  begin
    SThreadCriticalSectionWatcher := TThreadCriticalSectionWatcher.Create;
    SThreadCriticalSectionWatcher.Start;
  end;
  SThreadCriticalSectionWatcher.Add(Self);
  {$ENDIF}
end;

procedure TCriticalSectionVeltec.GetTimeSpentIn(AStartTime, AEndTime: Int64);
var
  LEllapsed: Extended;
begin
  LEllapsed := (MSecsPerSec * (AEndTime - AStartTime)) / SPerformanceFrequency;
  if LEllapsed < MS_LIMIT_LOG_RC then
    Exit;

  if FIsSectionName then
    Logger.Debug('TThreadCriticalSectionWatcher.GetTimeSpentIn[%s | %s]: %2.3f ms', [FName, FSectionIn, LEllapsed])
  else
    Logger.Debug('TThreadCriticalSectionWatcher.GetTimeSpentIn[%s]: %2.3f ms', [FName, LEllapsed]);
end;

procedure TCriticalSectionVeltec.GetTimeToGetIn(AStartTime, AEndTime: Int64);
var
  LEllapsed: Extended;
begin
  LEllapsed := (MSecsPerSec * (AEndTime - AStartTime)) / SPerformanceFrequency;
  if LEllapsed < MS_LIMIT_LOG_RC then
    Exit;

  if FIsSectionName then
    Logger.Debug('TThreadCriticalSectionWatcher.GetTimeToGetIn[%s | %s]: %2.3f ms', [FName, FSectionIn, LEllapsed])
  else
    Logger.Debug('TThreadCriticalSectionWatcher.GetTimeToGetIn[%s]: %2.3f ms', [FName, LEllapsed]);
end;

procedure TCriticalSectionVeltec.NamedAcquire(ASection: string);
{$IFDEF DEBUGRC}
var
  LTimeToGetInt: Int64;
{$ENDIF}
begin
  {$IFDEF DEBUGRC}
  QueryPerformanceCounter(LTimeToGetInt);
  FLastTryGetInTime := LTimeToGetInt;
  {$ENDIF}
  FSection.Enter;
  {$IFDEF DEBUGRC}
  FSectionIn := ASection;
  FIsLocked := True;
  FIsSectionName := True;
  FTickCountIn := GetTickCount;
  FCurrentThreadId := GetCurrentThreadId;
  QueryPerformanceCounter(FTimeIn);
  GetTimeToGetIn(LTimeToGetInt, FTimeIn);
  {$ENDIF}
end;

procedure TCriticalSectionVeltec.Release;
{$IFDEF DEBUGRC}
var
  LTimeGetOut: Int64;
{$ENDIF}
begin
  {$IFDEF DEBUGRC}
  QueryPerformanceCounter(LTimeGetOut);
  GetTimeSpentIn(FTimeIn, LTimeGetOut);
  FIsLocked := False;
  {$ENDIF}
  FSection.Leave;
end;

{ TThreadCriticalSectionWatcher }

procedure TThreadCriticalSectionWatcher.Add(var ACriticalSectionVeltec: TCriticalSectionVeltec);
begin
  FCriticalSection.Acquire;
  try
    FRCList.Add(ACriticalSectionVeltec)
  finally
    FCriticalSection.Release;
  end;
end;

constructor TThreadCriticalSectionWatcher.Create;
begin
  inherited Create(True);
  FRCList := TList<TCriticalSectionVeltec>.Create;
  FCriticalSection := TCriticalSection.Create;
//  FreeOnTerminate := True;
end;

destructor TThreadCriticalSectionWatcher.Destroy;
begin
  FCriticalSection.Free;
  FRCList.Free;
  inherited;
end;

procedure TThreadCriticalSectionWatcher.Execute;
begin
  while not Terminated do
  begin
    Sleep(1000);
    try
      if not Terminated then
        LogCriticalSections;
    except
      on E: Exception do
      begin
        if Assigned(Logger) then
          Logger.Error('TThreadCriticalSectionWatcher.Execute', Self, E);
      end;
    end;
  end;
end;

procedure TThreadCriticalSectionWatcher.LogCriticalSections;
var
  LTickCount: Cardinal;
  LCriticalSection: TCriticalSectionVeltec;
  LSpentTime: Cardinal;
begin
  if (Logger = nil) or (FRCList.Count = 0) then
    Exit;

  FCriticalSection.Acquire;
  try
    LTickCount := GetTickCount;
    for LCriticalSection in FRCList do
    begin
      if Logger = nil then
        Exit;

      if not (LCriticalSection.FIsLocked) then
        Continue;

      LSpentTime := LTickCount - LCriticalSection.FTickCountIn;
      if (LSpentTime > 30000) then
      begin
        if LCriticalSection.FIsSectionName then
          Logger.Error('TThreadCriticalSectionWatcher[ThreadId: %d | RC: %s | Section: %s]: Release for�ado para evitar deadlock. [%d ms]',
            [LCriticalSection.FCurrentThreadId, LCriticalSection.FName, LCriticalSection.FSectionIn, LSpentTime])
        else
          Logger.Error('TThreadCriticalSectionWatcher[ThreadId: %d | RC: %s]: Release for�ado para evitar deadlock. [%d ms]',
            [LCriticalSection.FCurrentThreadId, LCriticalSection.FName, LSpentTime]);

        LCriticalSection.Release;
      end
      else if (LSpentTime > 1000) then
      begin
        if LCriticalSection.FIsSectionName then
          Logger.Warn('TThreadCriticalSectionWatcher[ThreadId: %d | RC: %s | Section: %s]: Locked by [%d ms]', [LCriticalSection.FCurrentThreadId, LCriticalSection.FName, LCriticalSection.FSectionIn, LSpentTime])
        else
          Logger.Warn('TThreadCriticalSectionWatcher[ThreadId: %d | RC: %s]: Locked by [%d ms]', [LCriticalSection.FCurrentThreadId, LCriticalSection.FName, LSpentTime])
      end;

    end;
  finally
    FCriticalSection.Release;
  end;
end;

procedure TThreadCriticalSectionWatcher.Remove(var ACriticalSectionVeltec: TCriticalSectionVeltec);
begin
  FCriticalSection.Acquire;
  try
    FRCList.Remove(ACriticalSectionVeltec);
  finally
    FCriticalSection.Release;
  end;
end;

procedure TThreadCriticalSectionWatcher.RemoveUnsed;
var
  LTickCount: Cardinal;
  LCriticalSection: TCriticalSectionVeltec;
  LSpentTime: Cardinal;
begin
  if (Logger = nil) or (FRCList.Count = 0) then
    Exit;

  FCriticalSection.Acquire;
  try
    LTickCount := GetTickCount;
    for LCriticalSection in FRCList do
    begin
      if not LCriticalSection.FReleaseUnsed then
        Continue;

      if not (LCriticalSection.FIsLocked) then
        Continue;

      LSpentTime := LTickCount - LCriticalSection.FTickCountIn;
      if (LSpentTime > 30000) then
        LCriticalSection.Free;
    end;
  finally
    FCriticalSection.Release;
  end;
end;

initialization
  {$IFDEF DEBUGRC}
  QueryPerformanceFrequency(SPerformanceFrequency);
  {$ENDIF}

finalization
  SThreadCriticalSectionWatcher.Terminate;
  SThreadCriticalSectionWatcher := nil;

end.
