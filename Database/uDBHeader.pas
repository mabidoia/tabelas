﻿unit uDBHeader;

interface

uses
  SysUtils;

const
  HALT_OK = 0;
  HALT_ERROR = 1;

type
  // SGBDs suportados pelo sistema
  TSGBDs = (
    sgbdNone,
    sgbdFirebird,
    sgbdSQLServer,
    sgbdPostgreSQL
  );

  // banco de dados utilizados pelo veltrac
  TDatabases = (
    dbPrincipal,
    dbIntegracao,
    dbPrincipalGabarito,
    dbIntegracaoGabarito,
    dbMaster,
    dbIntegracaoTSR
  );

  TConstraintType = (
    ctPrimaryKey,
    ctForeignKey,
    ctUnique
  );

  TDBSettings = record
    SGBDType: TSGBDs;
    LocaleFormatSettings: TFormatSettings;
  end;

var
  DBSettings: TDBSettings;
  strConstraintFirebirdType: array[TConstraintType] of string;
  strConstraintSqlServerType: array[TConstraintType] of string;
  strConstraintPostgreSQLType: array[TConstraintType] of string;
  strTSGBDs: array[TSGBDs] of string;
  strTDatabases: array[TDatabases] of string;

implementation

procedure SetLocaleFormatForDatabase;
  procedure SetArrays(var Source: array of string; var Destination: array of string);
  var
    i: Integer;
  begin
    { TODO -oUlder -cMELHORIA : Verificar a possibilidade de implementar High(Source) }
    for i := Low(Source) to High(Destination) do
      Destination[i] := Source[i];
  end;
begin
  // Obtêm as Configurações Regionais do Brasil
  GetLocaleFormatSettings($0416, DBSettings.LocaleFormatSettings);
  // Não funciona, porque se o usuário alterar o padrão portugues no sistema,
  // vai dar erro na hora de converter as datas e horas do mesmo jeito
  // Então, as configurações cruciais para o sistema são setadas da forma que o sistema entende 

  // seta as variáveis Globais de Configurações Regionais
//  DBSettings.LocaleFormatSettings.CurrencyFormat    := '';
//  DBSettings.LocaleFormatSettings.NegCurrFormat     := '';
//  DBSettings.LocaleFormatSettings.ThousandSeparator := '';
  DBSettings.LocaleFormatSettings.DecimalSeparator  := '.';
//  DBSettings.LocaleFormatSettings.CurrencyDecimals  := '';
  DBSettings.LocaleFormatSettings.DateSeparator     := '-';
  DBSettings.LocaleFormatSettings.TimeSeparator     := ':';
//  DBSettings.LocaleFormatSettings.ListSeparator     := '';
//  DBSettings.LocaleFormatSettings.CurrencyString    := '';
  DBSettings.LocaleFormatSettings.ShortDateFormat := 'yyyy-MM-dd';
  DBSettings.LocaleFormatSettings.LongDateFormat := 'yyyy-MM-dd';
  DBSettings.LocaleFormatSettings.ShortTimeFormat := 'hh:nn';
  DBSettings.LocaleFormatSettings.LongTimeFormat := 'hh:nn:ss';
//  DBSettings.LocaleFormatSettings.LongDateFormat    := '';
//  DBSettings.LocaleFormatSettings.TimeAMString      := '';
//  DBSettings.LocaleFormatSettings.TimePMString      := '';
//  DBSettings.LocaleFormatSettings.LongTimeFormat    := '';

  SetArrays(DBSettings.LocaleFormatSettings.ShortMonthNames, ShortMonthNames);
  SetArrays(DBSettings.LocaleFormatSettings.LongMonthNames, LongMonthNames);
  SetArrays(DBSettings.LocaleFormatSettings.ShortDayNames, ShortDayNames);
  SetArrays(DBSettings.LocaleFormatSettings.LongDayNames, LongDayNames);
end;

initialization
  SetLocaleFormatForDatabase;

  // define o tipo da constraint
  strConstraintFirebirdType[ctPrimaryKey] := 'PRIMARY KEY';
  strConstraintFirebirdType[ctForeignKey] := 'FOREIGN KEY';
  strConstraintFirebirdType[ctUnique] := 'UNIQUE';

  strConstraintSqlServerType[ctPrimaryKey] := 'PK';
  strConstraintSqlServerType[ctForeignKey] := 'F';
  strConstraintSqlServerType[ctUnique] := 'UQ';

  strConstraintPostgreSQLType[ctPrimaryKey] := 'PRIMARY KEY';
  strConstraintPostgreSQLType[ctForeignKey] := 'FOREIGN KEY';
  strConstraintPostgreSQLType[ctUnique] := 'UNIQUE';

  strTSGBDs[sgbdNone]      := 'None';
  strTSGBDs[sgbdFirebird]  := 'Firebird';
  strTSGBDs[sgbdSQLServer] := 'SQLServer';

  strTDatabases[dbPrincipal] := 'Principal';
  strTDatabases[dbIntegracao] := 'Integracao';
  strTDatabases[dbPrincipalGabarito] := 'Principal Gabarito';
  strTDatabases[dbIntegracaoGabarito] := 'Integracao Gabarito';

end.
