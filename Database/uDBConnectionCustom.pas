unit uDBConnectionCustom;

interface

uses
  uDBHeader, uHelperLog, SysUtils, Windows, uDBPoolConnection;

const
  // o valor da constante est� como duplica para atender o nome em portugu�s(duplicada) e em ingl�s(duplicate)
  CHAVE_DUPLICADA = 'duplica';
  UNIQUE_KEY = 'UNIQUE KEY';
  FOREIGN_KEY = 'FOREIGN';

type
  // classe base da conex�o com o banco de dados
  TDBConnectionCustom = class
  private
  protected
    FThreadDBConnection: TThreadDBConnection;
    FTransactionId: Int64;
    // flags
    FFailSQL: Boolean;
    FRaiseException: Boolean;
    FLogSQL: Boolean;
    // conex�o com o banco de dados
    FDatabase: TDatabases;
    FRowsAffected: Integer;
    class var FDatabasePrincipal: string;
    class var FDatabaseIntegracao: string;
    class var FDatabasePrincipalGabarito: string;
    class var FDatabaseIntegracaoGabarito: string;
    class var FDatabaseIntegracaoTSR: string;    
    class var FUserName: string;
    class var FPassword: string;
    class var FCType: string;
    class var FIntegracaoSaidaBanco: Boolean;
    class var FIntegracaoSaidaArquivo: Boolean;

    procedure GenerateTransactionKey;
  public
    // controle da classe
    constructor Create(ADatabase: TDatabases = dbPrincipal); virtual; abstract;
    // controle de conex�o
    class procedure LoadSettings;
    // atualiza��o do usu�rio e senha utilizados no sistema
    class procedure UpdateSettingsUserPassword;

    { TODO -oUlder -cMELHORIA : Remover estes m�todos. }
    class function GetDatabasePrincipal: string;
    class function GetDatabaseIntegracao: string;
    class function GetDatabasePrincipalGabarito: string;
    class function GetDatabaseIntegracaoGabarito: string;
    class function GetDatabaseIntegracaoTSR: string;
    class function GetDatabaseUsername: string;
    class function GetDatabasePassword: string;

    function IsTransactionActive: boolean; virtual; abstract;
    function IsConnected: Boolean; virtual; abstract;

    procedure Reconnect; virtual; abstract;
    // controle de transa��es
    procedure BeginTransaction; virtual; abstract;
    procedure EndTransaction; virtual; abstract;
    // execu��es dos SQLs
    procedure ResetQuery; virtual; abstract;
    procedure OpenQuery; virtual; abstract; // select
    procedure ExecSQL; virtual; abstract;   // insert, update, delete, create, drop, alter
    // properties
    property RaiseException: Boolean read FRaiseException write FRaiseException;
    property LogSQL: Boolean read FLogSQL write FLogSQL;
    property FailSQL: Boolean read FFailSQL;
    property TransactionId: Int64 read FTransactionId;
    property RowsAffected: Integer read FRowsAffected;
  end;

implementation

uses
  IniFiles, {$IFDEF SERVIDOR} uInicializacao, uIntegracao, {$ENDIF SERVIDOR} uUtils, uDBQuery;

{ TDBConnectionCustom }

procedure TDBConnectionCustom.GenerateTransactionKey;
begin
  QueryPerformanceCounter(FTransactionId);
end;

class function TDBConnectionCustom.GetDatabaseIntegracao: string;
begin
  Result := FDatabaseIntegracao;
end;

class function TDBConnectionCustom.GetDatabaseIntegracaoGabarito: string;
begin
  Result := FDatabaseIntegracaoGabarito;
end;

class function TDBConnectionCustom.GetDatabaseIntegracaoTSR: string;
begin
  Result := FDatabaseIntegracaoTSR;
end;

class function TDBConnectionCustom.GetDatabasePassword: string;
begin
  Result := FPassword;
end;

class function TDBConnectionCustom.GetDatabasePrincipal: string;
begin
  Result := FDatabasePrincipal;
end;

class function TDBConnectionCustom.GetDatabasePrincipalGabarito: string;
begin
  Result := FDatabasePrincipalGabarito;
end;

class function TDBConnectionCustom.GetDatabaseUsername: string;
begin
  Result := FUserName;
end;

class procedure TDBConnectionCustom.LoadSettings;
var
  Ini: TIniFile;
  LCaminhoBancoIntegracaoTSR: String;
begin
  Ini := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'config.ini');
  try

    FDatabasePrincipal := Ini.ReadString('DATABASE', 'DatabasePrincipal', '');

    FDatabaseIntegracao := Ini.ReadString('DATABASE', 'DatabaseIntegracao', '');
    FDatabaseIntegracaoTSR := Ini.ReadString('DATABASE', 'DatabaseIntegracaoTSR', '');

    //adiciona a se��o "DatabaseIntegracaoTSR" caso a permiss�o de IntegracaoTSR esteja habilitada
    if Ini.ReadBool('INTEGRACAO_TERMINAL_SERVICOS', 'ATIVO', False) then
    begin
      LCaminhoBancoIntegracaoTSR := FDatabasePrincipal;
      LCaminhoBancoIntegracaoTSR := StringReplace(FDatabasePrincipal,'BD_CLIENTE','BD_INTEGRACAO_TSR', [rfReplaceAll, rfIgnoreCase]);
      FDatabaseIntegracaoTSR := LCaminhoBancoIntegracaoTSR;
      if (DBSettings.SGBDType = sgbdPostgreSQL) then
        FDatabaseIntegracaoTSR := LowerCase(FDatabaseIntegracaoTSR);
      Ini.WriteString('DATABASE', 'DatabaseIntegracaoTSR', FDatabaseIntegracaoTSR);
    end;

    FUserName := Ini.ReadString('DATABASE', 'UserName', DB_USER_SYSDBA);
    FPassword := Ini.ReadString('DATABASE', 'Password', DB_PASS_SYSDBA);
    { TODO -oUlder : O Encode afeta a cria��o do banco de dados NOVO. Verificar de converter o encode dos BD j� existentes.
      Este encode foi utilizado para permitir que o Sin�tica identifique os SQL que est�o sendo executados. }
    FCType := Ini.ReadString('DATABASE', 'CType', '');  // ISO8859_1

    {$IFDEF NEWDB}
    FDatabasePrincipal := ExtractFilePath(ParamStr(0)) + 'DB_CLIENTE' + StringReplace(VERSION_STR, '.','',[rfReplaceAll]) + '.NDB';
    {$ENDIF}
  finally
    Ini.UpdateFile;
    Ini.Free;
  end;
end;

class procedure TDBConnectionCustom.UpdateSettingsUserPassword;
begin
  // atualiza o usu�rio corrente, utilizado pelo sistema
  FUserName := DB_USER_VELTRAC;
  FPassword := DB_PASS_VELTRAC;
end;

end.


