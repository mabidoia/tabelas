﻿unit uDBCustom;

interface

uses
  Classes, Controls, Rtti, uMappedFields;

type
  TDBCustom = class abstract
  protected
    function GetLogSQL: Boolean; virtual; abstract;
    function GetRaiseException: Boolean; virtual; abstract;
    procedure SetLogSQL(const Value: Boolean); virtual; abstract;
    procedure SetRaiseException(const Value: Boolean); virtual; abstract;
    function GetTransactionId: Int64; virtual; abstract;
  public
    // controle de transações
    procedure BeginTransaction; virtual; abstract;
    procedure EndTransaction; virtual; abstract;
    // processamento do sql
    procedure Execute(HasReturn: Boolean = False; VerificarTabela: Boolean = True); virtual; abstract;
    procedure ExecuteDirect(SQL: string); overload; virtual; abstract;
    procedure ExecuteDirect(SQL: string; Args: array of const); overload; virtual; abstract;
    // controle de input
    procedure SetSQL(ASQL: string); overload; virtual; abstract;
    procedure SetSQL(ASQL: string; Args: array of const); overload; virtual; abstract;
    procedure SetParam(AParam: string; Value: Boolean); overload; virtual; abstract;
    procedure SetParam(AParam: string; Value: Integer; ForceNull: Boolean = True); overload; virtual; abstract;
    procedure SetParam(AParam: string; Value: Int64; ForceNull: Boolean = True); overload; virtual; abstract;
    procedure SetParam(AParam: string; Value: Real; ForceNull: Boolean = True); overload; virtual; abstract;
    procedure SetParam(AParam: string; Value: string; ForceNull: Boolean = True); overload; virtual; abstract;
    procedure SetParam(AParam: string; Value: TMemoryStream; ForceNull: Boolean = True); overload; virtual; abstract;
    procedure SetParamTime(AParam: string; Value: TTime; ForceNull: Boolean = True); virtual; abstract;
    procedure SetParamDate(AParam: string; Value: TDate; ForceNull: Boolean = True); virtual; abstract;
    procedure SetParamDateTime(AParam: string; Value: TDateTime; ForceNull: Boolean = True); virtual; abstract;

    procedure SetParam(AParam: string; ARttiField: TMappedRttiField; AMVCItemObject: TObject); overload;
    // properties
    property RaiseException: Boolean read GetRaiseException write SetRaiseException;
    property LogSQL: Boolean read GetLogSQL write SetLogSQL;
    property TransactionId: Int64 read GetTransactionId;
  end;

implementation

uses
  uHelperLog, TypInfo, SysUtils;

{ TDBCustom }

procedure TDBCustom.SetParam(AParam: string; ARttiField: TMappedRttiField; AMVCItemObject: TObject);
var
  Field: TRttiField;
  Memory: TStringStream;
begin
  Field := ARttiField.Field;
  if ARttiField.IsBlobField then
  begin
    Memory := TStringStream.Create(Field.GetValue(AMVCItemObject).AsString);
    SetParam(AParam, Memory);
    Memory.Free;
    Exit;
  end;

  if Field.FieldType = nil then
    SetParam(AParam, Field.GetValue(AMVCItemObject).AsInteger)
  else
  begin
    case Field.FieldType.TypeKind of
      tkInteger:
        SetParam(AParam, Field.GetValue(AMVCItemObject).AsInteger);
      tkInt64:
        SetParam(AParam, Field.GetValue(AMVCItemObject).AsInt64);
      tkEnumeration:
        SetParam(AParam, Field.GetValue(AMVCItemObject).AsOrdinal);
      tkFloat:
        begin
          if Field.FieldType.Name = 'TTime' then
            SetParamTime(AParam,FloatToDateTime(Field.GetValue(AMVCItemObject).AsExtended))
          else if (Field.FieldType.Name = 'TDate') then
            SetParamDate(AParam, FloatToDateTime(Field.GetValue(AMVCItemObject).AsExtended))
          else if (Field.FieldType.Name = 'TDateTime') then
            SetParamDateTime(AParam, FloatToDateTime(Field.GetValue(AMVCItemObject).AsExtended))
          else
            SetParam(AParam, Field.GetValue(AMVCItemObject).AsExtended);
        end;
      tkUString, tkDynArray, tkWString, tkString, tkWChar, tkChar, tkLString:
        SetParam(AParam, Field.GetValue(AMVCItemObject).AsString);
    else
      Logger.Error('TDBCustom.SetParam: Unsupported attribute type [%s]', [AMVCItemObject.ClassName, Field.FieldType.QualifiedName], Self, nil);
    end;
  end;

end;
end.
