unit FieldByNameSpeedUp;

{
  *****************************************************************************
  *                                                                           *
  *                        TDataset.FieldByName Speedup                       *
  *                                                                           *
  *                            By Jens Borrisholt                             *
  *                            Jens@Borrisholt.dk                             *
  *                                                                           *
  * FieldByNameHack are distributed and/or modified under the terms of        *
  * the GNU General Public License (GPL) version 2 as published by the        *
  * Free Software Foundation.                                                 *
  *                                                                           *
  *                                                                           *
  * This file has no warranty and is used at the users own peril              *
  *                                                                           *
  * Please report any bugs to Jens@Borrisholt.dk  or contact me if you want   *
  * to contribute to this unit.  It will be deemed a breach of copyright if   *
  * you publish any source code  (modified or not) herein under your own name *
  * without the authors consent!!!!!                                          *
  *                                                                           *
  * CONTRIBUTIONS:-                                                           *
  *      Jens Borrisholt (Jens@Borrisholt.dk) [ORIGINAL AUTHOR]               *
  *                                                                           *
  *****************************************************************************
}


interface

uses
  DB;


{.$DEFINE HookDataset_ON_Initialization}
procedure HookTDataset;
procedure UnHookTDataset;

type
  iFieldList = interface
    ['{C62FA3F5-532E-4B05-8B39-48C1E7C40C5B}']
    procedure Initialize(const aDataset: TDataSet);
    function FindField(const FieldName: string): TField;
    function FieldByName(const S: string): TField;
  end;

function GetFieldList: iFieldList; overload;
function GetFieldList(const aDataset: TDataSet): iFieldList; overload;


implementation

uses
  Windows, Sysutils, Classes, DBConsts, Contnrs;

{**************************************************************************************************}
{                                         BEGIN VCL HOOK                                           }
{                                                                                                  }
{ The Initial Developer of the Original Code is Andreas Hausladen (Andreas.Hausladen@gmx.de).      }
{ Portions created by Andreas Hausladen are Copyright (C) 2008-2009 Andreas Hausladen.             }
{ All Rights Reserved.                                                                             }
{                                                                                                  }
{**************************************************************************************************}


type
  TJumpOfs = Integer;
  PPointer = ^Pointer;

  PXRedirCode = ^TXRedirCode;
  TXRedirCode = packed record
    Jump: Byte;
    Offset: TJumpOfs;
  end;

  pWin9xDebugThunk = ^TWin9xDebugThunk;
  TWin9xDebugThunk = packed record
    PUSH: Byte;
    Addr: Pointer;
    JMP: TXRedirCode;
  end;

  PAbsoluteIndirectJmp = ^TAbsoluteIndirectJmp;
  TAbsoluteIndirectJmp = packed record
    OpCode: Word;
    Addr: PPointer;
  end;

function GetRealProcAddr(Proc: Pointer): Pointer;

  function IsWin9xDebugThunk(AAddr: Pointer): Boolean;
  begin
    Result := (AAddr <> nil) and
              (pWin9xDebugThunk(AAddr).PUSH = $68) and
              (pWin9xDebugThunk(AAddr).JMP.Jump = $E9);
  end;

begin
  if Proc <> nil then
  begin
    if (Win32Platform <> VER_PLATFORM_WIN32_NT) and IsWin9xDebugThunk(Proc) then
      Proc := pWin9xDebugThunk(Proc).Addr;

    if (PAbsoluteIndirectJmp(Proc).OpCode = $25FF) then
      Result := PAbsoluteIndirectJmp(Proc).Addr^
    else
      Result := Proc;
  end
  else
    Result := nil;
end;

procedure HookProc(Proc, Dest: Pointer; var BackupCode: TXRedirCode);
var
  n: {$IFDEF WIN32}Cardinal {$ELSE} NativeUInt {$ENDIF};
  Code: TXRedirCode;
  LNativeU: {$IFDEF WIN32}Cardinal {$ELSE} NativeUInt {$ENDIF};
begin
  Proc := GetRealProcAddr(Proc);

  if Proc = nil then
    Exit;

  LNativeU := SizeOf(BackupCode);
  if ReadProcessMemory(GetCurrentProcess, Proc, @BackupCode, LNativeU, n) then
  begin
    Code.Jump := $E9;
    Code.Offset := PAnsiChar(Dest) - PAnsiChar(Proc) - SizeOf(Code);
    WriteProcessMemory(GetCurrentProcess, Proc, @Code, SizeOf(Code), n);
  end;
end;

procedure UnhookProc(Proc: Pointer; var BackupCode: TXRedirCode);
var
  n: {$IFDEF WIN32}Cardinal {$ELSE} NativeUInt {$ENDIF};
begin
  if (BackupCode.Jump <> 0) and (Proc <> nil) then
  begin
    Proc := GetRealProcAddr(Proc);
    Assert(Proc <> nil);
    WriteProcessMemory(GetCurrentProcess, Proc, @BackupCode, SizeOf(BackupCode), n);
    BackupCode.Jump := 0;
  end;
end;

{**************************************************************************************************}
{                                          END VCL HOOK                                            }
{**************************************************************************************************}


type
  TMyStringList = class(TStringList)
  protected
    function CompareStrings(const S1, S2: string): Integer; override;
  end;

  TFieldList = class(TInterfacedObject, iFieldList)
  private
    FDataset : TDataset;
  protected
    InternalList: TMyStringList;
  public
    procedure Initialize(const aDataset: TDataSet);
    function FieldByName(const FieldName: string): TField;
    function FindField(const FieldName: string): TField;
    constructor Create; overload;
    constructor Create(const aDataset: TDataSet); reintroduce; overload;
    destructor Destroy; override;
  end;

constructor TFieldList.Create(const aDataset: TDataSet);
begin
  inherited Create;
  InternalList := TMyStringList.Create;
  FDataset := aDataset;
  Initialize(FDataset);
end;

destructor TFieldList.Destroy;
begin
  FreeAndNil(InternalList);
  inherited;
end;

constructor TFieldList.Create;
begin
  InternalList := TMyStringList.Create;
end;

function TFieldList.FieldByName(const FieldName: string): TField;
begin
  Result := FindField(FieldName);
  if Result = nil then
    DatabaseErrorFmt(SFieldNotFound, [FieldName], FDataSet);
end;

function TFieldList.FindField(const FieldName: string): TField;
var
  Index: Integer;
begin
  Index := InternalList.IndexOf(FieldName);
  if Index >= 0 then
    Result := TField(InternalList.Objects[Index])
  else
    Result := nil;
end;

procedure TFieldList.Initialize(const aDataset: TDataSet);
var
  aField: TField;
  i : Integer;
begin
  InternalList.Clear;

  for I := 0 to aDataset.FieldCount - 1 do
  begin
    aField := aDataset.Fields[i];
    InternalList.AddObject(aField.FieldName, aField);
  end;

  InternalList.Sorted := True;
end;

{ TMyStringList }

function TMyStringList.CompareStrings(const S1, S2: string): Integer;
begin
  Result := CompareText(S1, S2);
end;

Type
  TFastDataset =  class;

  TDatasetObject = class
  private
    FieldList : TFieldList;
    FDataset: TFastDataset;
  public
    constructor Create(const aDataset : TFastDataset);
    destructor Destroy; override;
    property Dataset : TFastDataset read FDataset;
  end;

  TDatasetObjectList = class(TObjectList)
  private
    function GetItem(Index: Integer): TDatasetObject;
    procedure SetItem(Index: Integer; const Value: TDatasetObject);
  public
    function Add(aDataset : TFastDataset): Integer;
    function Find(const aDataset : TFastDataset): TDatasetObject;
    function IndexOf(aDataset: TFastDataset): Integer;
    procedure Sort;
    property Items[Index: Integer]: TDatasetObject read GetItem write SetItem; default;
  end;

  TFastDataset = class(TDataSet)
  protected
    procedure NewDoAfterOpen; virtual;
    procedure NewDoAfterClose; virtual;
  public
    function NewFindField(const FieldName: WideString): TField;
  end;

var
  DatasetObjectList    : TDatasetObjectList = nil;
  DatasetHooked        : Boolean = False;
  TDatasetDoAfterOpen  : TXRedirCode;
  TDatasetDoAfterClose : TXRedirCode;
  TDatasetFindField    : TXRedirCode;

procedure HookTDataset;

begin
  if DatasetHooked then
    exit; // TDataset allready hooked;

  DatasetHooked := true;
  DatasetObjectList :=  TDatasetObjectList.Create(True);

  HookProc(@TFastDataset.DoAfterOpen  , @TFastDataset.NewDoAfterOpen , TDatasetDoAfterOpen);
  HookProc(@TFastDataset.DoAfterClose , @TFastDataset.NewDoAfterClose, TDatasetDoAfterClose);
  HookProc(@TDataset.FindField        , @TFastDataset.NewFindField   , TDatasetFindField);
end;

procedure UnHookTDataset;
begin
  if not DatasetHooked  then
    exit; // TDataset not hooked;

  DatasetHooked := False;

  UnhookProc( @TFastDataset.NewDoAfterOpen, TDatasetDoAfterOpen );
  UnhookProc( @TFastDataset.NewDoAfterClose, TDatasetDoAfterClose );
  UnhookProc( @TFastDataset.NewFindField, TDatasetFindField );
  FreeAndNil(DatasetObjectList);
end;

{ TFastDataset }

procedure TFastDataset.NewDoAfterClose;
begin
  DatasetObjectList.Remove(Self);

  if Assigned(AfterClose) then
    AfterClose(Self);
end;

procedure TFastDataset.NewDoAfterOpen;
begin
  DatasetObjectList.Add(Self);

  if Assigned(AfterOpen) then
    AfterOpen(Self);

  if not IsEmpty then
    DoAfterScroll;
end;

function TFastDataset.NewFindField(const FieldName: WideString): TField;
var
  DatasetObject : TDatasetObject;
begin
  DatasetObject := DatasetObjectList.Find(Self);
  Result := DatasetObject.FieldList.FindField(FieldName);
end;

{ TDatasetObject }

constructor TDatasetObject.Create(const aDataset: TFastDataset);
begin
  inherited Create;
  FDataset := aDataset;
  FieldList := TFieldList.Create(aDataset);
end;

destructor TDatasetObject.Destroy;
begin
  FreeAndNil(FieldList);
  inherited;
end;

{ TDatasetObjectList }

function TDatasetObjectList.Add(aDataset : TFastDataset): Integer;
begin
  Result := inherited Add(TDatasetObject.Create(aDataset));
end;

function CompareInt(Item1, Item2: Integer): Integer;
{In  : EAX = Item1, EDX = Item2}
{Out : EAX = Result}
asm
  XOR ECX, ECX
  CMP EAX, EDX
  JL  @Less
  JG  @Greater
  JE  @DONE
@Less:
  DEC ECX
  JMP @Done
@Greater:
  INC ECX
@Done:
  MOV EAX, ECX
end;

function TDatasetObjectList.Find(const aDataset: TFastDataset): TDatasetObject;
var
  Index : Integer;
begin
  Result := nil;
  Index := IndexOf(aDataset);
  if Index >= 0 then
    Result := Items[Index];
end;

function TDatasetObjectList.GetItem(Index: Integer): TDatasetObject;
begin
  Result := TDatasetObject(inherited GetItem(Index));
end;

function TDatasetObjectList.IndexOf(aDataset: TFastDataset): Integer;
var
  i : Integer;
begin
  Result := -1;

  for i := 0 to Count - 1 do
    if GetItem(i).Dataset = aDataset then
    begin
      Result := i;
      exit;
    end;
end;

procedure TDatasetObjectList.SetItem(Index: Integer;const Value: TDatasetObject);
begin
  inherited SetItem(Index, Value);
end;

procedure TDatasetObjectList.Sort;
begin
  inherited Sort(@CompareInt);
end;

function GetFieldList: iFieldList; overload;
begin
  Result := TFieldList.Create;
end;

function GetFieldList(const aDataset: TDataSet): iFieldList; overload;
begin
  Result := TFieldList.Create(aDataset);
end;

initialization
{$IFDEF HookDataset_ON_Initialization}
  HookTDataset;
{$ENDIF}
finalization
  UnHookTDataset;
end.


