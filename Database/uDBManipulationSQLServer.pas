﻿unit uDBManipulationSQLServer;

interface

uses
  uDBManipulationCustom, Classes, Controls, uDBConnectionCustom, uDBConnectionSQLServer,
  uDBHeader, Uni;

type
  TDBManipulationSQLServer = class(TDBManipulationCustom)
  private
    FDBConnection: TDBConnectionSQLServer;
    procedure ReplaceParam(AParam, Value: string);
  protected
    function GetFailSQL: Boolean; override;
    function GetLogSQL: Boolean; override;
    function GetRaiseException: Boolean; override;
    procedure SetLogSQL(const Value: Boolean); override;
    procedure SetRaiseException(const Value: Boolean); override;
    function GetTransactionId: Int64; override;
    function GetRowsAffected: Integer; override;
  public
    // controle da classe
    constructor Create(ADatabase: TDatabases);
    destructor Destroy; override;
    // controle de transações
    procedure BeginTransaction; override;
    procedure EndTransaction; override;
    // controle de conexão
    procedure Reconnect; override;
    // processamento do sql
    procedure Execute(HasReturn: Boolean = False; VerificarTabela: Boolean = True); override;
    procedure ExecuteBlock(SQL: string); override;
    procedure ExecuteDirect(SQL: string); override;
    procedure ExecuteDirect(SQL: string; Args: array of const); override;
    // controle de input
    procedure SetSQL(ASQL: string); overload; override;
    procedure SetSQL(ASQL: string; Args: array of const); overload; override;
    procedure SetSQLInsert(ASQL, AReturnField: string); overload; override;
    procedure SetParam(AParam: string; Value: Boolean); override;
    procedure SetParam(AParam: string; Value: Integer; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: Int64; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: Real; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: string; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: TMemoryStream; ForceNull: Boolean = True); override;
    procedure SetParamTime(AParam: string; Value: TTime; ForceNull: Boolean = True); override;
    procedure SetParamDate(AParam: string; Value: TDate; ForceNull: Boolean = True); override;
    procedure SetParamDateTime(AParam: string; Value: TDateTime; ForceNull: Boolean = True); override;
    // controle de output (returning)
    function GetAsInteger(AFieldName: string): Integer; override;
  end;

implementation

uses
  uDBQuery, SysUtils, DB, SqlTimSt, uHelperLog, IBSQL;

{ TDBManipulationSQLServer }

procedure TDBManipulationSQLServer.BeginTransaction;
begin
  FDBConnection.BeginTransaction;
end;

constructor TDBManipulationSQLServer.Create(ADatabase: TDatabases);
begin
  FDBConnection := TDBConnectionSQLServer.Create(ADatabase);
end;

destructor TDBManipulationSQLServer.Destroy;
begin
  FDBConnection.Free;

  inherited;
end;

procedure TDBManipulationSQLServer.EndTransaction;
begin
  FDBConnection.EndTransaction;
end;

procedure TDBManipulationSQLServer.Execute(HasReturn: Boolean; VerificarTabela: Boolean);
begin
  if HasReturn then
    FDBConnection.OpenQuery
  else
    FDBConnection.ExecSQL;
end;

procedure TDBManipulationSQLServer.ExecuteBlock(SQL: string);
begin
  SetSQL(SQL);
  FDBConnection.UniQuery.ParamCheck := False;
  Execute;
  FDBConnection.UniQuery.ParamCheck := True;
end;

procedure TDBManipulationSQLServer.ExecuteDirect(SQL: string; Args: array of const);
begin
  ExecuteDirect(Format(SQL, Args));
end;

procedure TDBManipulationSQLServer.ExecuteDirect(SQL: string);
begin
  SetSQL(SQL);
  Execute;
end;

function TDBManipulationSQLServer.GetAsInteger(AFieldName: string): Integer;
begin
  if FDBConnection.UniQuery.FieldByName(AFieldName).IsNull then
    Result := -1
  else
    Result := FDBConnection.UniQuery.FieldByName(AFieldName).AsInteger
end;

function TDBManipulationSQLServer.GetFailSQL: Boolean;
begin
  Result := FDBConnection.FailSQL;
end;

function TDBManipulationSQLServer.GetLogSQL: Boolean;
begin
  Result := FDBConnection.LogSQL;
end;

function TDBManipulationSQLServer.GetRaiseException: Boolean;
begin
  Result := FDBConnection.RaiseException;
end;

function TDBManipulationSQLServer.GetRowsAffected: Integer;
begin
  Result := FDBConnection.RowsAffected;
end;

function TDBManipulationSQLServer.GetTransactionId: Int64;
begin
  Result := FDBConnection.TransactionId;
end;

procedure TDBManipulationSQLServer.Reconnect;
begin
  FDBConnection.Reconnect;
end;

procedure TDBManipulationSQLServer.ReplaceParam(AParam, Value: string);
begin
  { TODO -oUlder -cURGENTE : Resolver problema quando há parâmetros que um é substring do outro. }
  FDBConnection.UniQuery.SQL.Text := StringReplace(FDBConnection.UniQuery.SQL.Text, ':' + AParam, Value, [rfIgnoreCase, rfReplaceAll])
end;

procedure TDBManipulationSQLServer.SetParam(AParam: string; Value: Integer; ForceNull: Boolean);
begin
  if ForceNull and (Value = -1) then
    FDBConnection.UniQuery.ParamByName(AParam).Clear
  else
    FDBConnection.UniQuery.ParamByName(AParam).AsInteger := Value;
end;

procedure TDBManipulationSQLServer.SetParam(AParam: string; Value: Boolean);
begin
  FDBConnection.UniQuery.ParamByName(AParam).AsBoolean := Value;
end;

procedure TDBManipulationSQLServer.SetParam(AParam, Value: string; ForceNull: Boolean);
begin
  if ForceNull and (Value = '') then
    FDBConnection.UniQuery.ParamByName(AParam).Clear
  else
    FDBConnection.UniQuery.ParamByName(AParam).Value := Value;
end;

procedure TDBManipulationSQLServer.SetLogSQL(const Value: Boolean);
begin
  FDBConnection.LogSQL := Value;
end;

procedure TDBManipulationSQLServer.SetParam(AParam: string; Value: TMemoryStream; ForceNull: Boolean);
var
  LParam: TUniParam;
begin
  Value.Position := 0;
  LParam := FDBConnection.UniQuery.ParamByName(AParam);
  LParam.LoadFromStream(Value, ftBlob);
end;

procedure TDBManipulationSQLServer.SetParam(AParam: string; Value: Int64; ForceNull: Boolean);
begin
  if ForceNull and (Value = -1) then
    FDBConnection.UniQuery.ParamByName(AParam).Clear
  else
    FDBConnection.UniQuery.ParamByName(AParam).Value := Value;
end;

procedure TDBManipulationSQLServer.SetParam(AParam: string; Value: Real; ForceNull: Boolean);
begin
  if ForceNull and (Value = -1) then
    FDBConnection.UniQuery.ParamByName(AParam).Clear
  else
    FDBConnection.UniQuery.ParamByName(AParam).AsFloat := Value;
end;

procedure TDBManipulationSQLServer.SetParamDate(AParam: string; Value: TDate; ForceNull: Boolean);
begin
  if ForceNull and (Value = 0) then
    FDBConnection.UniQuery.ParamByName(AParam).Clear
  else
    FDBConnection.UniQuery.ParamByName(AParam).AsDate := Value;
end;

procedure TDBManipulationSQLServer.SetParamDateTime(AParam: string; Value: TDateTime; ForceNull: Boolean);
begin
  if ForceNull and (Value = 0) then
    FDBConnection.UniQuery.ParamByName(AParam).Clear
  else
    FDBConnection.UniQuery.ParamByName(AParam).AsDateTime := Value;
end;

procedure TDBManipulationSQLServer.SetParamTime(AParam: string; Value: TTime; ForceNull: Boolean);
begin
  if ForceNull and (Value = 0) then
    FDBConnection.UniQuery.ParamByName(AParam).Clear
  else
    FDBConnection.UniQuery.ParamByName(AParam).AsTime := Value;
end;

procedure TDBManipulationSQLServer.SetRaiseException(const Value: Boolean);
begin
  FDBConnection.RaiseException := Value;
end;

procedure TDBManipulationSQLServer.SetSQLInsert(ASQL, AReturnField: string);
begin
  SetSQL(Format(ASQL, ['OUTPUT INSERTED.' + AReturnField, '']));
end;

procedure TDBManipulationSQLServer.SetSQL(ASQL: string; Args: array of const);
begin
  SetSQL(Format(ASQL, Args));
end;

procedure TDBManipulationSQLServer.SetSQL(ASQL: string);
begin
  FDBConnection.ResetQuery;
  FDBConnection.BeginLocalTransaction;
  FDBConnection.UniQuery.SQL.Text := ASQL;
end;

end.
