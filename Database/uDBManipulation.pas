﻿unit uDBManipulation;

interface

uses
  uDBManipulationCustom, Classes, Controls, uDBHeader;

const
  // o valor da constante está como duplica para atender o nome em português(duplicada) e em inglês(duplicate)
  CHAVE_DUPLICADA = 'duplica';
  UNIQUE_KEY = 'UNIQUE KEY';
  FOREIGN_KEY = 'FOREIGN';

type
  TDBManipulation = class(TDBManipulationCustom)
  private
    FDBManipulation: TDBManipulationCustom;
    function GetFailSQL: Boolean; override;
    function GetRowsAffected: Integer; override;
  protected
    function GetLogSQL: Boolean; override;
    function GetRaiseException: Boolean; override;
    procedure SetLogSQL(const Value: Boolean); override;
    procedure SetRaiseException(const Value: Boolean); override;

    function GetTransactionId: Int64; override;
  public
    // controle da classe
    constructor Create(ADatabase: TDatabases = dbPrincipal);
    destructor Destroy; override;
    // controle de transações
    procedure BeginTransaction; override;
    procedure EndTransaction; override;
    // controle de conexão
    procedure Reconnect; override;
    // processamento do sql
    procedure Execute(HasReturn: Boolean = False; VerificarTabela: Boolean = True); override;
    procedure ExecuteBlock(SQL: string); override;
    procedure ExecuteDirect(SQL: string); override;
    procedure ExecuteDirect(SQL: string; Args: array of const); override;
    // controle de input
    procedure SetSQL(ASQL: string); overload; override;
    procedure SetSQL(ASQL: string; Args: array of const); overload; override;
    procedure SetSQLInsert(ASQL, AReturnField: string); override;
    procedure SetParam(AParam: string; Value: Boolean); override;
    procedure SetParam(AParam: string; Value: Integer; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: Int64; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: Real; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: string; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: TMemoryStream; ForceNull: Boolean = True); override;
    procedure SetParamTime(AParam: string; Value: TTime; ForceNull: Boolean = True); override;
    procedure SetParamDate(AParam: string; Value: TDate; ForceNull: Boolean = True); override;
    procedure SetParamDateTime(AParam: string; Value: TDateTime; ForceNull: Boolean = True); override;
    // controle de output (returning)
    function GetAsInteger(AFieldName: string): Integer; override;
    property FailSQL: Boolean read GetFailSQL;
    property RowsAffected: Integer read GetRowsAffected;
  end;

implementation

uses
  uDBManipulationFirebird, uDBManipulationSQLServer, uDBConnectionCustom, SysUtils;

{ TDBManipulation }

procedure TDBManipulation.BeginTransaction;
begin
  FDBManipulation.BeginTransaction;
end;

constructor TDBManipulation.Create(ADatabase: TDatabases);
begin
  case DBSettings.SGBDType of
    sgbdFirebird:  FDBManipulation := TDBManipulationFirebird.Create(ADatabase);
    sgbdSQLServer: FDBManipulation := TDBManipulationSQLServer.Create(ADatabase);
  end;
end;

destructor TDBManipulation.Destroy;
begin
  FDBManipulation.Free;

  inherited;
end;

procedure TDBManipulation.EndTransaction;
begin
  FDBManipulation.EndTransaction;
end;

procedure TDBManipulation.Execute(HasReturn: Boolean; VerificarTabela: Boolean);
begin
  FDBManipulation.Execute(HasReturn, VerificarTabela);
end;

procedure TDBManipulation.ExecuteBlock(SQL: string);
begin
  FDBManipulation.ExecuteBlock(SQL);
end;

procedure TDBManipulation.ExecuteDirect(SQL: string);
begin
  FDBManipulation.ExecuteDirect(SQL);
end;

procedure TDBManipulation.ExecuteDirect(SQL: string; Args: array of const);
begin
  FDBManipulation.ExecuteDirect(SQL, Args);
end;

function TDBManipulation.GetAsInteger(AFieldName: string): Integer;
begin
  Result := FDBManipulation.GetAsInteger(AFieldName);
end;

function TDBManipulation.GetFailSQL: Boolean;
begin
  Result := FDBManipulation.FailSQL;  
end;

function TDBManipulation.GetLogSQL: Boolean;
begin
  Result := FDBManipulation.LogSQL;
end;

function TDBManipulation.GetRaiseException: Boolean;
begin
  Result := FDBManipulation.RaiseException;
end;

function TDBManipulation.GetRowsAffected: Integer;
begin
  Result := FDBManipulation.RowsAffected;
end;

function TDBManipulation.GetTransactionId: Int64;
begin
  Result := FDBManipulation.TransactionId;
end;

procedure TDBManipulation.Reconnect;
begin
  FDBManipulation.Reconnect;
end;

procedure TDBManipulation.SetParam(AParam: string; Value: Boolean);
begin
  FDBManipulation.SetParam(AParam, Value);
end;

procedure TDBManipulation.SetParam(AParam: string; Value: Integer; ForceNull: Boolean);
begin
  FDBManipulation.SetParam(AParam, Value, ForceNull);
end;

procedure TDBManipulation.SetParam(AParam, Value: string; ForceNull: Boolean);
begin
  FDBManipulation.SetParam(AParam, Value, ForceNull);
end;

procedure TDBManipulation.SetLogSQL(const Value: Boolean);
begin
  FDBManipulation.LogSQL := Value;
end;

procedure TDBManipulation.SetParam(AParam: string; Value: TMemoryStream; ForceNull: Boolean);
begin
  FDBManipulation.SetParam(AParam, Value, ForceNull);
end;

procedure TDBManipulation.SetParam(AParam: string; Value: Real; ForceNull: Boolean);
begin
  FDBManipulation.SetParam(AParam, Value, ForceNull);
end;

procedure TDBManipulation.SetParam(AParam: string; Value: Int64; ForceNull: Boolean);
begin
  FDBManipulation.SetParam(AParam, Value, ForceNull);
end;

procedure TDBManipulation.SetParamDate(AParam: string; Value: TDate; ForceNull: Boolean);
begin
  FDBManipulation.SetParamDate(AParam, Value, ForceNull);
end;

procedure TDBManipulation.SetParamDateTime(AParam: string; Value: TDateTime; ForceNull: Boolean);
begin
  FDBManipulation.SetParamDateTime(AParam, Value, ForceNull);
end;

procedure TDBManipulation.SetParamTime(AParam: string; Value: TTime; ForceNull: Boolean);
begin
  FDBManipulation.SetParamTime(AParam, Value, ForceNull);
end;

procedure TDBManipulation.SetRaiseException(const Value: Boolean);
begin
  FDBManipulation.RaiseException := Value;
end;

procedure TDBManipulation.SetSQL(ASQL: string; Args: array of const);
begin
  FDBManipulation.SetSQL(ASQL, Args);
end;

procedure TDBManipulation.SetSQLInsert(ASQL, AReturnField: string);
begin
  FDBManipulation.SetSQLInsert(ASQL, AReturnField);
end;

procedure TDBManipulation.SetSQL(ASQL: string);
begin
  FDBManipulation.SetSQL(ASQL);
end;

end.
