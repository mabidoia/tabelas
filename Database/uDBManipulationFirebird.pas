﻿unit uDBManipulationFirebird;

interface

uses
  uDBManipulationCustom, Classes, Controls, uDBConnectionCustom, uDBConnectionFirebird, 
  uDBHeader, Uni;

type
  TDBManipulationFirebird = class(TDBManipulationCustom)
  private
    FDBConnection: TDBConnectionFirebird;
    procedure ReplaceParam(AParam, Value: string);
  protected
    function GetFailSQL: Boolean; override;
    function GetLogSQL: Boolean; override;
    function GetRaiseException: Boolean; override;
    procedure SetLogSQL(const Value: Boolean); override;
    procedure SetRaiseException(const Value: Boolean); override;
    function GetTransactionId: Int64; override;
    function GetRowsAffected: Integer; override;
  public
    // controle da classe
    constructor Create(ADatabase: TDatabases);
    destructor Destroy; override;
    // controle de transações
    procedure BeginTransaction; override;
    procedure EndTransaction; override;
    // controle de conexão
    procedure Reconnect; override;
    // processamento do sql
    procedure Execute(HasReturn: Boolean = False; VerificarTabela: Boolean = True); override;
    procedure ExecuteBlock(SQL: string); override;
    procedure ExecuteDirect(SQL: string); override;
    procedure ExecuteDirect(SQL: string; Args: array of const); override;
    // controle de input
    procedure SetSQL(ASQL: string); overload; override;
    procedure SetSQL(ASQL: string; Args: array of const); overload; override;
    procedure SetSQLInsert(ASQL, AReturnField: string); overload; override;
    procedure SetParam(AParam: string; Value: Boolean); override;
    procedure SetParam(AParam: string; Value: Integer; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: Int64; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: Real; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: string; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: TMemoryStream; ForceNull: Boolean = True); override;
    procedure SetParamTime(AParam: string; Value: TTime; ForceNull: Boolean = True); override;
    procedure SetParamDate(AParam: string; Value: TDate; ForceNull: Boolean = True); override;
    procedure SetParamDateTime(AParam: string; Value: TDateTime; ForceNull: Boolean = True); override;
    // controle de output (returning)
    function GetAsInteger(AFieldName: string): Integer; override;
  end;

implementation

uses
  uDBQuery, SysUtils, DB, SqlTimSt, uHelperLog, IBSQL;

{ TDBManipulationFirebird }

procedure TDBManipulationFirebird.BeginTransaction;
begin
  FDBConnection.BeginTransaction;
end;

constructor TDBManipulationFirebird.Create(ADatabase: TDatabases);
begin
  FDBConnection := TDBConnectionFirebird.Create(ADatabase);
end;

destructor TDBManipulationFirebird.Destroy;
begin
  FDBConnection.Free;
  inherited;
end;

procedure TDBManipulationFirebird.EndTransaction;
begin
  FDBConnection.EndTransaction;
end;

procedure TDBManipulationFirebird.Execute(HasReturn: Boolean; VerificarTabela: Boolean);
begin
  FDBConnection.ExecSQL;
end;

procedure TDBManipulationFirebird.ExecuteBlock(SQL: string);
begin
  SetSQL(SQL);
  FDBConnection.UniSQL.ParamCheck := False;
  Execute;
  FDBConnection.UniSQL.ParamCheck := True;
end;

procedure TDBManipulationFirebird.ExecuteDirect(SQL: string; Args: array of const);
begin
  ExecuteDirect(Format(SQL, Args));
end;

procedure TDBManipulationFirebird.ExecuteDirect(SQL: string);
begin
  SetSQL(SQL);
  Execute;
end;

function TDBManipulationFirebird.GetAsInteger(AFieldName: string): Integer;
begin
  if FDBConnection.UniSQL.ParamByName('RET_' + AFieldName).IsNull then
    Result := -1
  else
    Result := FDBConnection.UniSQL.ParamByName('RET_' + AFieldName).AsInteger
end;

function TDBManipulationFirebird.GetFailSQL: Boolean;
begin
  Result := FDBConnection.FailSQL;
end;

function TDBManipulationFirebird.GetLogSQL: Boolean;
begin
  Result := FDBConnection.LogSQL;
end;

function TDBManipulationFirebird.GetRaiseException: Boolean;
begin
  Result := FDBConnection.RaiseException;
end;

function TDBManipulationFirebird.GetRowsAffected: Integer;
begin
  Result := FDBConnection.RowsAffected;
end;

function TDBManipulationFirebird.GetTransactionId: Int64;
begin
  Result := FDBConnection.TransactionId;
end;

procedure TDBManipulationFirebird.Reconnect;
begin
  FDBConnection.Reconnect;
end;

procedure TDBManipulationFirebird.ReplaceParam(AParam, Value: string);
begin
  { TODO -oUlder -cURGENTE : Resolver problema quando há parâmetros que um é substring do outro. }
  FDBConnection.UniSQL.SQL.Text := StringReplace(FDBConnection.UniSQL.SQL.Text, ':' + AParam, Value, [rfIgnoreCase, rfReplaceAll])
end;

procedure TDBManipulationFirebird.SetParam(AParam: string; Value: Integer; ForceNull: Boolean);
begin
  if ForceNull and (Value = -1) then
    FDBConnection.UniSQL.ParamByName(AParam).Clear
  else
    FDBConnection.UniSQL.ParamByName(AParam).AsInteger := Value;
end;

procedure TDBManipulationFirebird.SetParam(AParam: string; Value: Boolean);
begin
  FDBConnection.UniSQL.ParamByName(AParam).AsBoolean := Value;
end;

procedure TDBManipulationFirebird.SetParam(AParam, Value: string; ForceNull: Boolean);
begin
  if ForceNull and (Value = '') then
    FDBConnection.UniSQL.ParamByName(AParam).Clear
  else
    FDBConnection.UniSQL.ParamByName(AParam).AsString := Value;
end;

procedure TDBManipulationFirebird.SetLogSQL(const Value: Boolean);
begin
  FDBConnection.LogSQL := Value;
end;

procedure TDBManipulationFirebird.SetParam(AParam: string; Value: TMemoryStream; ForceNull: Boolean);
var
  LParam: TUniParam;
begin
  Value.Position := 0;
  LParam := FDBConnection.UniSQL.ParamByName(AParam);
  LParam.LoadFromStream(Value, ftBlob);
end;

procedure TDBManipulationFirebird.SetParam(AParam: string; Value: Int64; ForceNull: Boolean);
begin
  if ForceNull and (Value = -1) then
    FDBConnection.UniSQL.ParamByName(AParam).Clear
  else
    FDBConnection.UniSQL.ParamByName(AParam).Value := Value;
end;

procedure TDBManipulationFirebird.SetParam(AParam: string; Value: Real; ForceNull: Boolean);
begin
  if ForceNull and (Value = -1) then
    FDBConnection.UniSQL.ParamByName(AParam).Clear
  else
    FDBConnection.UniSQL.ParamByName(AParam).AsFloat := Value;
end;

procedure TDBManipulationFirebird.SetParamDate(AParam: string; Value: TDate; ForceNull: Boolean);
begin
  if ForceNull and (Value = 0) then
    FDBConnection.UniSQL.ParamByName(AParam).Clear
  else
    FDBConnection.UniSQL.ParamByName(AParam).AsDate := Value;
end;

procedure TDBManipulationFirebird.SetParamDateTime(AParam: string; Value: TDateTime; ForceNull: Boolean);
begin
  if ForceNull and (Value = 0) then
    FDBConnection.UniSQL.ParamByName(AParam).Clear
  else
    FDBConnection.UniSQL.ParamByName(AParam).AsDateTime := Value;
end;

procedure TDBManipulationFirebird.SetParamTime(AParam: string; Value: TTime; ForceNull: Boolean);
begin
  if ForceNull and (Value = 0) then
    FDBConnection.UniSQL.ParamByName(AParam).Clear
  else
    FDBConnection.UniSQL.ParamByName(AParam).AsTime := Value;
end;

procedure TDBManipulationFirebird.SetRaiseException(const Value: Boolean);
begin
  FDBConnection.RaiseException := Value;
end;

procedure TDBManipulationFirebird.SetSQLInsert(ASQL, AReturnField: string);
begin
  SetSQL(Format(ASQL, ['','RETURNING ' + AReturnField]));
end;

procedure TDBManipulationFirebird.SetSQL(ASQL: string; Args: array of const);
begin
  SetSQL(Format(ASQL, Args));
end;

procedure TDBManipulationFirebird.SetSQL(ASQL: string);
begin
  FDBConnection.BeginLocalTransaction;
  FDBConnection.ResetQuery;
  FDBConnection.UniSQL.SQL.Text := ASQL;
end;

end.
