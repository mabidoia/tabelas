unit uDBQueryPostgreSql;

interface

uses
  uDBQueryCustom, uDBConnectionPostgreSql, uDBHeader, Classes, Controls, DB, 
  DBClient;

type
  TDBQueryPostgreSql = class(TDBQueryCustom)
  private
    FDBConnection: TDBConnectionPostgreSql;
    function GetFieldCount: Integer;
    function GetFields: TFields;
  protected
    // controle de acesso as properties
    function GetEof: Boolean; override;
    function GetFilter: string; override;
    function GetFiltered: Boolean; override;
    function GetLogSQL: Boolean; override;
    function GetRaiseException: Boolean; override;
    procedure SetLogSQL(const Value: Boolean); override;
    procedure SetRaiseException(const Value: Boolean); override;
    procedure SetFilter(const Value: string); override;
    procedure SetFiltered(const Value: Boolean); override;
    function GetTransactionId: Int64; override;
  public
    // controle da classe
    constructor Create(ADatabase: TDatabases);
    destructor Destroy; override;
    // controle de transa��es
    procedure BeginTransaction; override;
    procedure EndTransaction; override;
    // processamento do sql
    procedure Execute(HasReturn: Boolean = False; VerificarTabela: Boolean = True); override;
    procedure ExecuteDirect(SQL: string); override;
    procedure ExecuteDirect(SQL: string; Args: array of const); override;
    // controle de input
    procedure SetSQL(ASQL: string); overload; override;
    procedure SetSQL(ASQL: string; Args: array of const); overload; override;
    procedure SetParam(AParam: string; Value: Boolean); override;
    procedure SetParam(AParam: string; Value: Integer; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: Int64; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: Real; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: string; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: TMemoryStream; ForceNull: Boolean = True); override;
    procedure SetParamTime(AParam: string; Value: TTime; ForceNull: Boolean = True); override;
    procedure SetParamDate(AParam: string; Value: TDate; ForceNull: Boolean = True); override;
    procedure SetParamDateTime(AParam: string; Value: TDateTime; ForceNull: Boolean = True); override;
    // controle de output
    function GetAsBoolean(AFieldName: string): Boolean; override;
    function GetAsInteger(AFieldName: string): Integer; override;
    function GetAsInt64(AFieldName: string): Int64; override;
    function GetAsFloat(AFieldName: string): Real; override;
    function GetAsString(AFieldName: string): string; override;
    function GetAsDateTime(AFieldName: string): TDateTime; override;
    procedure GetAsStream(AFieldName: string; out AMemoryStream: TMemoryStream); override;
    function IsNull(AFieldName: string): Boolean; override;
    function GetColumns: string; override;
    function Locate(const KeyFields: string; const KeyValues: Variant; Options: TLocateOptions): Boolean; override;
    function FieldByName(AFieldName: string): TField; override;
    // controle de navega��o
    procedure Next; override;
    procedure Last; override;
    procedure First; override;

    // controle adicional
    procedure InsertIntoClientDataSet(ADataSet: TClientDataSet); override;
    // properties
    property Eof: Boolean read GetEof;                  // Indicates whether a dataset is positioned at the last record
    property FieldCount: Integer read GetFieldCount;
    property Fields: TFields read GetFields;
    property Filter: string read GetFilter write SetFilter;
    property Filtered: Boolean read GetFiltered write SetFiltered;
  end;

implementation

uses
  SysUtils, SqlTimSt;

{ TDBQueryPostgreSql }

procedure TDBQueryPostgreSql.BeginTransaction;
begin
  FDBConnection.BeginTransaction;
end;

constructor TDBQueryPostgreSql.Create(ADatabase: TDatabases);
begin
  FDBConnection := TDBConnectionPostgreSql.Create(ADatabase);
end;

destructor TDBQueryPostgreSql.Destroy;
begin
  FDBConnection.Free;

  inherited;
end;

procedure TDBQueryPostgreSql.EndTransaction;
begin
  FDBConnection.EndTransaction;
end;

procedure TDBQueryPostgreSql.Execute(HasReturn: Boolean; VerificarTabela: Boolean);
begin
  FDBConnection.OpenQuery;
end;

procedure TDBQueryPostgreSql.ExecuteDirect(SQL: string; Args: array of const);
begin
  ExecuteDirect(Format(SQL, Args));
end;

procedure TDBQueryPostgreSql.ExecuteDirect(SQL: string);
begin
  SetSQL(SQL);
  Execute;
end;

function TDBQueryPostgreSql.FieldByName(AFieldName: string): TField;
begin
  Result := FDBConnection.SQLQuery.FieldByName(AFieldName);
end;

procedure TDBQueryPostgreSql.First;
begin
  FDBConnection.SQLQuery.First;
end;

function TDBQueryPostgreSql.GetAsBoolean(AFieldName: string): Boolean;
begin
  Result := FDBConnection.SQLQuery.FieldByName(AFieldName).AsBoolean;
end;

function TDBQueryPostgreSql.GetAsDateTime(AFieldName: string): TDateTime;
begin
  if FDBConnection.SQLQuery.FieldByName(AFieldName).IsNull then
    Result := 0
  else
    Result := FDBConnection.SQLQuery.FieldByName(AFieldName).AsDateTime;
end;

function TDBQueryPostgreSql.GetAsFloat(AFieldName: string): Real;
begin
  if FDBConnection.SQLQuery.FieldByName(AFieldName).IsNull then
    Result := 0
  else
    Result := FDBConnection.SQLQuery.FieldByName(AFieldName).AsFloat;
end;

function TDBQueryPostgreSql.GetAsInt64(AFieldName: string): Int64;
begin
  if FDBConnection.SQLQuery.FieldByName(AFieldName).IsNull then
    Result := -1
  else
    Result := StrToInt64(FDBConnection.SQLQuery.FieldByName(AFieldName).AsString);
end;

function TDBQueryPostgreSql.GetAsInteger(AFieldName: string): Integer;
begin
  if FDBConnection.SQLQuery.FieldByName(AFieldName).IsNull then
    Result := -1
  else
    Result := FDBConnection.SQLQuery.FieldByName(AFieldName).AsInteger
end;

procedure TDBQueryPostgreSql.GetAsStream(AFieldName: string; out AMemoryStream: TMemoryStream);
begin
  AMemoryStream := TMemoryStream.Create;
  if not TBlobField(FDBConnection.SQLQuery.FieldByName(AFieldName)).IsNull then
  begin
    TBlobField(FDBConnection.SQLQuery.FieldByName(AFieldName)).SaveToStream(AMemoryStream);
    AMemoryStream.Position := 0;
  end;
end;

function TDBQueryPostgreSql.GetAsString(AFieldName: string): string;
begin
  if FDBConnection.SQLQuery.FieldByName(AFieldName).IsNull then
    Result := ''
  else
    Result := FDBConnection.SQLQuery.FieldByName(AFieldName).AsString;
end;

function TDBQueryPostgreSql.GetColumns: string;
var
  I: Integer;
begin
  Result := '';

  for I := 0 to FDBConnection.SQLQuery.FieldCount - 1 do
    Result := Result + FDBConnection.SQLQuery.Fields[i].FieldName + ',';
end;

function TDBQueryPostgreSql.GetEof: Boolean;
begin
  Result := FDBConnection.SQLQuery.Eof;
end;

function TDBQueryPostgreSql.GetFieldCount: Integer;
begin
  Result := FDBConnection.SQLQuery.FieldCount;
end;

function TDBQueryPostgreSql.GetFields: TFields;
begin
  Result := FDBConnection.SQLQuery.Fields;
end;

function TDBQueryPostgreSql.GetFilter: string;
begin
  Result := FDBConnection.SQLQuery.Filter;
end;

function TDBQueryPostgreSql.GetFiltered: Boolean;
begin
  Result := FDBConnection.SQLQuery.Filtered;
end;

function TDBQueryPostgreSql.GetLogSQL: Boolean;
begin
  Result := FDBConnection.LogSQL;
end;

function TDBQueryPostgreSql.GetRaiseException: Boolean;
begin
  Result := FDBConnection.RaiseException;
end;

function TDBQueryPostgreSql.GetTransactionId: Int64;
begin
  Result := FDBConnection.TransactionId;
end;

procedure TDBQueryPostgreSql.InsertIntoClientDataSet(ADataSet: TClientDataSet);
var
  i: integer;
  LValor: String;
begin
  // cria os campos no dataset
  for i := 0 to FDBConnection.SQLQuery.FieldDefs.Count - 1 do
  begin
    if (FDBConnection.SQLQuery.FieldDefs[i].DataType = ftTimeStamp) then
      FDBConnection.SQLQuery.FieldDefs[i].DataType := ftDateTime
    else if (FDBConnection.SQLQuery.FieldDefs[i].DataType = ftFMTBcd) then
      FDBConnection.SQLQuery.FieldDefs[i].DataType := ftLargeint;

    ADataSet.FieldDefs.Add(
      FDBConnection.SQLQuery.FieldDefs[i].Name,
      FDBConnection.SQLQuery.FieldDefs[i].DataType,
      FDBConnection.SQLQuery.FieldDefs[i].Size,
      FDBConnection.SQLQuery.FieldDefs[i].Required
    );

    if (FDBConnection.SQLQuery.FieldDefs[i].DataType = ftLargeint) then
      FDBConnection.SQLQuery.FieldDefs[i].DataType := ftFMTBcd;
  end;
  ADataSet.CreateDataSet;
  // povoa o dataset com o resultado da query
  while not Eof do
  begin
    // cria novo registro do CDS
    ADataSet.Append;
    // insere os valores dos campos no CDS
    for i := 0 to ADataSet.Fields.Count - 1 do
    begin
      if FDBConnection.SQLQuery.Fields[i].isNull then
        Continue;

      if FDBConnection.SQLQuery.FieldDefs[i].DataType = ftFMTBcd then
      begin
        ADataSet.Fields[i].Value := StrToInt64(FDBConnection.SQLQuery.Fields[i].AsString);
      end
      else
        ADataSet.Fields[i].Value := FDBConnection.SQLQuery.Fields[i].Value;
    end;
    // salva o registro no CDS
    ADataSet.Post;
    // carrega pr�ximo resultado da consulta
    Next;
  end;

end;

function TDBQueryPostgreSql.IsNull(AFieldName: string): Boolean;
begin
  Result := FDBConnection.SQLQuery.FieldByName(AFieldName).IsNull;
end;

procedure TDBQueryPostgreSql.Last;
begin
  FDBConnection.SQLQuery.Last;
end;

function TDBQueryPostgreSql.Locate(const KeyFields: string; const KeyValues: Variant;
  Options: TLocateOptions): Boolean;
begin
  Result := FDBConnection.SQLQuery.Locate(KeyFields, KeyValues, Options);
end;

procedure TDBQueryPostgreSql.Next;
begin
  FDBConnection.SQLQuery.Next;
end;

procedure TDBQueryPostgreSql.SetFilter(const Value: string);
begin
  FDBConnection.SQLQuery.Filter := Value;
end;

procedure TDBQueryPostgreSql.SetFiltered(const Value: Boolean);
begin
  FDBConnection.SQLQuery.Filtered := Value;
end;

procedure TDBQueryPostgreSql.SetLogSQL(const Value: Boolean);
begin
  FDBConnection.LogSQL := Value;
end;

procedure TDBQueryPostgreSql.SetParam(AParam: string; Value: Real; ForceNull: Boolean);
begin
  FDBConnection.SQLQuery.ParamByName(AParam).AsFloat := Value;
end;

procedure TDBQueryPostgreSql.SetParam(AParam, Value: string; ForceNull: Boolean);
begin
  FDBConnection.SQLQuery.ParamByName(AParam).AsString := Value;
end;

procedure TDBQueryPostgreSql.SetParam(AParam: string; Value: TMemoryStream; ForceNull: Boolean);
var
  LParam: TParam;
begin
  Value.Position := 0;
  LParam := FDBConnection.SQLQuery.ParamByName(AParam);
  LParam.ParamType := ptInput;
  LParam.DataType := ftBlob;

  if Value.Size > 0 then
    LParam.SetBlobData(Value.Memory, Value.Size)
  else
    LParam.Clear;
end;

procedure TDBQueryPostgreSql.SetParam(AParam: string; Value: Int64; ForceNull: Boolean);
begin
  FDBConnection.SQLQuery.ParamByName(AParam).AsString := IntToStr(Value);
end;

procedure TDBQueryPostgreSql.SetParam(AParam: string; Value: Boolean);
begin
  FDBConnection.SQLQuery.ParamByName(AParam).AsBoolean := Value;
end;

procedure TDBQueryPostgreSql.SetParam(AParam: string; Value: Integer; ForceNull: Boolean);
begin
  FDBConnection.SQLQuery.ParamByName(AParam).AsInteger := Value;
end;

procedure TDBQueryPostgreSql.SetParamDate(AParam: string; Value: TDate; ForceNull: Boolean);
begin
  FDBConnection.SQLQuery.ParamByName(AParam).AsSQLTimeStamp := DateTimeToSQLTimeStamp(Value);
  SetMinMaxDataTime(Value);
end;

procedure TDBQueryPostgreSql.SetParamDateTime(AParam: string; Value: TDateTime; ForceNull: Boolean);
begin
  FDBConnection.SQLQuery.ParamByName(AParam).AsSQLTimeStamp := DateTimeToSQLTimeStamp(Value);
  SetMinMaxDataTime(Value);
end;

procedure TDBQueryPostgreSql.SetParamTime(AParam: string; Value: TTime; ForceNull: Boolean);
begin
  FDBConnection.SQLQuery.ParamByName(AParam).AsTime := Value;
end;

procedure TDBQueryPostgreSql.SetRaiseException(const Value: Boolean);
begin
  FDBConnection.RaiseException := Value;
end;

procedure TDBQueryPostgreSql.SetSQL(ASQL: string; Args: array of const);
begin
  SetSQL(Format(ASQL, Args));
end;

procedure TDBQueryPostgreSql.SetSQL(ASQL: string);
begin
  FDBConnection.ResetQuery;
  FDBConnection.SQLQuery.SQL.Text := ASQL;
end;

end.
