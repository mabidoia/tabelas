unit uDBManipulationPostgreSql;

interface

uses
  uDBManipulationCustom, uDBConnectionPostgreSql, uDBHeader, Classes, Controls;

type
  TDBManipulationPostgresSql = class(TDBManipulationCustom)
  private
    FDBConnection: TDBConnectionPostgreSql;
    procedure ReplaceParam(AParam, Value: string);
  protected
    function GetFailSQL: Boolean; override;
    function GetLogSQL: Boolean; override;
    function GetRaiseException: Boolean; override;
    procedure SetLogSQL(const Value: Boolean); override;
    procedure SetRaiseException(const Value: Boolean); override;
    function GetTransactionId: Int64; override;
  public
    // controle da classe
    constructor Create(ADatabase: TDatabases);
    destructor Destroy; override;
    // controle de transa��es
    procedure BeginTransaction; override;
    procedure EndTransaction; override;
    // controle de conex�o
    procedure Reconnect; override;
    // processamento do sql
    procedure Execute(HasReturn: Boolean = False; VerificarTabela: Boolean = True); override;
    procedure ExecuteDirect(SQL: string); override;
    procedure ExecuteDirect(SQL: string; Args: array of const); override;
    // controle de input
    procedure SetSQL(ASQL: string); overload; override;
    procedure SetSQL(ASQL: string; Args: array of const); overload; override;
    procedure SetSQLInsert(ASQL, AReturnField: string); overload; override;
    procedure SetParam(AParam: string; Value: Boolean); override;
    procedure SetParam(AParam: string; Value: Integer; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: Int64; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: Real; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: string; ForceNull: Boolean = True); override;
    procedure SetParam(AParam: string; Value: TMemoryStream; ForceNull: Boolean = True); override;
    procedure SetParamTime(AParam: string; Value: TTime; ForceNull: Boolean = True); override;
    procedure SetParamDate(AParam: string; Value: TDate; ForceNull: Boolean = True); override;
    procedure SetParamDateTime(AParam: string; Value: TDateTime; ForceNull: Boolean = True); override;
    // controle de output (returning)
    function GetAsInteger(AFieldName: string): Integer; override;

  end;

implementation

uses
  SysUtils, SqlTimSt, DB;

{ TDBManipulationPostgresSql }

procedure TDBManipulationPostgresSql.BeginTransaction;
begin
  FDBConnection.BeginTransaction;
end;

constructor TDBManipulationPostgresSql.Create(ADatabase: TDatabases);
begin
  FDBConnection := TDBConnectionPostgreSql.Create(ADatabase);
end;

destructor TDBManipulationPostgresSql.Destroy;
begin
  FDBConnection.Free;
  
  inherited;
end;

procedure TDBManipulationPostgresSql.EndTransaction;
begin
  FDBConnection.EndTransaction;
end;

procedure TDBManipulationPostgresSql.Execute(HasReturn: Boolean; VerificarTabela: Boolean);
begin
  if HasReturn then
    FDBConnection.OpenQuery
  else
    FDBConnection.ExecSQL;
end;

procedure TDBManipulationPostgresSql.ExecuteDirect(SQL: string);
begin
  SetSQL(SQL);
  Execute;
end;

procedure TDBManipulationPostgresSql.ExecuteDirect(SQL: string; Args: array of const);
begin
  ExecuteDirect(Format(SQL, Args));
end;

function TDBManipulationPostgresSql.GetAsInteger(AFieldName: string): Integer;
begin
  if FDBConnection.SQLQuery.FieldByName(AFieldName).IsNull then
    Result := -1
  else
    Result := FDBConnection.SQLQuery.FieldByName(AFieldName).AsInteger
end;

function TDBManipulationPostgresSql.GetFailSQL: Boolean;
begin
  Result := FDBConnection.FailSQL;
end;

function TDBManipulationPostgresSql.GetLogSQL: Boolean;
begin
  Result := FDBConnection.LogSQL;
end;

function TDBManipulationPostgresSql.GetRaiseException: Boolean;
begin
  Result := FDBConnection.RaiseException;
end;

function TDBManipulationPostgresSql.GetTransactionId: Int64;
begin
  Result := FDBConnection.TransactionId;
end;

procedure TDBManipulationPostgresSql.Reconnect;
begin
  FDBConnection.Reconnect;
end;

procedure TDBManipulationPostgresSql.ReplaceParam(AParam, Value: string);
begin
  FDBConnection.SQLQuery.SQL.Text := StringReplace(FDBConnection.SQLQuery.SQL.Text, ':' + AParam, Value, [rfIgnoreCase, rfReplaceAll])
end;

procedure TDBManipulationPostgresSql.SetLogSQL(const Value: Boolean);
begin
  FDBConnection.LogSQL := Value;
end;

procedure TDBManipulationPostgresSql.SetParam(AParam: string; Value: Real;
  ForceNull: Boolean);
begin
  if ForceNull and (Value = -1) then
    ReplaceParam(AParam, 'NULL')
  else
    FDBConnection.SQLQuery.ParamByName(AParam).AsFloat := Value;
end;

procedure TDBManipulationPostgresSql.SetParam(AParam: string; Value: TMemoryStream; ForceNull: Boolean);
var
  LParam: TParam;
begin
  Value.Position := 0;
  LParam := FDBConnection.SQLQuery.ParamByName(AParam);
  LParam.ParamType := ptInput;
  LParam.DataType := ftBlob;

  if Value.Size > 0 then
    LParam.SetBlobData(Value.Memory, Value.Size)
  else
    LParam.Clear;
end;

procedure TDBManipulationPostgresSql.SetParam(AParam, Value: string; ForceNull: Boolean);
begin
  if ForceNull and (Value = '') then
    ReplaceParam(AParam, 'NULL')
  else
    FDBConnection.SQLQuery.ParamByName(AParam).AsString := Value;
end;

procedure TDBManipulationPostgresSql.SetParam(AParam: string; Value: Int64;
  ForceNull: Boolean);
begin
  if ForceNull and (Value = -1) then
    ReplaceParam(AParam, 'NULL')
  else
    FDBConnection.SQLQuery.ParamByName(AParam).AsString := IntToStr(Value);
end;

procedure TDBManipulationPostgresSql.SetParam(AParam: string; Value: Boolean);
begin
  FDBConnection.SQLQuery.ParamByName(AParam).AsBoolean := Value
end;

procedure TDBManipulationPostgresSql.SetParam(AParam: string; Value: Integer;
  ForceNull: Boolean);
begin
  if ForceNull and (Value = -1) then
    ReplaceParam(AParam, 'NULL')
  else
    FDBConnection.SQLQuery.ParamByName(AParam).AsInteger := Value;
end;

procedure TDBManipulationPostgresSql.SetParamDate(AParam: string; Value: TDate;
  ForceNull: Boolean);
begin
  if ForceNull and (Value = 0) then
    ReplaceParam(AParam, 'NULL')
  else
    FDBConnection.SQLQuery.ParamByName(AParam).AsSQLTimeStamp := DateTimeToSQLTimeStamp(Value);
end;

procedure TDBManipulationPostgresSql.SetParamDateTime(AParam: string;
  Value: TDateTime; ForceNull: Boolean);
begin
  if ForceNull and (Value = 0) then
    ReplaceParam(AParam, 'NULL')
  else
    FDBConnection.SQLQuery.ParamByName(AParam).AsSQLTimeStamp := DateTimeToSQLTimeStamp(Value);
end;

procedure TDBManipulationPostgresSql.SetParamTime(AParam: string; Value: TTime;
  ForceNull: Boolean);
begin
  if ForceNull and (Value = 0) then
    ReplaceParam(AParam, 'NULL')
  else
    FDBConnection.SQLQuery.ParamByName(AParam).AsSQLTimeStamp := DateTimeToSQLTimeStamp(Value);
end;

procedure TDBManipulationPostgresSql.SetRaiseException(const Value: Boolean);
begin
  FDBConnection.RaiseException := Value;
end;

procedure TDBManipulationPostgresSql.SetSQL(ASQL: string);
begin
  FDBConnection.ResetQuery;
  FDBConnection.SQLQuery.SQL.Text := ASQL;
end;

procedure TDBManipulationPostgresSql.SetSQL(ASQL: string; Args: array of const);
begin
  SetSQL(Format(ASQL, Args));
end;

procedure TDBManipulationPostgresSql.SetSQLInsert(ASQL, AReturnField: string);
begin
  SetSQL(Format(ASQL, ['','RETURNING ' + AReturnField]));
end;

end.
