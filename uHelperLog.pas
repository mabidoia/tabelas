unit uHelperLog;

interface

uses
  SysUtils, Controls, Classes, SyncObjs, Forms, Dialogs;

type
  TLogType = (
    ltInfo,
    ltDebug,
    ltWarn,
    ltError,
    ltDBError,
    ltDBWarn,
    ltWeb,
    ltBenchMark,
    ltUnitTest,
    ltRelatorio,
    ltDebugViagem,
    ltDev,
    ltLoad,
    ltInt,
    ltIntGR
  );

  TWriteType = set of TLogType;
  TOnWriteLog = procedure(AType: TLogType; AMsg: string; ADateTime: TDateTime);

  TThreadZipArchives = class(TThread)
  strict private
    FDirPath: string;
    FFileExtention: string;
    FExcetionFiles: TStringList;
    F7ZipPath: string;
  protected
    procedure Execute; override;
  public
    constructor Create(ADirPath, AFileExtention, A7ZipPath: string; AExceptionFiles: TStringList);
    destructor Destroy; override;
  end;

  const
    KBYTE = 1024;
    MBYTE = 1024 * KBYTE;

  type
  TThreadWriteLogOnDisk = class(TThread)
  const
    WRITE_FORCED_DELAY = 20000; // Corresponde a 20 segundos
  private
    FRCMessages: TCriticalSection;
    //Arquivo onde vai ser escrito o log
    FLogFile: TFileStream;
    //Lista de mensagens pendentes para escrita no HD
    FPendingMessages: TStringStream;
    //Lista de mensagens que est�o sendo escritas, para n�o travar a regi�o critica
    FWrittingMessages: TStringStream;
    //Nos 20 segundos iniciais escreve tudo no log pra caso o servidor feche derrepente e n�o consigamos identificar a mensagem
    FDateTimeInicialization: TDateTime;

    procedure WriteLogsOnDisk;
  protected
    procedure Execute; override;
  public
    constructor Create;
    destructor Destroy; override;

    procedure SetLogFile(var ALogFile: TFileStream);
    procedure WriteLog(AMensagem: string; AForceWrite: Boolean);
  end;

  THelperLog = class
  const
    FILE_MAX_SIZE = 100 * MBYTE;
  private
    FLastLogType: TLogType;
    FCountMsg: array[TLogType] of NativeUInt;
    FCurrentDate: TDate;
    FPath: string;
    FFilename: string;
    F7Zip: string;
    FFileStream: TFileStream;
    FStoreDays: Integer;
    FWriteType: TWriteType;
    FWriteStandardOutput: Boolean;
    FOnWriteLog: TOnWriteLog;
    FThreadWriteLogOnDisk: TThreadWriteLogOnDisk;
    FisDelphiRunning: Boolean;
    // controle dos arquivos de log
    procedure ZipFiles;
    procedure DeleteOldFiles;
    function GetDateFromFilename(AFilename: string): TDateTime;
    // controle de escrita de mensagens no arquivo de log
    procedure ChangeLogFile;
    procedure ClearCounters;
    procedure WriteLog(AType: TLogType; AMsg: string);
    // controle de acesso as properties
    function GetCountMsg(Index: TLogType): Integer;
  protected
  public
    // controle da classe
    constructor Create(APathLog, A7Zip: string);
    destructor Destroy; override;
    // controle de escrita de mensagens no arquivo de log
    procedure Info(AMsg: string); overload;
    procedure Info(const AMsg: string; const AArgs: array of const); overload;
    procedure Dev(AMsg: string); overload;
    procedure Dev(const AMsg: string; const AArgs: array of const); overload;
    procedure Relatorio(AMsg: string); overload;
    procedure Relatorio(const AMsg: string; const AArgs: array of const); overload;
    procedure Debug(AMsg: string); overload;
    procedure Debug(const AMsg: string; const AArgs: array of const); overload;
    procedure Warn(AMsg: string); overload;
    procedure Warn(const AMsg: string; const AArgs: array of const); overload;
    procedure Error(AMsg: string; Sender: TObject = nil; E: Exception = nil; ANotifyClient: Boolean = False); overload;
    procedure Error(const AMsg: string; const AArgs: array of const; Sender: TObject = nil; E: Exception = nil; ANotifyClient: Boolean = False); overload;
    procedure DBWarn(AMsg: string); overload;
    procedure DBWarn(const AMsg: string; const AArgs: array of const); overload;
    procedure DBError(AMsg: string); overload;
    procedure DBError(const AMsg: string; const AArgs: array of const); overload;
    procedure Web(AMsg: string); overload;
    procedure Web(AMsg: string; const AArgs: array of const); overload;
    procedure UnitTest(AMsg: string); overload;
    procedure UnitTest(AMsg: string; const AArgs: array of const); overload;
    procedure BenchMark(AMsg: string); overload;
    procedure BenchMark(AMsg: string; const AArgs: array of const); overload;
    procedure DebugViagem(AMsg: string); overload;
    procedure DebugViagem(AMsg: string; const AArgs: array of const); overload;
    procedure ErroStackTrace(Sender: TObject; E: Exception);
    procedure Integracao(AMsg: string); overload;
    procedure Integracao(const AMsg: string; const AArgs: array of const); overload;
    procedure IntegracaoGR(AMsg: string); overload;
    procedure IntegracaoGR(const AMsg: string; const AArgs: array of const); overload;

    // properties
    property CountMsg[Index: TLogType]: Integer read GetCountMsg;
    property Path: string read FPath;
    property Filename: string read FFilename;
    property WriteType: TWriteType read FWriteType write FWriteType;
    property WriteStandardOutput: Boolean read FWriteStandardOutput write FWriteStandardOutput;
    property StoreDays: Integer read FStoreDays write FStoreDays;
    property OnWriteLog: TOnWriteLog read FOnWriteLog write FOnWriteLog;
  end;

var
  Logger: THelperLog;
  LoggerRequisicoesImagens: THelperLog;
  strTLogType: array[TLogType] of string;
  SDataHoraUltimaEscritaNoLog: TDateTime;

implementation

uses
  Windows, IdGlobal, IniFiles ,StrUtils, JclDebug, uUtils, uUtil, DateUtils;

var
  RCHelperLog: TCriticalSection;

const
  MSG_MEMORIA = '( Mem. Utilizada [ %d MB] Mem. F�sica Disp. [ %d MB] )';

function Format(const AFormat: string; const Args: array of const): string;
begin
  try
    Result := SysUtils.Format(AFormat, Args, FormatSettings);
  except
    on E: exception do
      Logger.Error('THelperLog.Format[%s]: Error of parameters.', [AFormat], nil, E);
  end;
end;

{ THelperLog }
procedure THelperLog.BenchMark(AMsg: string);
begin
  WriteLog(ltBenchMark, AMsg);
end;

procedure THelperLog.BenchMark(AMsg: string; const AArgs: array of const);
begin
  BenchMark(Format(AMsg, AArgs));
end;

procedure THelperLog.ChangeLogFile;
var
  LOldFilename: string;
  LFileTime: TTime;
  LZipFileName: string;
  LFileNew: string;

  function SetFileName: string;
  begin
    LFileNew := Format('%slog_%s_%d-%d.txt', [FPath, FormatDateTime('yyyy-mm-dd', FCurrentDate), HourOf(LFileTime), MinuteOf(LFileTime)]);
    LZipFileName := StringReplace(LFileNew, '.txt', '.zip', [rfReplaceAll]);
    Result := LFileNew;
  end;

begin
  if (FCurrentDate = Date) and (FFileStream.Size < FILE_MAX_SIZE) then
    Exit;

  LOldFilename := FFilename;
  // atualiza a data atual e o nome do arquivo de log
  FCurrentDate := DateOf(Now);

  LFileTime := Now;
  FFilename := Format('%slog_%s.txt', [FPath, FormatDateTime('yyyy-mm-dd', FCurrentDate)]);

  if FFileStream <> nil then
  begin
    while FileExists(SetFileName) or FileExists(LZipFileName) do
      LFileTime := IncMinute(LFileTime);

    FreeAndNil(FFileStream);
    RenameFile(FFilename, LFileNew);
  end;

  // cria o arquivo de log se necess�rio
  if not FileExists(FFilename) then
    FileClose(FileCreate(FFilename));
  // carrega o arquivo e posiciona o cursor no final

  FFileStream := TFileStream.Create(FFilename, fmOpenWrite + fmShareDenyNone);
  FFileStream.Position := FFileStream.Size;
  FThreadWriteLogOnDisk.SetLogFile(FFileStream);
  ClearCounters;
  // compacta os arquivos de log e remove os antigos

  ZipFiles;
  DeleteOldFiles;
end;

procedure THelperLog.ClearCounters;
var
  i: TLogType;
begin
  for i := Low(TLogType) to High(TLogType) do
    FCountMsg[i] := 0;
end;

function DelphiIsRunning: Boolean;
begin
  Result := {$IFDEF DEBUG} True or {$ENDIF} (FindWindowA('TAppBuilder', nil) <> 0);
end;

constructor THelperLog.Create(APathLog, A7Zip: string);
begin
  // inicializa thread que escreve efetivamente no disco
  FThreadWriteLogOnDisk := TThreadWriteLogOnDisk.Create;
  FThreadWriteLogOnDisk.Resume;

  // Variavel global que controra a ultima data hora de escrita  no log
  SDataHoraUltimaEscritaNoLog := Now;

  F7Zip := A7Zip;
  // inicializa os atributos da classe
  ClearCounters;
  FLastLogType := ltInfo;
  FCurrentDate := 0;
  FPath := APathLog;
  FFilename := '';
  FWriteType := [Low(TLogType)..High(TLogType)];
  FWriteStandardOutput := True;
  FStoreDays := 14;
  FOnWriteLog := nil;

  // cria o caminho do log se n�o existir
  if not DirectoryExists(FPath) then
    CreateDir(FPath);

  // carrega o arquivo de log
  ChangeLogFile;
  FisDelphiRunning := DelphiIsRunning;

  {$IFOPT W+}
  Application.OnException := Self.ErroStackTrace;
  {$ENDIF}
end;

procedure THelperLog.DBError(const AMsg: string; const AArgs: array of const);
begin
  DBError(Format(AMsg, AArgs));
end;

procedure THelperLog.DBWarn(AMsg: string);
begin
  WriteLog(ltDBWarn, AMsg);
end;

procedure THelperLog.DBWarn(const AMsg: string; const AArgs: array of const);
begin
  DBWarn(Format(AMsg, AArgs));
end;

procedure THelperLog.DBError(AMsg: string);
begin
  WriteLog(ltDBError, AMsg);
end;

procedure THelperLog.Debug(AMsg: string);
begin
  WriteLog(ltDebug, AMsg);
end;

procedure THelperLog.Debug(const AMsg: string; const AArgs: array of const);
begin
  Debug(Format(AMsg, AArgs));
end;

procedure THelperLog.DebugViagem(AMsg: string);
begin
  WriteLog(ltDebugViagem, AMsg);
end;

procedure THelperLog.DebugViagem(AMsg: string; const AArgs: array of const);
begin
  DebugViagem(Format(AMsg, AArgs));
end;

procedure THelperLog.DeleteOldFiles;
var
  i: Integer;
  LListFiles: TStringList;
  LDataLimite: TDateTime;
  LDateFile: TDateTime;
begin
  LDataLimite := IncDay(Now, -FStoreDays);
  // obt�m a lista de arquivos da pasta de log
  LListFiles := GetFiles(FPath, '*.zip', True);
  try
    for i := 0 to LListFiles.Count - 1 do
    begin
      // procura somente pelos arquivos de log zipados
      if Pos('log_', LListFiles.Strings[i]) > 0 then
      begin
        LDateFile := GetDateFromFilename(LListFiles.Strings[i]);
        if (LDateFile <> 0) and (LDateFile < LDataLimite) then
          DeleteFile(PWideChar(LListFiles.Strings[i]));
      end;
    end;
  finally
    LListFiles.Free;
  end;
end;

destructor THelperLog.Destroy;
begin
  FThreadWriteLogOnDisk.Terminate;
  FFileStream.Free;
  inherited;
end;

procedure THelperLog.Dev(AMsg: string);
begin
  if not FisDelphiRunning then
    Exit;

  WriteLog(ltDev, AMsg);
end;

procedure THelperLog.Dev(const AMsg: string; const AArgs: array of const);
begin
  if not FisDelphiRunning then
    Exit;
  Dev(Format(AMsg, AArgs));
end;

procedure THelperLog.Error(AMsg: string; Sender: TObject; E: Exception; ANotifyClient: Boolean);
const
  UM_MEGA = 1048576;
begin

  {Obs: Deve ser feito assim para n�o dar erro com o Guardian}
  if Pos('MEMORY',uppercase(AMsg)) > 0 then
  begin
    {$IFDEF CLIENTE}
    InfoAPPMemory;
    WriteLog(ltRelatorio, AMsg + '(Memoria utilizada[' + IntToStr(SMemoriaUtilizada div UM_MEGA) + ' MB])' );
    {$ENDIF}
    {$IFDEF SERVIDOR}
    WriteLog(ltRelatorio, AMsg + Format(MSG_MEMORIA, [SMemoriaUtilizada div UM_MEGA, InfoAPPMemory.GetAvailablePhisycalMemory]) );
    {$ENDIF}
  end
  else
    if E <> nil then
      AMsg := AMsg + ' | ' + E.ClassName + ': ' + e.Message;
    WriteLog(ltError, AMsg);

  {$IFOPT W+}
  if Assigned(Sender) and Assigned(E) then
    ErroStackTrace(Sender, E);
  {$ENDIF}
end;

procedure THelperLog.Error(const AMsg: string; const AArgs: array of const; Sender: TObject; E: Exception; ANotifyClient: Boolean);
begin
  Error(Format(AMsg, AArgs), Sender, E, ANotifyClient);
end;

procedure THelperLog.ErroStackTrace(Sender: TObject; E: Exception);
begin
  try
    {$IFOPT W+}
    if (E = nil) or (Sender = nil) then
      Exit;

    if Logger <> nil then
      Error(TraceException(E, @Sender));
    {$ENDIF}
  except
    on e: exception do
      Error('THelperLog.ErroStackTrace[Erro ao gerar stacktrace]: Sender [%d] | Exception [%d]', [Sender = nil, Exception = nil], Sender, E);
  end;
end;

function THelperLog.GetCountMsg(Index: TLogType): Integer;
begin
  Result := FCountMsg[Index];
end;

function THelperLog.GetDateFromFilename(AFilename: string): TDateTime;
var
  LFormatSettings: TFormatSettings;
begin
  // formato da data de log
  LFormatSettings.DateSeparator := '-';
  LFormatSettings.ShortDateFormat := 'yyyy-mm-dd';
  // obt�m somente o nome do arquivo sem a extens�o
  AFilename := ExtractFileName(AFilename);
  Fetch(AFilename, 'log_');
  AFilename := Fetch(AFilename, '.');
  // converte convete para data o nome do arquivo
  try
    Result := StrToDateTime(AFilename, LFormatSettings);
  except
    Result := 0;
  end;
end;

procedure THelperLog.Info(const AMsg: string; const AArgs: array of const);
begin
  Info(Format(AMsg, AArgs));
end;

procedure THelperLog.Integracao(AMsg: string);
begin
  WriteLog(ltInt, AMsg);
end;

procedure THelperLog.Integracao(const AMsg: string; const AArgs: array of const);
begin
  Integracao(Format(AMsg, AArgs));
end;

procedure THelperLog.IntegracaoGR(AMsg: string);
begin
  WriteLog(ltIntGR, AMsg);
end;

procedure THelperLog.IntegracaoGR(const AMsg: string; const AArgs: array of const);
begin
  IntegracaoGR(Format(AMsg, AArgs));
end;

procedure THelperLog.Relatorio(const AMsg: string; const AArgs: array of const);
begin
  Relatorio(Format(AMsg, AArgs));
end;

procedure THelperLog.Relatorio(AMsg: string);
const
  UM_MEGA = 1048576;
begin
  {Obs: Deve ser feito assim para n�o dar erro com o Guardian}
  {$IFDEF CLIENTE}
  InfoAPPMemory;
  WriteLog(ltRelatorio, AMsg + '(Memoria utilizada[' + IntToStr(SMemoriaUtilizada div UM_MEGA) + ' MB])' );
  {$ENDIF}
  {$IFDEF SERVIDOR}
  WriteLog(ltRelatorio, AMsg + Format(MSG_MEMORIA, [SMemoriaUtilizada div UM_MEGA, InfoAPPMemory.GetAvailablePhisycalMemory]));
  {$ENDIF}
end;

procedure THelperLog.UnitTest(AMsg: string);
begin
  WriteLog(ltUnitTest, AMsg);
end;

procedure THelperLog.UnitTest(AMsg: string; const AArgs: array of const);
begin
  UnitTest(Format(AMsg, AArgs));
end;

procedure THelperLog.Info(AMsg: string);
begin
  WriteLog(ltInfo, AMsg);
end;

procedure THelperLog.Warn(const AMsg: string; const AArgs: array of const);
begin
  Warn(Format(AMsg, AArgs));
end;

procedure THelperLog.Warn(AMsg: string);
begin
  WriteLog(ltWarn, AMsg);
end;

procedure THelperLog.Web(AMsg: string; const AArgs: array of const);
begin
  Web(Format(AMsg, AArgs));
end;

const
  MAX_COUNTER = 4294967295;

procedure THelperLog.WriteLog(AType: TLogType; AMsg: string);
var
  LMessage: string;
begin
  if Application.Terminated then
    Exit;

  {$IFDEF UNITTESTMVC}
  if (Pos('UnitTest', AMsg) = 0) and (Pos('Pessoa', AMsg) = 0) then
    Exit;
  {$ENDIF}

  try
    // formata a mensagem quer ser� escrita no log e no standard output
    LMessage := Format('%s | %d | %s | %s', [
      FormatDateTime('dd.mm.yyyy hh":"nn":"ss"."zzz', Now),
      GetCurrentThreadId,
      strTLogType[AType],
      AMsg
    ]);
  except
    On E: Exception do
    begin
      Error('THelperLog.WriteLog', Self, E);
      LMessage := AMsg;
    end;
  end;

  RCHelperLog.Acquire;
  try
    // verifica se � necess�rio trocar o arquivo de log
    ChangeLogFile;

    // s� escreve a mensagem no log se o tipo estiver dentro do conjunto definido no config
    if AType in FWriteType then
    begin
      // escreve a mensagem no standard output
      {$IFDEF CONSOLE}
      if FWriteStandardOutput then
      begin
        if FLastLogType <> AType then
        begin
          case AType of
            ltInfo,
            ltDebug,
            ltDebugViagem,
            ltRelatorio:  SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED or FOREGROUND_GREEN or FOREGROUND_BLUE);
            ltWarn:       SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED or FOREGROUND_GREEN);
            ltError:      SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED);
            ltDBError:    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED or FOREGROUND_INTENSITY);
            ltDBWarn:     SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED or FOREGROUND_GREEN or FOREGROUND_INTENSITY);
            ltLoad,
            ltInt:        SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_GREEN);
            ltUnitTest:   SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_GREEN);
            ltBenchMark:  SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_GREEN or FOREGROUND_INTENSITY);
            ltIntGR:      SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_BLUE or FOREGROUND_INTENSITY);
          end;
          FLastLogType := AType;
        end;
        Writeln(LMessage);
      end;
      {$ENDIF}

      // escreve a mensagem no arquivo de log
      FThreadWriteLogOnDisk.WriteLog(LMessage, AType in [ltError]);
      {$IFDEF SERVIDOR}
      if (SVRay <> nil) and (AType in [ltWarn, ltError, ltDBError, ltDBWarn]) then
        SVRay.SendWarnError(LMessage);
      {$ENDIF}
    end;

    // incrementa o contador de mensagens por tipo
    if MAX_COUNTER = FCountMsg[AType] then
      FCountMsg[AType] := 0;

    Inc(FCountMsg[AType]);

    // dispara o evento de escrita no log
    if Assigned(FOnWriteLog) then
      FOnWriteLog(AType, AMsg, Now);

    // Variavel global que controra a ultima data hora de escrita  no log
    SDataHoraUltimaEscritaNoLog := Now;
  finally
    RCHelperLog.Release;
  end;
end;

procedure THelperLog.ZipFiles;
var
  LArquivosProtegidos: TStringList;
begin
  if F7Zip = '' then
    Exit;

  LArquivosProtegidos := TStringList.Create;
  LArquivosProtegidos.Add(FFilename);
  TThreadZipArchives.Create(FPath, 'txt', F7Zip, LArquivosProtegidos);
end;

procedure THelperLog.Web(AMsg: string);
begin
  WriteLog(ltWeb, AMsg);
end;

{ TThreadWriteLogOnDisk }

constructor TThreadWriteLogOnDisk.Create;
begin
  inherited Create(True);
  FreeOnTerminate := True;

  FRCMessages := TCriticalSection.Create;
  FPendingMessages := TStringStream.Create;
  FWrittingMessages := TStringStream.Create;
  NameThreadForDebugging('TThreadWriteLogOnDisk', Self.ThreadID);
end;

destructor TThreadWriteLogOnDisk.Destroy;
begin
  FreeAndNil(FRCMessages);
  FreeAndNil(FWrittingMessages);
  FreeAndNil(FPendingMessages);
  inherited;
end;

procedure TThreadWriteLogOnDisk.Execute;
begin
  FDateTimeInicialization := Now;
  while not Terminated do
  begin
    Sleep(1000);
    if not Terminated  then
      WriteLogsOnDisk;
  end;
end;

procedure TThreadWriteLogOnDisk.SetLogFile(var ALogFile: TFileStream);
begin
  FLogFile := ALogFile;
end;

procedure TThreadWriteLogOnDisk.WriteLog(AMensagem: string; AForceWrite: Boolean);
begin
  FRCMessages.Acquire;
  try
    AMensagem := AMensagem + EOL;
    FPendingMessages.Position := FPendingMessages.Size;
    FPendingMessages.WriteString(AMensagem);

    if AForceWrite then
      WriteLogsOnDisk;
  finally
    FRCMessages.Release;
  end;

  if AForceWrite or ((Now - FDateTimeInicialization) < WRITE_FORCED_DELAY) then
    WriteLogsOnDisk;
end;

procedure TThreadWriteLogOnDisk.WriteLogsOnDisk;
begin
  try
    if (FPendingMessages.Size = 0) or (FLogFile = nil) then
      Exit;

    FRCMessages.Acquire;
    try
      FPendingMessages.Position := 0;
      FWrittingMessages.CopyFrom(FPendingMessages, FPendingMessages.Size);
      FPendingMessages.Clear;
    finally
      FRCMessages.Release;
    end;

    FWrittingMessages.Position := 0;
    FLogFile.Position := FLogFile.Size;
    FLogFile.WriteBuffer(FWrittingMessages.Memory^, FWrittingMessages.Size);
    FWrittingMessages.Clear;
  except
  //aqui apresenta exce��o ao fechar o Cliente
  end;
end;

{ TThreadZipArchives }

constructor TThreadZipArchives.Create(ADirPath, AFileExtention, A7ZipPath: string; AExceptionFiles: TStringList);
begin
  FDirPath := ADirPath;
  FFileExtention := AFileExtention;
  FExcetionFiles := AExceptionFiles;
  F7ZipPath := A7ZipPath;

  inherited Create(False);
  FreeOnTerminate := True;
  NameThreadForDebugging('TThreadZipArchives', Self.ThreadID);
end;

destructor TThreadZipArchives.Destroy;
begin
  FreeAndNil(FExcetionFiles);
  inherited;
end;

procedure TThreadZipArchives.Execute;
var
  i: Integer;
  LListFiles: TStringList;
  LFilenameZip: string;
  zipCmd: string;
  LResultStr: string;
begin
  // obt�m a lista de arquivos da pasta de log
  LListFiles := GetFiles(FDirPath, '*.' + FFileExtention, True);
  try
    for i := 0 to LListFiles.Count - 1 do
    begin
      // n�o compacta o arquivo de log atual
      if FExcetionFiles.IndexOf(LListFiles.Strings[i]) = -1 then
      begin
        // compacta o arquivo de log
        LFilenameZip := AnsiReplaceStr(LListFiles.Strings[i], '.txt', '.zip');
        zipCmd := Format('%s a "%s" "%s"', [
          F7ZipPath,
          LFilenameZip,
          LListFiles.Strings[i]
        ]);
//        LResultStr := GetDosOutputAndWait(zipCmd, FDirPath);
        Logger.Debug('TThreadZipArchives.Execute: ' + StringReplace(LResultStr, sLineBreak, ' ', [rfReplaceAll]));

        // apaga o aquivo .txt de log se tiver criado o arquivo .zip
        if FileExists(LFilenameZip) then
          DeleteFile(PWideChar(LListFiles.Strings[i]));
      end;
    end;
  finally
    LListFiles.Free;
  end;

  LListFiles := GetFiles(FDirPath, '*.tmp*', True);
  try
    for I := 0 to LListFiles.Count - 1 do
      DeleteFile(PWideChar(LListFiles.Strings[I]));
  finally
    LListFiles.Free;
  end;

end;

initialization
  {$IFOPT W+}
  JclStackTrackingOptions := JclStackTrackingOptions + [stExceptFrame];
  // Enable raw mode (default mode uses stack frames which aren't always generated by the compiler)
  JclStackTrackingOptions := JclStackTrackingOptions + [stRawMode];
  // Disable stack tracking in dynamically loaded modules (it makes stack tracking code a bit faster)
  JclStackTrackingOptions := JclStackTrackingOptions + [stStaticModuleList];
  JclStartExceptionTracking;
  {$ENDIF}

  RCHelperLog := TCriticalSection.Create;

  // convers�o do tipo enumerado para string
  strTLogType[ltInfo]         := 'INFO';
  strTLogType[ltDebug]        := 'DEBUG';
  strTLogType[ltWarn]         := 'WARN';
  strTLogType[ltError]        := 'ERROR';
  strTLogType[ltDBError]      := 'DBERROR';
  strTLogType[ltDBWarn]       := 'DBWARN';
  strTLogType[ltWeB]          := 'WEB';
  strTLogType[ltUnitTest]     := 'UNITTEST';
  strTLogType[ltBenchMark]    := 'BENCHMARK';
  strTLogType[ltRelatorio]    := 'RELATORIO';
  strTLogType[ltDebugViagem]  := 'VIAGEM';
  strTLogType[ltLoad]         := 'LOAD';
  strTLogType[ltInt]          := 'INTEGRACAO';
  strTLogType[ltIntGR]        := 'INTEGRACAO_GR';

finalization
  RCHelperLog.Free;

end.
