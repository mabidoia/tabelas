unit uDomains;

interface

const

  // usuarios
  USER_ADMIN              = 1;
  INTEGRACAO_USUARIO_ID   = 9999;
  USUARIO_ADMINSUPORTE    = 9990;

  // Medidas
  CINQUENTA_METROS            = 50;
  MIL_METROS                  = 1000;
  MIL_MILILITROS              = 1000;
  UM_GIGA                     = 1073741824;
  UM_PONTO_TRES_GIGAS         = 1388314624;
  UM_PONTO_CINCO_GIGAS        = 1610612736;
  UM_MEGA                     = 1048576;
  CINCO_MEGA                  = 5242880;
  QUINH_MEGA                  = 524288000;

  // Tempo
  UM_SEGUNDO                  = 1000;
  TRES_SEGUNDOS               = UM_SEGUNDO * 3;
  CINCO_SEGUNDOS              = UM_SEGUNDO * 5;
  TRINTA_SEGUNDO              = UM_SEGUNDO * 30;
  QUARENTA_CINCO_SEGUNDOS     = UM_SEGUNDO * 45;
  UM_MINUTO                   = UM_SEGUNDO * 60;
  TRES_MINUTOS                = UM_MINUTO * 3;
  CINCO_MINUTOS               = UM_MINUTO * 5;
  DEZ_MINUTOS                 = UM_MINUTO * 10;
  QUINZE_MINUTOS              = UM_MINUTO * 15;
  TEMPO_RETRANSMISSAO         = CINCO_MINUTOS;

  // C�digos de alvos para speed limit configurado automaticamente

  ALVO_PADRAO_GPS_INVALIDO  = 65534;
  ALVO_PADRAO_CONFIG_MANUAL = 65535;

  // Minutos
  UM_SEGUNDO_INT                  = 1;
  CINCO_SEGUNDOS_INT              = 5;
  TRINTA_SEGUNDO_INT              = 30;
  QUARENTA_CINCO_SEGUNDOS_INT     = 45;
  UM_MINUTO_INT                   = 1;
  TRES_MINUTOS_INT                = 3;
  CINCO_MINUTOS_INT               = 5;
  DEZ_MINUTOS_INT                 = 10;
  QUINZE_MINUTOS_INT              = 15;
  VINTE_MINUTOS_INT               = 20;
  TRINTA_MINUTOS_INT              = 30;

  // Numero de horas
  UMA_HORA = 1;
  DUAS_HORAS = 2;
  TRES_HORAS = 3;
  QUATRO_HORAS = 4;
  CINCO_HORAS = 5;
  SEIS_HORAS = 6;
  SETE_HORAS = 7;
  OITO_HORAS = 8;
  NOVE_HORAS = 9;
  DEZ_HORAS = 10;
  DOZE_HORAS = 12;

  //N�mero de dias
  UM_DIA_EM_SEGUNDOS          = 86400;
  UMA_HORA_EM_SEGUNDOS        = 3600;
  UMA_HORA_EM_MILISEGUNDOS    = UMA_HORA_EM_SEGUNDOS * UM_SEGUNDO;
  UM_DIA                      = 1;
  DOSI_DIAS                   = 2;
  TRES_DIAS                   = 3;
  QUATRO_DIAS                 = 4;
  CINCO_DIAS                  = 5;
  SEIS_DIAS                   = 6;
  SETE_DIAS                   = 7;
  OITO_DIAS                   = 8;
  NOVE_DIAS                   = 9;
  DEZ_DIAS                    = 10;

  // Conex�o
  NRO_TENTATIVAS_CONEXAO = 5;

  // Mensagem
  ORIGEM_BD                     = 'Banco de Dados.';
  FIREBIRD_IMPLEMENTATION_LIMIT = 1500;

  // Bancos de dados na string de conex�o
  BANCO_DE_DADOS_FIREBIRD     = 'Firebird';
  BANCO_DE_DADOS_SQLSERVER    = 'SQLOLEDB';
  BANCO_DE_DADOS_POSTGRESQL   = 'Postgresql';

  // Clientes
  CAMPOS_INTEGRACAO_HBSIS = 'O~ CONFIRMAR DEVOLUCAO~P~ CONFIRMAR ENTREGA~';
  CAMPOS_INTEGRACAO_HBSIS_PARCIAL = 'O~ CONFIRMAR DEVOLUCAO~Q~ DEVOLUCAO PARCIAL~P~ CONFIRMAR ENTREGA~';
  CAMPOS_INTEGRACAO_HBSIS_PARCIAL_VALOR = 'O~ CONFIRMAR DEVOLUCAO~R~ DEVOLUCAO PARCIAL~P~ CONFIRMAR ENTREGA~';
  CAMPOS_INTEGRACAO_HBSIS_PARCIAL_QTD_COD = 'O~ CONFIRMAR DEVOLUCAO~T~ DEVOLUCAO PARCIAL~P~ CONFIRMAR ENTREGA~';

  CAMPOS_INTEGRACAO_AMBEV = 'M~ CHEGADA NO PDV~N~ INICIO DE ENTREGA~O~ PROBLEMAS NA ENTREGA~P~ FINALIZAR~';

//************************************* | INTEGRACAO | ***********************************************//
  //Integracao de viagens online                                                                      //
  INTEGRACAO_VIAGENS_ONLINE = 'INTEGRACAO_VIAGENS_ONLINE';                                            //
  TIME_EXPORT               = 'TimeExport';                                                           //
                                                                                                      //
                                                                                                      //
  // Integra��o garcia                                                                                //
  TOLERANCIA_MAX_DIST_PERCORRIDA = 99999999;                                                          //
  TOLERANCIA_MAX_CONSUMO = 9999;                                                                      //
                                                                                                      //
  // integracao BGM                                                                                   //
  MOTORISTA_ATIVO = 'ATIVO';                                                                          //
  MOTORISTA_INATIVO = 'INATIVO';                                                                      //
  LINHA_ATIVA = 'S';                                                                                  //
  LINHA_INATIVA = 'D';                                                                                //
                                                                                                      //
  // Integra��o GR                                                                                    //
  ITEM_PENDENTE  = 0;                                                                                 //
  ITEM_EXPORTADO = 1;                                                                                 //
                                                                                                      //
  TIPO_SOLICITACAO = 'solicitacao';                                                                   //
  LISTA_EVENTOS    = 'solicitareventos';                                                              //
                                                                                                      //
//****************************************************************************************************//
  // Regi�o eletr�nica
  AREA_TIPO_GARAGEM = '[GARAGEM]';

  // Tipos de dados
  TYPE_INTEGER          		= 'integer';
  TYPE_SMALLINT         		= 'SMALLINT';
  TYPE_BOOLEAN          		= 'boolean';
  TYPE_BIGINT               = 'bigint';
  TYPE_VARCHAR_11       		= 'VARCHAR(11)';
  TYPE_VARCHAR_12       		= 'VARCHAR(12)';
  TYPE_VARCHAR_15       		= 'VARCHAR(15)';
  TYPE_VARCHAR_30       		= 'VARCHAR(30)';
  TYPE_VARCHAR_32       		= 'VARCHAR(32)';
  TYPE_VARCHAR_50           = 'VARCHAR(50)';
  TYPE_VARCHAR_100      		= 'VARCHAR(100)';
  TYPE_VARCHAR_255      		= 'VARCHAR(255)';
  TYPE_VARCHAR_500      		= 'VARCHAR(500)';
  TYPE_DATETIME         		= 'DATETIME';
  TYPE_TIMESTAMP        		= 'TIMESTAMP';
  TYPE_LATITUDE             = 'LATITUDE';
  TYPE_LONGITUDE            = 'LONGITUDE';

  // Reiniciar o servidor
  ARQUIVO_REINICIO = 'reboot';
  TEMPO_DE_ESPERA_CRIAR_ARQUIVO_REBOOT = 300;

  //Relat�rios pr�-processados
  RELATORIO_CONSUMO_DE_COMBUSTiVEL = 'Relat�rio consumo de combust�vel';

  // Ve�culos
  VEICULO_BLOQUEADO = 1;
  STR_VEICULO_BLOQUEADO = 'Bloqueado';
  VEICULO_DESBLOQUEADO = 0;
  STR_VEICULO_DESBLOQUEADO = 'Desbloqueado';
  VEICULO_BLOQUEIO_INDEFINIDO = 2;
  VEICULO_MIN_VALOR_BLOQUEIO = 127;
  VEICULO_MIN_VALOR_DESBLOQUEIO = -1;

  // Viagens

  // Tipos de itiner�rio
  TIPO_ITINERARIO_SOLTURA      = 'SOLTURA';
  TIPO_ITINERARIO_RECOLHIMENTO = 'RECOLHIMENTO';
  TIPO_ITINERARIO_VIAGEM       = 'VIAGEM';
  TIPO_ITINERARIO_EXTRA        = 'EXTRA';

  // Sentido Itinerario
  SENTIDO_ITINERARIO_IDA   = 'IDA';
  SENTIDO_ITINERARIO_VOLTA = 'VOLTA';
  SENTIDO_ITINERARIO_UNICO = 'UNICO';

  //Temperaturas
  SECAO_TEMPERATURAS = 'TEMPERATURAS';
  IDENTIFICADOR_TEMPERATURA_MAXIMA_SENSOR01 = 'TEMPERATURA_MAXIMA_SENSOR01';
  IDENTIFICADOR_TEMPERATURA_MAXIMA_SENSOR02 = 'TEMPERATURA_MAXIMA_SENSOR02';
  IDENTIFICADOR_TEMPERATURA_MAXIMA_SENSOR03 = 'TEMPERATURA_MAXIMA_SENSOR03';
  IDENTIFICADOR_TEMPERATURA_MAXIMA_SENSOR04 = 'TEMPERATURA_MAXIMA_SENSOR04';
  IDENTIFICADOR_TEMPERATURA_MINIMA_SENSOR01 = 'TEMPERATURA_MINIMA_SENSOR01';
  IDENTIFICADOR_TEMPERATURA_MINIMA_SENSOR02 = 'TEMPERATURA_MINIMA_SENSOR02';
  IDENTIFICADOR_TEMPERATURA_MINIMA_SENSOR03 = 'TEMPERATURA_MINIMA_SENSOR03';
  IDENTIFICADOR_TEMPERATURA_MINIMA_SENSOR04 = 'TEMPERATURA_MINIMA_SENSOR04';

  // Motoristas
  STR_MOTORISTA_NENHUM = 'NENHUM';
  STR_SEM_VIGENCIA = 'Sem vig�ncia de motorista para o per�odo';
  // Data limite estipulada para a vigencia sem data final - Determinada pela Gerencia.
  // Para evitar problemas com formatos de datas � passado um float equivalente a data
  DATA_LIMITE_VIGENCIA = 73050.9999884259; // este float representa a seguinte data '31/12/2099 23:59:59';
  CNH_VENCIDA = 'Motorista Conduzindo com CNH Vencida';

type
  { TODO -oUlder -cURGENTE : Criar dom�nios para cada tipo de campo. }

  // tipos de dados utilizados na defini��o dos campos da tabela
  TString2 = string[2];   // Sigla Estado
  TString3 = string[3];
  TString5 = string[5];
  TString7 = string[7];   // veiculo.placa
  TString8 = string[8];   // placa
  TString9 = string[9];   // statusdirecional
  TString10 = string[10]; // Telefone
  TString11 = string[11]; // CPF
  TString12 = string[12]; // C�digo Motorista
  TString13 = string[13]; //
  TString14 = string[14]; // CPF_CNPJ
  TString15 = string[15]; // INTEGRACAO, login    ]
  TString16 = string[16]; // Descri��o grupo formul�rio
  TString20 = string[20];
  TString22 = string[22]; // dura��o total
  TString24 = string[24];
  TString25 = string[25]; // Modelo Veiculo
  TString29 = string[29];
  TString30 = string[30]; // RFID
  TString31 = string[31]; // agendamento
  TString32 = string[32]; // senha md5
  TString35 = string[35]; // senha md5
  TString40 = string[40];
  TString45 = string[45]; // Descricao do grupo de macro de acao de veiculos
  TString48 = string[48]; // Descricao da MacroAcao **N�o existe mais
  TString50 = string[50];
  TString52 = string[52];
  TString58 = string[58];
  TString59 = string[59]; //Descricao do Formul�rio
  TString60 = string[60];
  TString64 = string[64]; //Login usu�rio
  TString80 = string[80];
  TString100 = string[100];
  TString130 = string[130];
  TString150 = string[150];
  TString200 = string[200];
  TString250 = string[250];
  TString255 = string[255];

  TAcaoRealizada = (
    tarNaoInformada,
    tarInserido,
    tarInseridoIntegracao,
    tarAtualizado,
    tarAtualizadoIntegracao,
    tarInativado,
    tarInativadoIntegracao,
    tarCancelado,
    tarCanceladoIntegracao
  );

var
  BoolToStrPT: array[Boolean] of string;

implementation

initialization
  BoolToStrPT[False] := 'N�o';
  BoolToStrPT[True]  := 'True';

end.

