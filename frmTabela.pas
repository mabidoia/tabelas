unit frmTabela;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, uCriarTabela;

type
  TForm1 = class(TForm)
    mmoSaida: TMemo;
    btnGerarTabela: TButton;
    procedure btnGerarTabelaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.btnGerarTabelaClick(Sender: TObject);
var
  tabela: TTabela;
  Resultado: TStringList;
  Linha: string;
begin
    mmoSaida.Clear;

    tabela := TTabela.Create('Teste', True);

    tabela.SetCampo('campo01', tcint, True, '', True);
    tabela.SetCampo('campo02', tcdatetime, False, 'getdate()');

    tabela.SetChavePrimaria(['campo01'], 'pk_tabela_campo01');
    tabela.SetChaveEstrangeira('fk_Teste02', 'campo02', 'Teste02', 'campo01');

    Resultado := tabela.GerarTabela;

    for Linha in Resultado do
      mmoSaida.Lines.Add(Linha);
end;

end.
